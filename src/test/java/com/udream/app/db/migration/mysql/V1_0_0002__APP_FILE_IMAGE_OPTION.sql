-- Dumping structure for table fitz.app_file
CREATE TABLE IF NOT EXISTS `app_file` (
  `FILE_ID` varchar(32) NOT NULL COMMENT '文件ID',
  `FILE_NAME` varchar(250) NOT NULL COMMENT '文件名称(例如:图片.png)',
  `FILE_SIZE` int(11) DEFAULT NULL COMMENT '文件尺寸',
  `FILE_EXT` varchar(250) DEFAULT NULL COMMENT '文件扩展名(例如：png,mpg,mp3,doc等等,不带点)',
  `FILE_DESCRIPTION` varchar(250) DEFAULT NULL COMMENT '描述',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATE_USER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  `UPDATE_USER` varchar(32) DEFAULT NULL COMMENT '更新人',
  `SERVER_STORE` int(11) DEFAULT NULL COMMENT '服务端文件存储类型(0:本地磁盘;1:云存储结点1;2:云存储结点2;...N.结点N)',
  `SERVER_FILE_NAME` varchar(250) DEFAULT NULL COMMENT '服务端文件名称(例如:uuid.png)',
  `SERVER_PARENT_PATH` varchar(250) DEFAULT NULL COMMENT '服务端文件存储目录(例如：/<用户>/<年份>/<月份>)',
  `FILE_ORDER` int(11) DEFAULT NULL COMMENT '业务集合排序(实现图集、附件等排序）',
  `FILE_OWNER_CLASS` varchar(32) DEFAULT NULL COMMENT '拥有者CLASS',
  `FILE_OWNER_ID` varchar(32) DEFAULT NULL COMMENT '拥有者ID',
  `SERVER_THUMB_NAME` varchar(250) DEFAULT NULL COMMENT '服务端缩略图文件名称',
  PRIMARY KEY (`FILE_ID`)
)  COMMENT='文件';


-- Dumping structure for table fitz.app_image
CREATE TABLE IF NOT EXISTS `app_image` (
  `IMAGE_ID` varchar(32) NOT NULL COMMENT '图片ID',
  `IMAGE_OWNER_CLASS` varchar(32) DEFAULT NULL COMMENT '拥有者CLASS',
  `IMAGE_OWNER_ID` varchar(32) DEFAULT NULL COMMENT '拥有者ID',
  `IMAGE_URL` varchar(250) DEFAULT NULL COMMENT '图片地址',
  `IMAGE_THUMBNAIL_URL` varchar(250) DEFAULT NULL COMMENT '缩略图地址',
  `IMAGE_SIZE` int(11) DEFAULT NULL COMMENT '图片大小',
  `IMAGE_ORDER` int(11) DEFAULT NULL COMMENT '图片排序',
  `IMAGE_NAME` varchar(250) DEFAULT NULL COMMENT '图片名称',
  PRIMARY KEY (`IMAGE_ID`)
)  COMMENT='图片管理';

-- Dumping structure for table fitz.app_option
CREATE TABLE IF NOT EXISTS `app_option` (
  `OPTION_ID` varchar(32) NOT NULL COMMENT '选项ID',
  `OPTION_PARENT_ID` varchar(32) DEFAULT NULL COMMENT '上级选项ID',
  `OPTION_CODE` varchar(250) DEFAULT NULL COMMENT '选项标识',
  `OPTION_NAME` varchar(250) DEFAULT NULL COMMENT '选项名称',
  `OPTION_COMMENT` varchar(250) DEFAULT NULL COMMENT '选项描述',
  `OPTION_ORDER` int(11) DEFAULT NULL COMMENT '选项排序',
  `OPTION_ICON` varchar(32) DEFAULT NULL COMMENT '选项图标',
  `OPTION_URL` varchar(250) DEFAULT NULL COMMENT '选项URL',
  `OPTION_TYPE` int(11) DEFAULT NULL COMMENT '选项类型 ',
  `OPTION_FLAG` int(11) DEFAULT NULL COMMENT '选项状态 0启用 1停用',
  PRIMARY KEY (`OPTION_ID`),
  KEY `IDX_APP_OPTION_CODE` (`OPTION_CODE`),
  KEY `IDX_APP_OPTION_PARENT_ID` (`OPTION_PARENT_ID`)
)  COMMENT='系统选项';

-- Dumping data for table fitz.app_option: ~3 rows (approximately)
INSERT IGNORE INTO `app_option` (`OPTION_ID`, `OPTION_PARENT_ID`, `OPTION_CODE`, `OPTION_NAME`, `OPTION_COMMENT`, `OPTION_ORDER`, `OPTION_ICON`, `OPTION_URL`, `OPTION_TYPE`, `OPTION_FLAG`) VALUES
	('yes_no', '0', 'yes_no', '是否', NULL, 0, NULL, NULL, 0, 1),
	('yes_no_0', 'yes_no', '0', '否', NULL, 0, NULL, NULL, 1, 1),
	('yes_no_1', 'yes_no', '1', '是', NULL, 1, NULL, NULL, 1, 1);