drop table if exists MERCHANT_RAISE;
/*==============================================================*/
/* Table: MERCHANT_RAISE                                        */
/*==============================================================*/
create table MERCHANT_RAISE
(
   RAISE_ID             varchar(32) not null comment '提额ID',
   RAISE_APPLY_ID       varchar(32) comment '申请人ID ',
   RAISE_APPLY_NAME     varchar(250) comment '申请人姓名',
   RAISE_TYPE           int comment '提额类型 0 永久提额 1 临时提额',
   RAISE_MONEY          decimal(32,2) comment '提额金额',
   RAISE_START_TIME     datetime comment '开始时间',
   RAISE_APPLY_PHONE    varchar(32) comment '申请人手机',
   RAISE_END_TIME       datetime comment '截止时间',
   RAISE_APPLY_COMMENTS varchar(1000) comment '申请说明',
   RAISE_AREA_USER_ID   varchar(32) comment '区县审核人ID',
   RAISE_AREA_USER_NAME varchar(250) comment '区县审核人姓名',
   RAISE_AREA_TIME      datetime comment '区县审核时间',
   RAISE_AREA_COMMENTS  varchar(250) comment '区县审核意见',
   RAISE_CITY_USER_ID   varchar(32) comment '市审核人ID',
   RAISE_CITY_USER_NAME varchar(250) comment '市审核人姓名',
   RAISE_CITY_TIME      datetime comment '市审核时间',
   RAISE_CITY_COMMENTS  varchar(250) comment '市审核意见',
   RAISE_PROVINCE_USER_ID varchar(32) comment '省审核人ID',
   RAISE_PROVINCE_USER_NAME varchar(250) comment '省审核人姓名',
   RAISE_PROVINCE_TIME  datetime comment '省审核时间',
   RAISE_PROVINCE_COMMENTS varchar(250) comment '省审核意见',
   RAISE_STATUS         varchar(250) comment '审核状态 0 草稿 1 区审核中 2 区审核驳回 3 市审核中 4 市审核驳回 5 省审核中 6 省审核驳回 ',
   RAISE_AREA_ID        varchar(32) comment '区县ID',
   RAISE_CITY_ID        varchar(32) comment '市ID',
   RAISE_PROVINCE_ID    varchar(32) comment '省ID',
   primary key (RAISE_ID)
);

alter table MERCHANT_RAISE comment '商家提额';