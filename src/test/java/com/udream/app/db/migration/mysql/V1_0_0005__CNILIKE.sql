/*==============================================================*/
/* Table: AGENT_ACCOUNT                                         */
/*==============================================================*/
create table AGENT_ACCOUNT
(
   ACCOUNT_ID           varchar(32) not null comment '帐号ID',
   ACCOUNT_PASSWORD     varchar(32) comment '交易密码',
   ACCOUNT_CREDIT       numeric(22,0) comment '积分',
   ACCOUNT_COIN         numeric(22,0) comment '金币(孝分)',
   primary key (ACCOUNT_ID)
);

alter table AGENT_ACCOUNT comment '代理资金帐号';

/*==============================================================*/
/* Table: AGENT_APPLY                                           */
/*==============================================================*/
create table AGENT_APPLY
(
   APPLY_ID             varchar(32) not null comment '代理商(孝友)申请ID',
   APPLY_SUBMIT_USER_ID varchar(32) comment '代理商(孝友)申请提交人',
   APPLY_SUBMIT_USER_NAME varchar(32) comment '代理商(孝友)申请提交人姓名',
   APPLY_SUBMIT_TIME    datetime comment '代理商(孝友)申请提交时间',
   APPLY_NAME           varchar(250) comment '代理商(孝友)申请:申请人姓名',
   APPLY_MOBILE         varchar(32) comment '代理商(孝友)申请:手机号码',
   APPLY_PASSWORD       varchar(32) comment '代理商(孝友)申请:设置密码',
   APPLY_PASSWORD_CONFIRM varchar(32) comment '代理商(孝友)申请:确认密码',
   APPLY_STATE          int comment '代理商(孝友)申请状态 0:草稿 1:提交审核 10: 审核通过 -1:审核不通过',
   APPLY_CURRENT_ORG    varchar(32) comment '代理商(孝友)申请当前审核部门',
   APPLY_AREA_USER_ID   varchar(32) comment '代理商(孝友)申请:区审核人',
   APPLY_AREA_TIME      datetime comment '代理商(孝友)申请:区审核时间',
   APPLY_AREA_COMMENT   varchar(250) comment '代理商(孝友)申请:区审核意见',
   APPLY_CITY_USER_ID   varchar(32) comment '代理商(孝友)申请:市审核人',
   APPLY_CITY_TIME      datetime comment '代理商(孝友)申请:市审核时间',
   APPLY_CITY_COMMENT   varchar(1000) comment '代理商(孝友)申请:市审核意见',
   APPLY_GRADE          int comment '代理商(孝友)申请:类型(0:准孝友 1:至尊孝友)',
   AGENT_USER_ID        varchar(32) comment '代理商(孝友)申请:申请通过创建或者关联的帐号',
   primary key (APPLY_ID)
);

alter table AGENT_APPLY comment '代理商(孝友)申请';

/*==============================================================*/
/* Table: AGENT_USER                                            */
/*==============================================================*/
create table AGENT_USER
(
   USER_ID              varchar(32) not null comment '用户ID',
   USER_CODE            varchar(32) comment '用户编码(用户名)',
   USER_PASSWORD        varchar(32) comment '用户密码',
   USER_NAME            varchar(250) comment '用户姓名',
   USER_MOBILE          varchar(32) comment '用户手机',
   USER_NICKNAME        varchar(250) comment '用户昵称',
   USER_IMAGE           varchar(250) comment '用户图像',
   USER_GRADE           int comment '用户级别(0:普通 1:至尊)',
   USER_STATE           int default 1 comment '用户状态',
   primary key (USER_ID)
);

alter table AGENT_USER comment '代理商用户';

/*==============================================================*/
/* Table: APP_ABOUT                                             */
/*==============================================================*/
create table APP_ABOUT
(
   ABOUT_ID             varchar(32) not null comment '关于ID',
   ABOUT_CODE           varchar(32) comment '关于编码',
   ABOUT_NAME           varchar(250) comment '关于名称',
   ABOUT_LOGO           varchar(250) comment '关于LOGO',
   ABOUT_WEIXIN         varchar(32) comment '关于微信号',
   ABOUT_WEIBO          varchar(32) comment '关于微博',
   ABOUT_CONTACT        varchar(32) comment '关于联系电话',
   ABOUT_COMPANY_NAME   varchar(250) comment '关于公司名称',
   ABOUT_DESCRIPTION    varchar(1000) comment '关于描述',
   ABOUT_ONLINE_SUPPORT_IM varchar(32) comment '关于在线客服号码(IM)',
   ABOUT_ONLINE_SUPPORT_QQ varchar(32) comment '关于在线客服号码(QQ)',
   primary key (ABOUT_ID)
);

alter table APP_ABOUT comment '关于我们';

/*==============================================================*/
/* Table: APP_AD                                                */
/*==============================================================*/
create table APP_AD
(
   AD_ID                varchar(32) not null comment '广告ID',
   AD_TITLE             varchar(250) comment '广告标题',
   AD_SUBTITLE          varchar(500) comment '广告副标题',
   AD_IMAGE             varchar(250) comment '广告图片',
   AD_URL               varchar(250) comment '广告URL',
   AD_URL_TYPE          int comment '广告URL类型',
   AD_ORDER             numeric(22,0) comment '广告排列顺序',
   AD_OS                int comment '广告展示的平台 0:ALL 1:Adnroid 2:ios 3:web',
   USER_TYPE            int comment '广告针对的用户类型 0:消费者 10:商家 100:代理商 1000:运营商 10000:总部/总公司',
   primary key (AD_ID)
);

alter table APP_AD comment '广告';

/*==============================================================*/
/* Table: APP_BANK_CARD                                         */
/*==============================================================*/
create table APP_BANK_CARD
(
   BANK_CARD_ID         varchar(32) not null comment '银行卡ID',
   BANK_NAME            varchar(250) comment '银行名称',
   BANK_CARD_NUMBER     varchar(32) comment '银行卡号',
   BANK_CARD_TYPE       int comment '银行卡类型，0:不能识别; 1: 借记卡; 2: 信用卡',
   BIND_USER_ID         varchar(32) comment '绑定用户ID',
   BIND_TIME            varchar(32) comment '绑定时间',
   BIND_STATE           varchar(32) comment '绑定状态',
   primary key (BANK_CARD_ID)
);

alter table APP_BANK_CARD comment '银行卡认证';

/*==============================================================*/
/* Table: APP_CLIENT                                            */
/*==============================================================*/
create table APP_CLIENT
(
   CLIENT_ID            varchar(32) not null comment '客户端ID',
   CLIENT_TYPE          int comment '客户端类型Android:IOS,Web',
   CLIENT_VERSION       varchar(32) comment '客户端版本系统版本号',
   CLIENT_MODEL         varchar(32) comment '客户端型号',
   CLIENT_SERIAL_NUMBER varchar(32) comment '客户端设备ID',
   primary key (CLIENT_ID)
);

alter table APP_CLIENT comment '保存每个调用API的客户端信息。包括Android客户端/iOS客户端/Web客户端.';

/*==============================================================*/
/* Table: APP_CODE                                              */
/*==============================================================*/
create table APP_CODE
(
   CLIENT_ID            varchar(32) not null comment '客户端ID',
   CODE_TYPE            int not null comment '验证码类型0:CAPTCHA 1:短信',
   CODE_TEXT            varchar(32) comment '验证码内容',
   CODE_CREATE_TIME     datetime comment '验证码生成时间',
   CODE_EXPIRATION_TIME datetime comment '验证码过期时间',
   CODE_STATE           int default 1 comment '验证码是否有效',
   CODE_PURPOSE         int comment '验证码意图',
   CODE_TARGET          varchar(32) comment '验证码目标(短信为目标手机号码）',
   primary key (CLIENT_ID, CODE_TYPE)
);

alter table APP_CODE comment '验证码(短信验证码4位数字/CAPTCHA)';

/*==============================================================*/
/* Table: APP_IDENT                                             */
/*==============================================================*/
create table APP_IDENT
(
   IDENT_ID             varchar(32) not null comment '实名认证ID',
   IDENT_ADDRESS        varchar(250) comment '住址',
   IDENT_CARD_NUMBER    varchar(32) comment '公民身份号码',
   IDENT_BIRTHDAY       datetime comment '出生',
   IDENT_NAME           varchar(250) comment '姓名',
   IDENT_SEX            int comment '性别',
   IDENT_NATION         varchar(250) comment '民族',
   IDENT_SIGN_DATE      datetime comment '签发日期',
   IDENT_INVALID_DATE   datetime comment '失效日期',
   IDENT_FRONT_IMAGE    varchar(250) comment '上传身份证的正面',
   IDENT_BACK_IMAGE     varchar(250) comment '上传身份证的背面',
   USER_ID              varchar(32) comment '用户ID',
   USER_TYPE            int comment '用户类型',
   IDENT_STATE          int comment '认证状态 0:未认证 1:通过认证 -1:未通过认证',
   primary key (IDENT_ID)
);

alter table APP_IDENT comment '实名认证';

/*==============================================================*/
/* Table: APP_REPORT                                            */
/*==============================================================*/
create table APP_REPORT
(
   REPORT_ID            varchar(32) not null comment '举报ID',
   REPORT_SUBMIT_USER_ID varchar(32) comment '举报提交人',
   REPORT_SUBMIT_TIME   datetime comment '举报提交时间',
   REPORT_SUBMIT_USER_NAME varchar(250) comment '举报提交人姓名',
   REPORT_SUBMIT_CONTACT varchar(32) comment '举报提交人联系电话',
   REPORT_TYPE          int comment '举报类型',
   REPORT_DESCRIPTION   varchar(1000) comment '举报事件说明',
   REPORT_STATE         int comment '举报状态',
   REPORT_AREA_USER_ID  varchar(32) comment '举报区受理人',
   REPORT_AREA_TIME     datetime comment '举报区受理时间',
   REPORT_AREA_USER_NAME varchar(250) comment '举报区受理人姓名',
   REPORT_AREA_COMMENT  varchar(250) comment '举报区受理意见',
   REPORT_CITY_USER_ID  varchar(32) comment '举报市受理人',
   REPORT_CITY_TIME     datetime comment '举报市受理时间',
   REPORT_CITY_USER_NAME varchar(250) comment '举报市受理人姓名',
   REPORT_CITY_COMMENT  varchar(250) comment '举报市受理意见',
   REPORT_PROVINCE_USER_ID varchar(32) comment '举报省受理人',
   REPORT_PROVINCE_TIME datetime comment '举报省受理时间',
   REPORT_PROVINCE_USER_NAME varchar(250) comment '举报省受理人姓名',
   REPORT_PROVINCE_COMMENT varchar(250) comment '举报省受理意见',
   REPORT_CURRENT_ORG   varchar(32) comment '举报当前受理组织',
   primary key (REPORT_ID)
);

alter table APP_REPORT comment '举报管理';

/*==============================================================*/
/* Table: APP_THIRD_USER                                        */
/*==============================================================*/
create table APP_THIRD_USER
(
   THIRD_ID             varchar(32) not null comment '第三方ID',
   THIRD_TYPE           varchar(32) not null comment '第三方类型(微信:WEI_XIN)',
   USER_ID              varchar(32) not null comment '系统用户ID',
   USER_TYPE            varchar(32) not null comment '系统用户类型 0:消费者 10:商家 100:代理商 1000:运营商 10000:总部/总公司',
   primary key (THIRD_ID, USER_ID, USER_TYPE, THIRD_TYPE)
);

alter table APP_THIRD_USER comment '第三方用户登录(例如:保存微信用户ID和系统用户ID之间的关系)';

/*==============================================================*/
/* Table: APP_TOKEN                                             */
/*==============================================================*/
create table APP_TOKEN
(
   TOKEN_ID             varchar(32) not null comment '令牌ID',
   TOKEN_CREATE_TIME    datetime comment '令牌创建时间',
   TOKEN_EXPIRATION_TIME datetime comment '令牌过期时间',
   TOKEN_STATE          int default 1 comment '令牌是否有效',
   CLIENT_ID            varchar(32) comment '令牌所属客户端ID',
   USER_ID              varchar(32) comment '令牌所属用户ID',
   USER_TYPE            int comment '令牌所属用户类型 0:消费者 10:商家 100:代理商 1000:运营商 10000:总部/总公司',
   primary key (TOKEN_ID)
);

alter table APP_TOKEN comment '令牌';

/*==============================================================*/
/* Table: CONSUMER_ACCOUNT                                      */
/*==============================================================*/
create table CONSUMER_ACCOUNT
(
   ACCOUNT_ID           varchar(32) not null comment '帐号ID',
   ACCOUNT_PASSWORD     varchar(32) comment '交易密码',
   ACCOUNT_CREDIT       numeric(22,0) comment '积分',
   ACCOUNT_COIN         numeric(22,0) comment '金币(孝分)',
   primary key (ACCOUNT_ID)
);

alter table CONSUMER_ACCOUNT comment '消费者资金帐号';

/*==============================================================*/
/* Table: CONSUMER_FAVORITE                                     */
/*==============================================================*/
create table CONSUMER_FAVORITE
(
   FAVORITE_USER_ID     varchar(32) not null comment '消费者ID',
   FAVORITE_TARGET_ID   varchar(32) not null comment '收藏对象ID',
   FAVORITE_TARGET_TYPE varchar(32) comment '收藏对象类型',
   FAVORITE_STATE       int default 1 comment '收藏状态',
   FAVORITE_DATE        datetime comment '收藏时间',
   primary key (FAVORITE_USER_ID, FAVORITE_TARGET_ID)
);

alter table CONSUMER_FAVORITE comment '消费者我的收藏';

/*==============================================================*/
/* Table: CONSUMER_USER                                         */
/*==============================================================*/
create table CONSUMER_USER
(
   USER_ID              varchar(32) not null comment '用户ID',
   USER_CODE            varchar(32) comment '用户编码(用户名)',
   USER_PASSWORD        varchar(32) comment '用户密码',
   USER_NAME            varchar(250) comment '用户姓名',
   USER_MOBILE          varchar(32) comment '用户手机',
   USER_NICKNAME        varchar(250) comment '用户昵称',
   USER_IMAGE           varchar(250) comment '用户图像',
   REF_USER_ID          varchar(32) comment '推荐人ID',
   REF_USER_MOBILE      varchar(32) comment '推荐人手机',
   IDENT_STATE          int default 1 comment '是否通过实名认证',
   BANK_CARD_STATE      int default 1 comment '是否绑定银行卡',
   USER_STATE           int comment '用户状态',
   primary key (USER_ID)
);

alter table CONSUMER_USER comment '消费者用户';

/*==============================================================*/
/* Table: MALL_PRODUCT                                          */
/*==============================================================*/
create table MALL_PRODUCT
(
   PRODUCT_ID           varchar(32) not null comment '产品ID',
   PRODUCT_NAME         varchar(250) comment '产品名称',
   PRODUCT_COVER_IMAGE  varchar(250) comment '产品封面图地址',
   PRODUCT_PROMOTION_STATE int default 1 comment '是否促销(推广)',
   primary key (PRODUCT_ID)
);

alter table MALL_PRODUCT comment '商城商品';

/*==============================================================*/
/* Table: MALL_PRODUCT_TYPE                                     */
/*==============================================================*/
create table MALL_PRODUCT_TYPE
(
   PRODUCT_TYPE_ID      varchar(32) not null comment '产品分类ID',
   PRODUCT_TYPE_CODE    varchar(32) comment '产品分类编码',
   PRODUCT_TYPE_NAME    varchar(32) comment '产品分类名称',
   PRODUCT_TYPE_PARENT_ID varchar(32) comment '产品分类上级分类',
   PRODUCT_TYPE_CODE_PATH varchar(250) comment '产品分类编码路径',
   PRODUCT_TYPE_NAME_PATH varchar(250) comment '产品分类名称路径',
   PRODUCT_TYPE_ORDER   numeric(22,0),
   PRODUCT_TYPE_PROMOTION int default 1 comment '是否推荐(推广)',
   PRODUCT_TYPE_IMAGE   varchar(250) comment '产品分类图片',
   primary key (PRODUCT_TYPE_ID)
);

alter table MALL_PRODUCT_TYPE comment '商品分类';

/*==============================================================*/
/* Table: MERCHANT_ACCOUNT                                      */
/*==============================================================*/
create table MERCHANT_ACCOUNT
(
   ACCOUNT_ID           varchar(32) not null comment '帐号ID',
   ACCOUNT_PASSWORD     varchar(32) comment '交易密码',
   ACCOUNT_CREDIT       numeric(22,0) comment '积分',
   ACCOUNT_COIN         numeric(22,0) comment '金币(孝分)',
   primary key (ACCOUNT_ID)
);

alter table MERCHANT_ACCOUNT comment '商家资金帐号';

/*==============================================================*/
/* Table: MERCHANT_APPLY                                        */
/*==============================================================*/
create table MERCHANT_APPLY
(
   APPLY_ID             varchar(32) not null comment '商家申请ID',
   APPLY_SUBMIT_USER_ID varchar(32) comment '商家申请提交人',
   APPLY_SUBMIT_USER_NAME varchar(32) comment '商家申请提交人姓名',
   APPLY_SUBMIT_TIME    datetime comment '商家申请提交时间',
   APPLY_NAME           varchar(250) comment '商家申请商家名称',
   APPLY_USER_NAME      varchar(250) comment '商家申请店主姓名',
   APPLY_USER_MOBILE    varchar(32) comment '商家申请手机号码',
   APPLY_ADDRESS        varchar(250) comment '商家申请商家地址',
   APPLY_TYPE           int comment '商家申请商家类型',
   APPLY_MALL_FLAG      int default 1 comment '商家申请是否申请商城',
   APPLY_PASSWORD       varchar(32) comment '商家申请设置密码',
   APPLY_PASSWORD_CONFIRM varchar(32) comment '商家申请确认密码',
   APPLY_AREA_USER_ID   varchar(32) comment '商家申请区审核人',
   APPLY_AREA_USER_NAME varchar(250) comment '商家申请区审核人姓名',
   APPLY_AREA_TIME      datetime comment '商家申请区审核时间',
   APPLY_CITY_USER_ID   varchar(32) comment '商家申请市审核人',
   APPLY_CITY_USER_NAME varchar(250) comment '商家申请市审核人姓名',
   APPLY_CITY_TIME      datetime comment '商家申请市审核时间',
   APPLY_STATE          int comment '商家申请审核状态',
   MERCHANT_USER_ID     varchar(32) comment '商家申请审核通过创建的商家帐号',
   APPLY_CURRENT_ORG    varchar(32) comment '商家申请当前审核组织',
   primary key (APPLY_ID)
);

alter table MERCHANT_APPLY comment '商家申请';

/*==============================================================*/
/* Table: MERCHANT_DETAIL                                       */
/*==============================================================*/
create table MERCHANT_DETAIL
(
   DETAIL_ID            varchar(32) not null comment '商家ID',
   DETAIL_COVER_IMAGE   varchar(250) comment '商家封面图',
   DETAIL_NAME          varchar(250) comment '商家名称',
   DETAIL_TYPE          varchar(32) comment '商家类型',
   DETAIL_HONESTY_STATE int default 1 comment '商家是否诚信',
   DETAIL_RETURN_TYPE   int comment '商家激励方式',
   DETAIL_AGAIN_STATE   int default 1 comment '商家是否可再消费',
   DETAIL_ADDRESS       varchar(250) comment '商家地址',
   DETAIL_LONGITUDE     decimal(32,6) comment '商家位置-经度',
   DETAIL_LATITUDE      decimal(32,6) comment '商家位置-纬度',
   DETAIL_CONTACT       varchar(32) comment '商家联系电话',
   DETAIL_SUPPORT       varchar(32) comment '商家在线客服',
   DETAIL_BUSINESS_TIME varchar(250) comment '商家营业时间',
   DETAIL_DESCRIPTION   varchar(1000) comment '商家介绍',
   primary key (DETAIL_ID)
);

alter table MERCHANT_DETAIL comment '商家详细信息';

/*==============================================================*/
/* Table: MERCHANT_PHOTO                                        */
/*==============================================================*/
create table MERCHANT_PHOTO
(
   PHOTO_ID             varchar(32) not null comment '图片ID',
   PHOTO_TYPE           int comment '图片分类',
   PHOTO_NAME           varchar(250) comment '图片名称',
   PHOTO_IMAGE          varchar(250) comment '图片地址',
   PHOTO_ORDER          numeric(22,0) comment '图片排序',
   USER_ID              varchar(32) comment '商家ID',
   primary key (PHOTO_ID)
);

alter table MERCHANT_PHOTO comment '商家照片';

/*==============================================================*/
/* Table: MERCHANT_TYPE                                         */
/*==============================================================*/
create table MERCHANT_TYPE
(
   TYPE_ID              varchar(32) not null comment '商家类型ID',
   TYPE_CODE            varchar(32) comment '商家类型编码',
   TYPE_NAME            varchar(250) comment '商家类型名称',
   TYPE_PARENT_ID       varchar(32) comment '商家类型上级',
   TYPE_CODE_PATH       varchar(250) comment '商家类型编码路径',
   TYPE_NAME_PATH       varchar(250) comment '商家类型名称路径',
   TYPE_ORDER           numeric(22,0) comment '商家类型排序',
   TYPE_ENABLED_STATE   int default 1 comment '商家类型是否启用',
   primary key (TYPE_ID)
);

alter table MERCHANT_TYPE comment '商家类型';

/*==============================================================*/
/* Table: MERCHANT_USER                                         */
/*==============================================================*/
create table MERCHANT_USER
(
   USER_ID              varchar(32) not null comment '用户ID',
   USER_CODE            varchar(32) comment '用户编码(用户名)',
   USER_PASSWORD        varchar(32) comment '用户密码',
   USER_NAME            varchar(250) comment '用户姓名',
   USER_MOBILE          varchar(32) comment '用户手机',
   USER_NICKNAME        varchar(250) comment '用户昵称',
   USER_IMAGE           varchar(250) comment '用户图像',
   USER_STATE           int comment '用户状态',
   primary key (USER_ID)
);

alter table MERCHANT_USER comment '商家用户';

/*==============================================================*/
/* Table: OPERATOR_ACCOUNT                                      */
/*==============================================================*/
create table OPERATOR_ACCOUNT
(
   ACCOUNT_ID           varchar(32) not null comment '帐号ID',
   ACCOUNT_PASSWORD     varchar(32) comment '交易密码',
   ACCOUNT_CREDIT       numeric(22,0) comment '积分',
   ACCOUNT_COIN         numeric(22,0) comment '金币(孝分)',
   primary key (ACCOUNT_ID)
);

alter table OPERATOR_ACCOUNT comment '运营资金帐号';

/*==============================================================*/
/* Table: OPERATOR_APPLY                                        */
/*==============================================================*/
create table OPERATOR_APPLY
(
   APPLY_ID             varchar(32) not null comment '运营中心申请ID',
   APPLY_SUBMIT_USER_ID varchar(32) comment '运营中心申请提交人ID',
   APPLY_SUBMIT_USER_TYPE int comment '运营中心申请提交人类型 0:消费者 10:商家 100:代理商 1000:运营商 10000:总部/总公司',
   APPLY_SUBMIT_USER_NAME varchar(32) comment '运营中心申请提交人姓名',
   APPLY_SUBMIT_TIME    datetime comment '运营中心申请提交时间',
   APPLY_PROVINCE_ID    varchar(32) comment '运营中心申请代理省',
   APPLY_CITY_ID        varchar(32) comment '运营中心申请代理市',
   APPLY_AREA_ID        varchar(32) comment '运营中心申请代理区',
   APPLY_CONTACT_NAME   varchar(32) comment '运营中心申请联系人姓名',
   APPLY_CONTACT_PHONE  varchar(32) comment '运营中心申请联系人电话',
   APPLY_DESCRIPTION    varchar(250) comment '运营中心申请备注',
   APPLY_STATE          int comment '运营中心申请状态0:草稿 1:提交审核 10: 审核通过 -1:审核不通过',
   APPLY_COMMENT        varchar(250) comment '运营中心审核意见',
   OPERATOR_CENTER_ID   varchar(32) comment '运营中心ID(审核通过以后创建或者关联运营中心)',
   OPERATOR_USER_ID     varchar(32) comment '运营人员ID(审核通过以后创建或者关联运营帐号)',
   APPLY_CURRENT_ORG    varchar(32) comment '运营中心申请当前在哪个组织(部门) （目前只有总部能审核)',
   APPLY_AUDIT_USER_ID  varchar(32) comment '运营中心审核人ID',
   APPLY_AUDIT_USER_NAME varchar(32) comment '运营中心审核人姓名',
   APPLY_AUDIT_TIME     datetime comment '运营中心审核时间',
   primary key (APPLY_ID)
);

alter table OPERATOR_APPLY comment '运营中心申请';

/*==============================================================*/
/* Table: OPERATOR_CENTER                                       */
/*==============================================================*/
create table OPERATOR_CENTER
(
   CENTER_ID            varchar(32) not null comment '运营中心ID',
   CENTER_CODE          varchar(32) comment '运营中心编码',
   CENTER_NAME          varchar(250) comment '运营中心名称',
   CENTER_PROVINCE_ID   varchar(32) comment '运营中心省',
   CENTER_CITY_ID       varchar(32) comment '运营中心市',
   CENTER_AREA_ID       varchar(32) comment '运营中心区',
   OPERATOR_USER_ID     varchar(32) comment '运营中心运营帐号',
   primary key (CENTER_ID)
);

alter table OPERATOR_CENTER comment '运营中心';

/*==============================================================*/
/* Table: OPERATOR_COLLECT                                      */
/*==============================================================*/
create table OPERATOR_COLLECT
(
   COLLECT_ID           varchar(32) not null comment '运营中心征集ID',
   COLLECT_TYPE         int comment '运营中心征集类型',
   COLLECT_PROVINCE_ID  varchar(32) comment '运营中心征集省',
   COLLECT_CITY_ID      varchar(32) comment '运营中心征集市',
   COLLECT_AREA_ID      varchar(32) comment '运营中心征集区',
   COLLECT_MONEY        numeric(22,0) comment '运营中心征集金额',
   COLLECT_STATE        int comment '运营中心征集状态 0:征集中 1:成功签约',
   primary key (COLLECT_ID)
);

alter table OPERATOR_COLLECT comment '运营中心征集';

/*==============================================================*/
/* Table: OPERATOR_USER                                         */
/*==============================================================*/
create table OPERATOR_USER
(
   USER_ID              varchar(32) not null comment '用户ID',
   USER_CODE            varchar(32) comment '用户编码(用户名)',
   USER_PASSWORD        varchar(32) comment '用户密码',
   USER_NAME            varchar(250) comment '用户姓名',
   USER_MOBILE          varchar(32) comment '用户手机',
   USER_NICKNAME        varchar(250) comment '用户昵称',
   USER_IMAGE           varchar(250) comment '用户图像',
   USER_STATE           int comment '用户状态',
   primary key (USER_ID)
);

alter table OPERATOR_USER comment '运营用户';