-- Dumping structure for table fitz.app_permission
CREATE TABLE IF NOT EXISTS `app_permission` (
  `PERMISSION_ID` varchar(32) NOT NULL COMMENT '权限ID',
  `PERMISSION_PARENT_ID` varchar(32) DEFAULT NULL COMMENT '上级权限ID',
  `PERMISSION_CODE` varchar(250) DEFAULT NULL COMMENT '权限标识',
  `PERMISSION_NAME` varchar(250) DEFAULT NULL COMMENT '权限名称',
  `PERMISSION_COMMENT` varchar(250) DEFAULT NULL COMMENT '权限描述',
  `PERMISSION_ORDER` int(11) DEFAULT NULL COMMENT '权限级排序',
  `PERMISSION_ICON` varchar(32) DEFAULT NULL COMMENT '菜单图标',
  `PERMISSION_URL` varchar(250) DEFAULT NULL COMMENT '菜单URL',
  `PERMISSION_TYPE` int(11) DEFAULT NULL COMMENT '权限类型 0=系统 1=目录 2=菜单 3=按钮',
  `PERMISSION_STATE` int(11) DEFAULT NULL COMMENT '权限状态 0启用 1停用',
  PRIMARY KEY (`PERMISSION_ID`),
  KEY `IDX_APP_PERMISSION_CODE` (`PERMISSION_CODE`),
  KEY `IDX_APP_PERMISSION_PARENT_ID` (`PERMISSION_PARENT_ID`)
) COMMENT='系统权限包括菜单及功能控制';

-- Dumping data for table fitz.app_permission: ~4 rows (approximately)
DELETE FROM `app_permission`;
INSERT INTO `app_permission` (`PERMISSION_ID`, `PERMISSION_PARENT_ID`, `PERMISSION_CODE`, `PERMISSION_NAME`, `PERMISSION_COMMENT`, `PERMISSION_ORDER`, `PERMISSION_ICON`, `PERMISSION_URL`, `PERMISSION_TYPE`, `PERMISSION_STATE`) VALUES
	('1', '0', 'SYSTEM', '系统管理', NULL, 1, NULL, NULL, 0, 1),
	('2', '1', 'app:appRole:index', '角色管理', '', 1, '', 'app/appRole/index.do', 2, 1),
	('3', '1', 'app:appPermission:index', '权限管理', NULL, 2, NULL, 'app/appPermission/index.do', 2, 1),
	('4', '1', 'app:appUser:index', '用户管理', NULL, 1, NULL, 'app/appUser/index.do', 2, 1);


-- Dumping structure for table fitz.app_role
CREATE TABLE IF NOT EXISTS `app_role` (
  `ROLE_ID` varchar(32) NOT NULL COMMENT '角色ID',
  `ROLE_CODE` varchar(32) DEFAULT NULL COMMENT '角色编码',
  `ROLE_NAME` varchar(250) DEFAULT NULL COMMENT '角色名称',
  `ROLE_COMMENT` varchar(250) DEFAULT NULL COMMENT '角色描述',
  `ROLE_STATE` int(11) DEFAULT NULL COMMENT '角色状态 0启用 1停用',
  PRIMARY KEY (`ROLE_ID`),
  KEY `IDX_APP_ROLE_CODE` (`ROLE_CODE`)
) COMMENT='系统角色';

-- Dumping data for table fitz.app_role: ~1 rows (approximately)
DELETE FROM `app_role`;
INSERT INTO `app_role` (`ROLE_ID`, `ROLE_CODE`, `ROLE_NAME`, `ROLE_COMMENT`, `ROLE_STATE`) VALUES
	('admin', 'admin', '管理员', '管理员', 1);


-- Dumping structure for table fitz.app_role_permission
CREATE TABLE IF NOT EXISTS `app_role_permission` (
  `ROLE_ID` varchar(32) NOT NULL COMMENT '角色id',
  `PERMISSION_ID` varchar(32) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`ROLE_ID`,`PERMISSION_ID`)
);

-- Dumping data for table fitz.app_role_permission: ~4 rows (approximately)
DELETE FROM `app_role_permission`;
INSERT INTO `app_role_permission` (`ROLE_ID`, `PERMISSION_ID`) VALUES
	('admin', '1'),
	('admin', '2'),
	('admin', '3'),
	('admin', '4');


-- Dumping structure for table fitz.app_user
CREATE TABLE IF NOT EXISTS `app_user` (
  `USER_ID` varchar(32) NOT NULL COMMENT 'ID',
  `USER_CODE` varchar(32) DEFAULT NULL COMMENT '用户编码',
  `USER_PASSWORD` varchar(32) DEFAULT NULL COMMENT '用户密码',
  `USER_COMMENT` varchar(250) DEFAULT NULL COMMENT '用户描述',
  `USER_NAME` varchar(250) DEFAULT NULL COMMENT '用户实名',
  `USER_STATE` int(11) DEFAULT NULL COMMENT '用户状态 0启用 1停用',
  `USER_TYPE` int(11) DEFAULT NULL COMMENT '用户类型 0普通用户 1管理用户',
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `IDX_APP_USER_CODE` (`USER_CODE`)
) COMMENT='系统用户';

-- Dumping data for table fitz.app_user: ~1 rows (approximately)
DELETE FROM `app_user`;
INSERT INTO `app_user` (`USER_ID`, `USER_CODE`, `USER_PASSWORD`, `USER_COMMENT`, `USER_NAME`, `USER_STATE`, `USER_TYPE`) VALUES
	('fitz', 'fitz', '0C1815CD56F54D1FBC4B4ECFBF942E84', NULL, 'fitz', 1, NULL);


-- Dumping structure for table fitz.app_user_role
CREATE TABLE IF NOT EXISTS `app_user_role` (
  `USER_ID` varchar(32) NOT NULL COMMENT '用户id',
  `ROLE_ID` varchar(32) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`USER_ID`,`ROLE_ID`)
) COMMENT='系统用户角色';

-- Dumping data for table fitz.app_user_role: ~1 rows (approximately)
DELETE FROM `app_user_role`;
INSERT INTO `app_user_role` (`USER_ID`, `ROLE_ID`) VALUES
	('fitz', 'admin');