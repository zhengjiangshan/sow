/*==============================================================*/
/* Table: OPERATOR_WELFARE_APPLY                                */
/*==============================================================*/
create table OPERATOR_WELFARE_APPLY
(
   WELFARE_ID           varchar(32) not null comment '主键',
   WELFARE_APPLY_ID     varchar(32) comment '申请人ID',
   WELFARE_PROVINCE_ID  varchar(32) comment '省ID',
   WELFARE_CITY_ID      varchar(32) comment '市ID',
   WELFARE_AREA_ID      varchar(32) comment '区县ID',
   WELFARE_PERSON_NAME  varchar(250) comment '联系人姓名',
   WELFARE_PERSON_PHONE varchar(32) comment '联系人电话',
   WELFARE_DESCRIPTION  varchar(1000) comment '内容描述',
   WELFARE_AREA_USER_ID varchar(32) comment '区县审核人ID',
   WELFARE_AREA_USER_NAME varchar(250) comment '区县审核人姓名',
   WELFARE_AREA_TIME    datetime comment '区县审核时间',
   WELFARE_CITY_USER_ID varchar(32) comment '市审核人ID',
   WELFARE_CITY_USER_NAME varchar(250) comment '市审核人姓名',
   WELFARE_CITY_TIME    datetime comment '市审核时间',
   WELFARE_PROVINCE_USER_ID varchar(32) comment '省审核人ID',
   WELFARE_PROVINCE_USER_NAME varchar(250) comment '省审核人姓名',
   WELFARE_PROVINCE_TIME datetime comment '省审核时间',
   WELFARE_STATE        int comment '状态 0 草稿 1 区级审核中 2 区级审核驳回 3 市级审核中 4 市级审核驳回 5 省级审核中 6 省级审核驳回',
   WELFARE_AREA_COMMENT varchar(250) comment '区县审核意见',
   WELFARE_CITY_COMMENT varchar(250) comment '市级审核意见',
   WELFARE_PROVINCE_COMMENT varchar(250) comment '省级审核意见',
   WELFARE_BANK_ACCOUNT varchar(250) comment '公益金打款银行账户',
   WELFARE_BANK_NAME    varchar(250) comment '公益金打款银行名称',
   primary key (WELFARE_ID)
);

alter table OPERATOR_WELFARE_APPLY comment '公益金申请';
