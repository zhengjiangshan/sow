/*==============================================================*/
/* Table: GETUI_APP                                             */
/*==============================================================*/
create table GETUI_APP
(
   APP_ID               varchar(32) not null comment '个推应用ID',
   APP_KEY              varchar(32) comment '个推应用KEY',
   APP_SECRET           varchar(32) comment '个推应用SECRET',
   APP_MASTER_SECRET    varchar(32) comment '个推应用MASTERSECRET',
   APP_CODE             varchar(32) comment '个推应用编码',
   primary key (APP_ID)
);

alter table GETUI_APP comment '个推APP';

/*==============================================================*/
/* Table: GETUI_CLIENT                                          */
/*==============================================================*/
create table GETUI_CLIENT
(
   USER_ID              varchar(32) not null comment '关联系统用户ID',
   APP_ID               varchar(32) not null comment '个推应用ID',
   USER_TYPE            int comment '关联系统用户类型',
   CLIENT_ID            varchar(32) not null comment '个推客户端ID',
   primary key (USER_ID, APP_ID)
);

alter table GETUI_CLIENT comment '个推客户端';
