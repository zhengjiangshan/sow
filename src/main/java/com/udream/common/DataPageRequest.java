package com.udream.common;

import java.util.Date;

/**
 * 数据分页请求.
 * <p>
 * page:每页显示数量.
 * </p>
 * <p>
 * timeBefore:时间之前(例如:指定客户端列表最后一条记录的时间，用于加载更多历史数据)
 * </p>
 * <p>
 * timeAfter:时间之后(例如:指定客户端数据列表第一条记录的时间，用于加载最新的数据)
 * (备注:客户端第一次展示列表数据，两个时间都不需要指定，返回最新的一页数据）
 * </p>
 * 
 * @author zhoupan
 *
 */
public class DataPageRequest extends DataRequest {

 private static final long serialVersionUID = 1L;

 /** The Constant DEFAULT_PAGE_SIZE. */
 public static final int DEFAULT_PAGE_SIZE = 10;

 /** The Constant MAX_PAGE_SIZE. */
 public static final int MAX_PAGE_SIZE = 5000;

 /**
  * 每页显示数量.
  */
 private Integer pageSize;

 public DataPageRequest pageSize(Integer pageSize) {
  this.setPageSize(pageSize);
  return this;
 }

 /**
  * 时间之前.
  */
 private Long timeBefore;

 public DataPageRequest timeBefore(Date timeBefore) {
  this.setTimeBefore(timeBefore.getTime());
  return this;
 }

 public DataPageRequest timeBefore(long date) {
  this.setTimeBefore(date);
  return this;
 }

 /**
  * 时间之后.
  */
 private Long timeAfter;

 public DataPageRequest timeAfter(long timeAfter) {
  this.setTimeAfter(timeAfter);
  return this;
 }

 public DataPageRequest timeAfter(Date timeAfter) {
  this.setTimeAfter(timeAfter.getTime());
  return this;
 }

 /**
  * 如果没有指定pageSize,或者超过最大的pageSize.返回默认的pageSize.
  *
  * @return the page size
  */
 public Integer getPageSize() {
  if (pageSize == null || pageSize > MAX_PAGE_SIZE) {
   return DEFAULT_PAGE_SIZE;
  }
  return pageSize;
 }

 /**
  * Sets the page size.
  *
  * @param pageSize
  *         the page size
  */
 public void setPageSize(Integer pageSize) {
  this.pageSize = pageSize;
 }

 /**
  * Gets the time before.
  *
  * @return the time before
  */
 public Long getTimeBefore() {
  return timeBefore;
 }

 /**
  * Sets the time before.
  *
  * @param timeBefore
  *         the time before
  */
 public void setTimeBefore(Long timeBefore) {
  this.timeBefore = timeBefore;
 }

 /**
  * Gets the time after.
  *
  * @return the time after
  */
 public Long getTimeAfter() {
  return timeAfter;
 }

 /**
  * Sets the time after.
  *
  * @param timeAfter
  *         the time after
  */
 public void setTimeAfter(Long timeAfter) {
  this.timeAfter = timeAfter;
 }

}
