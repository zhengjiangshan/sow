package com.udream.common;

import org.hibernate.validator.constraints.NotBlank;

/**
 * 公用请求 携带店铺ID
 */
public class DataRequestShopSupport extends DataRequest {
 
 @NotBlank(message = "{shop.id.cannot.null}")
 private String shopId;
 
 public String getShopId() {
  return shopId;
 }
 
 public void setShopId(String shopId) {
  this.shopId = shopId;
 }
}
