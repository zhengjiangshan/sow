package com.udream.common;

/**
 * String简单工具类
 *
 */
public class StringUtil {

 /**
  * 验证字符串是否为空
  * 
  * @param str
  * @return false不为空 true为空
  */
 public static boolean isEmpty(String str) {
  if (null == str) {
   return true;
  }
  if ("".equals(str.trim())) {
   return true;
  }
  return false;
 }

 /**
  * 字符串转换unicode
  */
 public static String string2Unicode(String string) {

  StringBuffer unicode = new StringBuffer();

  for (int i = 0; i < string.length(); i++) {

   // 取出每一个字符
   char c = string.charAt(i);

   // 转换为unicode
   unicode.append("\\u" + Integer.toHexString(c));
  }

  return unicode.toString();
 }

 /**
  * unicode 转字符串
  */
 public static String unicode2String(String unicode) {

  StringBuffer string = new StringBuffer();

  String[] hex = unicode.split("\\\\u");

  for (int i = 1; i < hex.length; i++) {

   // 转换出每一个代码点
   int data = Integer.parseInt(hex[i], 16);

   // 追加成string
   string.append((char) data);
  }

  return string.toString();
 }

 public static void main(String[] args) {
  String hstr = "科技时代卡萨丁";
  String str = "\u79d1\u6280\u65f6\u4ee3\u5361\u8428\u4e01";
  System.out.println("111" + unicode2String(str));
  System.out.println("222" + string2Unicode(hstr));
 }
}
