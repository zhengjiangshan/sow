package com.udream.common;

/**
 * 保存每个调用API的客户端信息。包括Android客户端/iOS客户端/Web客户端.
 * 
 * 
 * @author zhoupan
 *
 */
public class ClientIdRequest {

 /**
  * 客户端类型:0=Android,10=IOS,100=Web
  */
 private String type;

 /**
  * 客户端系统版本.Android或者IOS系统版本号.
  */
 private String version;
 /**
  * 客户端型号 .手机型号
  */
 private String model;
 /**
  * 客户端设备ID.(可不填,能获取到就获取)
  */
 private String sn;

 public String getType() {
  return type;
 }

 public void setType(String type) {
  this.type = type;
 }

 public String getVersion() {
  return version;
 }

 public void setVersion(String version) {
  this.version = version;
 }

 public String getModel() {
  return model;
 }

 public void setModel(String model) {
  this.model = model;
 }

 public String getSn() {
  return sn;
 }

 public void setSn(String sn) {
  this.sn = sn;
 }

}
