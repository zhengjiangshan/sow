package com.udream.common;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DataPageListResponse<T> {
 /***
  * 当前页数
  */
 private Integer pageNum = 1;

 /***
  * 总记录条数
  */
 private Long total = 0L;
 @JsonProperty("list")
 private List<T> items = new ArrayList<T>();

 public List<T> getItems() {
  return items;
 }

 public void setItems(List<T> items) {
  this.items = items;
 }

 public Integer getPageNum() {
  return pageNum;
 }

 public void setPageNum(Integer pageNum) {
  this.pageNum = pageNum;
 }

 public Long getTotal() {
  return total;
 }

 public void setTotal(Long total) {
  this.total = total;
 }
}
