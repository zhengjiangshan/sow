package com.udream.common;

import java.io.Serializable;

public class MessageData implements Serializable{

 private static final long serialVersionUID = -8815176375708438166L;

 private Integer code;
 private String title;
 private String message;

 public Integer getCode() {
  return code;
 }

 public void setCode(Integer code) {
  this.code = code;
 }

 public String getTitle() {
  return title;
 }

 public void setTitle(String title) {
  this.title = title;
 }

 public String getMessage() {
  return message;
 }

 public void setMessage(String message) {
  this.message = message;
 }
}
