package com.udream.common;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 数据请求.
 * 
 * @author zhoupan
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataRequest implements java.io.Serializable, ClientTokenSupport {

 private static final long serialVersionUID = 1L;

 @NotNull(message = "{common.validate.field.not_null}")
 private String clientId;
 @NotNull(message = "{common.validate.field.not_null}")
 private String token;

 public String getClientId() {
  return clientId;
 }

 public void setClientId(String clientId) {
  this.clientId = clientId;
 }

 public String getToken() {
  return token;
 }

 public void setToken(String token) {
  this.token = token;
 }

}
