package com.udream.common;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 通用数据响应模板
 */
public class ResponseTemplate extends DataResponse {

 @JsonProperty("data")
 private Map<String, Object> data;

 public Map<String, Object> getData() {
  return data;
 }

 public void setData(Map<String, Object> data) {
  this.data = data;
 }
}
