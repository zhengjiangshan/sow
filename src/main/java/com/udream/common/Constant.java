package com.udream.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindingResult;

import com.udream.support.DataStatusException;

/**
 * 项目内常量定义.
 */
public class Constant {
 
 /**
  * The Constant ROLE_OPERATOR_PROVINCE.
  */
 public static final String ROLE_OPERATOR_PROVINCE = "OPERATOR_PROVINCE";
 
 /**
  * The Constant ROLE_OPERATOR_CITY.
  */
 public static final String ROLE_OPERATOR_CITY = "OPERATOR_CITY";
 
 /**
  * The Constant ROLE_OPERATOR_AREA.
  */
 public static final String ROLE_OPERATOR_AREA = "OPERATOR_AREA";
 
 /**
  * 消费者.
  */
 public static final int USER_TYPE_CONSUMER = 0;
 
 /**
  * 商家.
  */
 public static final int USER_TYPE_MERCHANT = 10;
 
 /**
  * 代理商.
  */
 public static final int USER_TYPE_AGENT = 100;
 
 /**
  * 运营商.
  */
 public static final int USER_TYPE_OPERATOR = 1000;
 
 /**
  * 运营商申请banner.
  */
 public static final int USER_TYPE_1001 = 1001;
 
 /**
  * 总部/总公司.
  */
 public static final int USER_TYPE_HQ = 10000;
 
 /**
  * 成功.
  */
 public static final int RETURN_CODE_SUCCESS = 0;
 
 /**
  * 失败.
  */
 public static final int RETURN_CODE_FAIL = 1;
 
 /**
  * token过期错误代码.
  */
 public static final int RETURN_CODE_TOKEN_EXPIRED = 1000;
 
 /**
  * token过期时间.
  */
 public static final int RETURN_CODE_TOKEN_TIMEOUT = 7200000;// 2小时
 
 /**
  * token过期错误提示.
  */
 public static final String RETURN_MSG_TOKEN_EXPIRED = "token过期";
 
 /**
  * clientId无效.
  */
 public static final int RETURN_CODE_CLIENT_ID_INVALID = 2000;
 
 /**
  * clientId无效提示.
  */
 public static final String RETURN_MSG_CLIENT_ID_INVALID = "clientId无效";
 
 /**
  * yes
  */
 public static final int STATE_YES = 1;
 
 /**
  * no.
  */
 public static final int STATE_NO = 0;
 
 /**
  * 短信验证码长度.
  */
 public static final int APP_CODE_SMS_LENGTH = 4;
 
 /**
  * 短信验证码过期时间(分钟).
  */
 public static final int APP_CODE_SMS_EXPIRED_MINUTE = 10;
 
 /**
  * 抛出token过期异常.
  */
 public static void throwTokenExpiredException() {
  throw new DataStatusException(RETURN_CODE_TOKEN_EXPIRED, RETURN_MSG_TOKEN_EXPIRED);
 }
 
 /**
  * 抛出client无效异常.
  */
 public static void throwClientIdInvalidException() {
  throw new DataStatusException(RETURN_CODE_CLIENT_ID_INVALID, RETURN_MSG_CLIENT_ID_INVALID);
 }
 
 /**
  * 抛出绑定结果异常.
  *
  * @param result
  */
 public static void checkBindingResult(BindingResult result) {
  if (result.hasErrors()) {
   throw new DataStatusException(Constant.RETURN_CODE_FAIL, ErrorMessageHelper.praseErrorMessage(result.getAllErrors()));
  }
 }
 
 /**
  * 手机号码正则表达式
  */
 public static final String MOBILE_REGX = "^0?(13[0-9]|15[012356789]|17[013678]|18[0-9]|14[57])[0-9]{8}$";
 
 /**
  * 邮箱正则表达式
  */
 public static final String EMAIL_REGX = "^[A-Za-z0-9\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
 
 
 // 前端 孝友申请状态{ 0:全部 1:待审核 2:审核中3:已通过4:已驳回
 /**
  * 全部
  */
 public static final int FRONT_AGENT_STATE_ALL = 0;
 /**
  * 待审核
  */
 public static final int FRONT_AGENT_STATE_WILL_APPROVE = 1;
 /**
  * 审核中
  */
 public static final int FRONT_AGENT_STATE_DO_APPROVE = 2;
 /**
  * 已通过
  */
 public static final int FRONT_AGENT_STATE_SUCCESS = 3;
 /**
  * 已驳回
  */
 public static final int FRONT_AGENT_STATE_REJECT = 4;
 // 前端 孝友申请状态}
 
 /**
  * 普通代理商（孝友）
  */
 public static final int AGENT_GRADE_NORMAL = 0;
 
 /**
  * 至尊孝友（代理商）
  */
 public static final int AGENT_GRADE_VIP = 1;
 
 /**
  * 1 区审核中
  */
 public static final String RAISE_STATUS_1 = "1";
 /**
  * 2 区审核驳回
  */
 public static final String RAISE_STATUS_2 = "2";
 /**
  * 3 市审核中
  */
 public static final String RAISE_STATUS_3 = "3";
 /**
  * 4 市审核驳回
  */
 public static final String RAISE_STATUS_4 = "4";
 /**
  * 5 省审核中
  */
 public static final String RAISE_STATUS_5 = "5";
 /**
  * 6 省审核驳回
  */
 public static final String RAISE_STATUS_6 = "6";
 /**
  * 7审核通过
  */
 public static final String RAISE_STATUS_7 = "7";
 
 /**
  * 默认的举报类型（全部）
  */
 public static final Integer DEFAULT_REPORT_TYPE = 0;
 
 /**
  * 举报运营商类型
  */
 public static final Integer REPORT_OPERAOTR_TYPE = 4;
 
 /**
  * 举报代理商（孝友）类型
  */
 public static final Integer REPORT_AGENT_TYPE = 3;
 
 /**
  * 举报商户类型
  */
 public static final Integer REPORT_MERCHANT_TYPE = 2;
 
 /**
  * 举报消费者类型
  */
 public static final Integer REPORT_CONSUMER_TYPE = 1;
 
 /**
  * 默认的举报状态（全部）
  */
 public static final Integer DEFAULT_REPORT_STATE = 0;
 
 /**
  * 举报已经处理状态
  */
 public static final Integer REPORT_ALREADY_HANDLE = 1;
 
 /**
  * 举报等待处理状态
  */
 public static final Integer REPORT_PENDING_HANDLE = 2;
 
 /**
  * 举报“处理中”
  */
 public static final Integer REPORT_IN_PROCESS = 3;
 
 /**
  * 运营商类型-省级
  */
 public static final Integer OPERATOR_PROVINCE_TYPE = 0;
 
 /**
  * 运营商类型-市级
  */
 public static final Integer OPERATOR_CITY_TYPE = 1;
 
 /**
  * 运营商类型-区级
  */
 public static final Integer OPERATOR_AREA_TYPE = 2;
 
 /**
  * 总部在组织表中的org_code(代码)
  */
 public static final String ORG_HQ_CODE = "0";
 
 /**
  * 总部在组织表中的ID
  */
 public static final String ORG_HQ_ID = "0";
 
 /**
  * 总部在组织表中的路径（ORG_PATH）
  */
 public static final String ORG_HQL_PATH = "0";
 
 /**
  * 举报在图片资源表中的CLASS名称
  */
 public static final String APP_REPORT_IMG_CLASS_NAME = "AppReport";
 
 /**
  * 密码登录
  */
 public static final int LOGIN_TYPE_PASS = 1;
 
 /**
  * 短信验证码登录
  */
 public static final int LOGIN_TYPE_CODE = 2;
 
 /**
  * 第三方登录
  */
 public static final int LOGIN_TYPE_OPEN = 3;
 
 /**
  * 商品状态：上架
  */
 public static final int PRODUCT_STATUS_SHELVES = 0;
 
 /**
  * 商品状态：下架
  */
 public static final int PRODUCT_STATUS_OFF_SHELF = 1;
 
 /**
  * 商品状态：禁用
  */
 public static final int PRODUCT_STATUS_DISABLE = 2;
 
 /**
  * 产品状态：异常
  */
 public static final int PRODUCT_STATUS_EXCEPTION = 3;
 
 /**
  * 产品状态：所有
  */
 public static final int PRODUCT_STATUS_ALL = 4;
 
 /**
  * 订单（待支付）
  */
 public static final int ORDER_STATUS_1 = 1;
 /**
  * 订单（待发货）
  */
 public static final int ORDER_STATUS_2 = 2;
 /**
  * 订单（待收货）
  */
 public static final int ORDER_STATUS_3 = 3;
 /**
  * 订单（待评价）
  */
 public static final int ORDER_STATUS_4 = 4;
 /**
  * 订单（退货中）
  */
 public static final int ORDER_STATUS_5 = 5;
 /**
  * 订单（退款中 ）
  */
 public static final int ORDER_STATUS_6 = 6;
 /**
  * 订单（换货中）
  */
 public static final int ORDER_STATUS_7 = 7;
 /**
  * 订单（已关闭）
  */
 public static final int ORDER_STATUS_8 = 8;
 /**
  * 订单（已完成）
  */
 public static final int ORDER_STATUS_9 = 9;
 
 /**
  * 订单（所有状态）
  */
 public static final int ORDER_STATE_ALL = 0;
 
 /**
  * 订单类型（自营）
  */
 public static final int ORDER_TYPE_MINE = 1;
 
 /**
  * 订单类型（线上）
  */
 public static final int ORDER_TYPE_ONLINE = 2;
 
 /**
  * 订单类型（全部）
  */
 public static final int ORDER_TYPE_ALL = 0;
 
 /**
  * 订单申请了售后
  */
 public static final int ORDER_HAS_AFTER_SALE = 1;
 /**
  * 订单未申请兽兽
  */
 public static final int ORDER_DOSENT_AFTER_SALE = -1;
 
 /**
  * 退款售后类型 退款
  */
 public static final int ORDER_AFTER_SALE_TYPE_BACK_MONEY = 1;
 
 /**
  * 退款售后类型 退货
  */
 public static final int ORDER_AFTER_SALE_TYPE_BACK_GOODS = 2;
 
 /**
  * 退款售后类型 换货
  */
 public static final int ORDER_AFTER_SALE_TYPE_CHANGE_GOODS = 3;
 
 /**
  * 订单支付类型 支付宝
  */
 public static final int ORDER_PAY_TYPE_ALIPAY = 1;
 
 /**
  * 订单支付类型 微信
  */
 public static final int ORDER_PAY_TYPE_WECHAT = 2;
 
 /**
  * 默认永久额度
  */
 public static final double MERCHANT_FOR_RAISE = 500;
 /**
  * 默认临时额度
  */
 public static final double MERCHANT_TEM_RAISE = 500;
 
 /**
  * 产品类型自营
  */
 public static final int PRODUCT_TYPE_OFFLINE = 2;
 
 /**
  * 产品类型线上
  */
 public static final int PRODUCT_TYPE_ONLINE = 1;
 
 /**
  * 产品类型全部
  */
 public static final int PRODUCT_TYPE_ALL = 0;
 
 /**
  * 商品审核状态：待处理
  */
 public static final int PRODUCT_APPROVE_STATUS_PROCCDING = 2;
 
 /**
  * 商品审核状态:审核通过
  */
 public static final int PRODUCT_APPROVE_STATUS_PASS = 1;
 
 /**
  * 商品审核状态：审核不通过
  */
 public static final int PRORDUCT_APPROVE_STATUS_REJECT = 0;
 
 /**
  * 线下订单状态 未结算
  */
// public static final int ORDER_OFFLINE_STATE_NOTPAY = -1;
 public static final int ORDER_OFFLINE_STATE_NOTPAY = 0;
 
 /**
  * 线下订单状态 已结算
  */
 public static final int ORDER_OFFLINE_STATE_ALREADY_PAY = 1;
 
 /**
  * 先下订支付方式  现金
  */
 public static final int ORDER_OFFLINE_PAY_WAY_CASH = 1;
 /**
  * 先下订支付方式  银行卡
  */
 public static final int ORDER_OFFLINE_PAY_WAY_BANKCARD = 2;
 /**
  * 先下订支付方式  支付宝
  */
 public static final int ORDER_OFFLINE_PAY_WAY_ALIPAY = 3;
 /**
  * 先下订支付方式  微信
  */
 public static final int ORDER_OFFLINE_PAY_WAY_WECHAT = 4;
 /**
  * 先下订支付方式  其他
  */
 public static final int ORDER_OFFLINE_PAY_WAY_OTHER = 5;
 /**
  * 线下订单未返还积分
  */
 public static final int ORDER_OFFLIN_JIFEN_NOT_GIVEN = 0;
 /**
  * 线下订单已经返还积分
  */
 public static final int ORDER_OFFLIN_JIFEN_HAS_GIVEN = 1;
 /**
  * 线下订单：创建录单
  */
 public static final int ORDER_OFFLIN_TYPE_OFFLINE = 1;
 /**
  * 线下订单：收款码创建
  */
 public static final int ORDER_OFFLIN_TYPE_QRCODE = 2;
 
 /**
  * 已赠送积分
  */
 public static final int ORDER_IS_YSE_GIVE = 1;
 
 /**
  * 未赠送积分
  */
 public static final int ORDER_IS_NOT_GIVE = -1;
 
 /**
  * 现金
  */
 public static final int RECORD_TYPE_MONEY = 0;
 
 /**
  * 积分
  */
 public static final int RECORD_TYPE_JIFEN = 1;
 
 /**
  * 孝分
  */
 public static final int RECORD_TYPE_XIAOFEN = 2;
 
 /**
  * 退款申请通过
  */
 public static final int MALL_BACK_MONEY_STATUS_PASS = 1;
 /**
  * 退款申请拒绝
  */
 public static final int MALL_BACK_MONEY_STATUS_REJECT = -1;
 /**
  * 退款申请待审核
  */
 public static final int MALL_BACK_MONEY_STATUS_WAIT = 2;
 
 /**
  * 默认收获地址
  */
 public static final int ADDRESS_IS_MAIN = 1;
 
 /**
  * 非默认收获地址
  */
 public static final int ADDRESS_IS_NUT_MAIN = -1;
 
 /**
  * 消费
  */
 public static final int RECORD_PAY_SOURCE_XIAOFEI = 0;
 /**
  * 返还
  */
 public static final int RECORD_PAY_SOURCE_FANHAI = 1;
 /**
  * 退款
  */
 public static final int RECORD_PAY_SOURCE_TUIKUAN = 2;
 /**
  * 积分转换孝分
  */
 public static final int RECORD_PAY_SOURCE_JZHUANX = 3;
 /**
  * 提现
  */
 public static final int RECORD_PAY_SOURCE_TIXIAN = 4;
 /**
  * 退还可提现
  */
 public static final int RECORD_PAY_SOURCE_TKTX = 5;
 /**
  * 退还可消费
  */
 public static final int RECORD_PAY_SOURCE_TKXF = 6;
 /**
  * 消费可提现
  */
 public static final int RECORD_PAY_SOURCE_XFKTX = 7;
 /**
  * 消费可消费
  */
 public static final int RECORD_PAY_SOURCE_XFKXF = 8;
 /**
  * 转换可消费
  */
 public static final int RECORD_PAY_SOURCE_ZHKXF = 9;
 /**
  * 转换可提现
  */
 public static final int RECORD_PAY_SOURCE_ZHKTX = 10;
 /**
  * 推荐人孝分
  */
 public static final int RECORD_PAY_SOURCE_TJRXF = 11;
 
 /**
  * 退货 商户待审
  */
 public static final int BACK_STATUS_MERCHANT_PENDING_AUDIT = 1;
 
 /**
  * 退货 用户待发货
  */
 public static final int BACK_STATUS_CONSUMER_PENDING_POST = 2;
 /**
  * 退货 商户待收货
  */
 public static final int BACK_STATUS_MERCHANT_PENDING_DELIVERY = 3;
 
 /**
  * 退货 商户待退款
  */
 public static final int BACK_STATUS_MERCHANT_PENDING_BACK_MONEY = 4;
 /**
  * 退货 完成
  */
 public static final int BACK_STATUS_SUCCEED = 5;
 /**
  * 退货 商户驳回
  */
 public static final int BACK_STATUS_MERCHANT_REJECT = 6;
 /**
  * 退货 撤销
  */
 public static final int BACK_STATUS_MERCHANT_CANCELED = 7;
 
 /**
  * 用户申请换货
  */
 public static final int MALL_CHANGE_STATUS_1 = 1;
 /**
  * 商家发货
  */
 public static final int MALL_CHANGE_STATUS_2 = 2;
 /**
  * 用户确认收货
  */
 public static final int MALL_CHANGE_STATUS_3 = 3;
 /**
  * 商家拒绝
  */
 public static final int MALL_CHANGE_STATUS_4 = 4;
 /**
  * 商家同意换货
  */
 public static final int MALL_CHANGE_STATUS_5 = 5;
 /**
  * 用户待发货
  */
 public static final int MALL_CHANGE_STATUS_6 = 6;
 /**
  * 商家待收货
  */
 public static final int MALL_CHANGE_STATUS_7 = 7;
 /**
  * 商家待发货
  */
 public static final int MALL_CHANGE_STATUS_8 = 8;
 
 /**
  * 物流类型 1用户换货
  */
 public static final int LOGISTICS_TYPE_1 = 1;
 /**
  * 物流类型 2用户换货-商家发货
  */
 public static final int LOGISTICS_TYPE_2 = 2;
 /**
  * 物流类型 3商家发货
  */
 public static final int LOGISTICS_TYPE_3 = 3;
 /**
  * 物流类型 4用户退款退货
  */
 public static final int LOGISTICS_TYPE_4 = 4;
 
 /**
  * 商家申请-状态 草稿
  */
 public static final int MERCHANT_APPLY_STATUS_0 = 0;
 /**
  * 商家申请-状态 商家提交
  */
 public static final int MERCHANT_APPLY_STATUS_1 = 1;
 /**
  * 商家申请-状态 区审核通过
  */
 public static final int MERCHANT_APPLY_STATUS_2 = 2;
 /**
  * 商家申请-状态 区审核驳回
  */
 public static final int MERCHANT_APPLY_STATUS_3 = 3;
 /**
  * 商家申请-状态 市审核通过
  */
 public static final int MERCHANT_APPLY_STATUS_4 = 4;
 /**
  * 商家申请-状态 市审核驳回
  */
 public static final int MERCHANT_APPLY_STATUS_5 = 5;
 /**
  * 商家申请-状态 省审核通过
  */
 public static final int MERCHANT_APPLY_STATUS_6 = 6;
 /**
  * 商家申请-状态 省审核驳回
  */
 public static final int MERCHANT_APPLY_STATUS_7 = 7;
 
 /**
  * 商家申请-状态 总部审核通过
  */
 public static final int MERCHANT_APPLY_STATUS_8 = 8;
 /**
  * 商家申请-状态 总部审核不通过
  */
 public static final int MERCHANT_APPLY_STATUS_9 = 9;
 
 public static final String KUAIDI_100_PARAM = "&id=1&valicode&temp=0.08001715072286408";
 
 /**
  * 支付宝状态：交易创建，等待买家付款
  */
 public static final int ALIPAY_TRADE_STATUS_WAIT_BUYER_PAY = 0;
 /**
  * 支付宝状态：未付款交易超时关闭，或支付完成后全额退款
  */
 public static final int ALIPAY_TRADE_STATUS_TRADE_CLOSED = 1;
 /**
  * 支付宝状态：交易支付成功
  */
 public static final int ALIPAY_TRADE_STATUS_TRADE_SUCCESS = 2;
 /**
  * 支付宝状态：交易结束，不可退款
  */
 public static final int ALIPAY_TRADE_STATUS_TRADE_FINISHED = 3;
 
 /**
  * 消费端提现
  */
 public static final int APP_PAY_OFFLINE_TYPE_1 = 1;
 /**
  * 运营端提现
  */
 public static final int APP_PAY_OFFLINE_TYPE_2 = 2;
 /**
  * 公益金
  */
 public static final int APP_PAY_OFFLINE_TYPE_3 = 3;
 
 /**
  * 初始化个推消息
  */
 public static List<MessageData> messageList = new ArrayList<MessageData>();
 
 static {
  MessageData info = null;
  for (int i = 1; i < 30; i++) {
   info = new MessageData();
   info.setCode(i);
   info.setTitle(i + "_title");
   info.setMessage(i + "_message");
  }
 }
 
 /**
  * 审核中
  */
 public static final String APPROVE_INPROCESS = "审核中";
 
 /**
  * 审核通过
  */
 public static final String APPROVE_PASSED = "审核通过";
 
 /**
  * 审核驳回
  */
 public static final String APPROVE_REJECT = "驳回";
 
 /**
  * 线上
  */
 public static final Integer SHOP_MODE_1 = 1;
 
 /**
  * 线下
  */
 public static final Integer SHOP_MODE_2 = 2;
 
 /**
  * 线上线下
  */
 public static final Integer SHOP_MODE_3 = 3;
 
 /**
  * 店铺状态正常
  */
 public static final Integer SHOP_STATUS_NORMAL = 1;
 
 /**
  * 店铺状态关闭
  */
 public static final Integer SHOP_STATUS_CLOSED = 0;
 
 
 public static final Integer MERCHANT_PRODUCT_SHOULD_NOT_APPLY = 1;
 public static final Integer MERCHANT_PRODUCT_SHOULD_APPLY = 0;
 
 /**
  * 提现申请
  */
 public static final Integer PAY_OFFLINE_APPLY = -1;
 
 /**
  * 已提现
  */
 public static final Integer PAY_OFFLINE_PASS = 1;
 
 /**
  * 提现失败
  */
 public static final Integer PAY_OFFLINE_FAILED = 2;
 
 /**
  * 驳回提现
  */
 public static final Integer PAY_OFFLINE_REJECT = 3;

 /**
  * 营业执照
  */
 public static final Integer MERCHANT_PHOTO_TYPE_1 = 1;
 /**
  * 实景图
  */
 public static final Integer MERCHANT_PHOTO_TYPE_2 = 2;
 
 /**
  * 公告App 所有
  */
 public static final Integer NOTICE_TARGET_ALL = -1;
 /**
  * 公告App 运营中心
  */
 public static final Integer NOTICE_TARGET_OPERATOR = 1;
 /**
  * 公告App 商家端
  */
 public static final Integer NOTICE_TARGET_MERCHANT = 2;
 /**
  * 公告App 消费端
  */
 public static final Integer NOTICE_TARGET_CONSUMER = 3;

}
