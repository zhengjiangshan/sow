package com.udream.common;

/**
 * 数据分页请求.
 * <p>
 * pageSize:每页显示数量
 * </p>
 * <p>
 * pageNum:页数
 * </p>
 * 
 * @author zhoupan
 *
 */
public class DataPageNumRequest extends DataRequest {

 private static final long serialVersionUID = 1L;

 /** The Constant DEFAULT_PAGE_SIZE. */
 public static final int DEFAULT_PAGE_SIZE = 10;

 /** The Constant MAX_PAGE_SIZE. */
 public static final int MAX_PAGE_SIZE = 200;

 /***
  * 默认当前页为1
  */
 public static final int DEFAULT_CURRENT_PAGE_NUM = 1;
 /**
  * 每页显示数量.
  */
 private Integer pageSize;

 /***
  * 当前页数
  */
 private Integer pageNum;

 /**
  * 如果没有指定pageSize,或者超过最大的pageSize.返回默认的pageSize.
  *
  * @return the page size
  */
 public Integer getPageSize() {
  if (pageSize == null || pageSize > MAX_PAGE_SIZE) {
   return DEFAULT_PAGE_SIZE;
  }
  return pageSize;
 }

 /**
  * Sets the page size.
  *
  * @param pageSize
  *         the page size
  */
 public void setPageSize(Integer pageSize) {
  this.pageSize = pageSize;
 }

 public Integer getPageNum() {
  if (pageNum == null) {
   return DEFAULT_CURRENT_PAGE_NUM;
  }
  return pageNum;
 }

 public void setPageNum(Integer pageNum) {
  this.pageNum = pageNum;
 }

}
