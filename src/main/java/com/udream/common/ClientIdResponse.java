package com.udream.common;

/**
 * 客户id响应.
 * 
 * @author zhoupan
 *
 */
public class ClientIdResponse extends DataItemRequest<String> {

 /**
  * 
  */
 private static final long serialVersionUID = 1L;

}
