package com.udream.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 数据请求.
 * 
 * @author zhoupan
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataNoLoginRequest implements java.io.Serializable {

 private static final long serialVersionUID = 1L;

 private String clientId;

 private String token;

 public String getClientId() {
  return clientId;
 }

 public void setClientId(String clientId) {
  this.clientId = clientId;
 }

 public String getToken() {
  return token;
 }

 public void setToken(String token) {
  this.token = token;
 }

}
