package com.udream.common;

/**
 * ClientTokenSupport.
 * 
 * @author zhoupan
 */
public interface ClientTokenSupport {
 /**
  * Sets the clientId.
  *
  * @param clientId
  *         the clientId.
  */
 public void setClientId(String clientId);

 /**
  * Sets the token.
  *
  * @param token
  *         the token
  */
 public void setToken(String token);
}
