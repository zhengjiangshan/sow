package com.udream.common;

import org.hibernate.validator.constraints.NotBlank;

public class DataPageNumRequestShopSupport extends DataPageNumRequest{
 @NotBlank(message = "{shop.id.cannot.null}")
 private String shopId;
 
 public String getShopId() {
  return shopId;
 }
 
 public void setShopId(String shopId) {
  this.shopId = shopId;
 }
}
