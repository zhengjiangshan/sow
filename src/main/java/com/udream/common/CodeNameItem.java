package com.udream.common;

/**
 * 编码和名称
 * 
 * @author zhoupan
 *
 */
public class CodeNameItem {

 /**
  * 编码.
  */
 private String code;

 /**
  * 名称.
  */
 private String name;

 public String getCode() {
  return code;
 }

 public void setCode(String code) {
  this.code = code;
 }

 public String getName() {
  return name;
 }

 public void setName(String name) {
  this.name = name;
 }

 public CodeNameItem code(String code) {
  this.setCode(code);
  return this;
 }

 public CodeNameItem name(String name) {
  this.setName(name);
  return this;
 }

}
