package com.udream.common;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 按接口要求重新封装分页数据结构.
 * 
 * @author zhoupan
 *
 * @param <Item>
 *         the generic type
 */
public class DataPageResponse<Item> extends DataResponse {

 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;

 /** The data. */
 private PageItem<Item> data = new PageItem<Item>();

 /**
  * Gets the data.
  *
  * @return the data
  */
 public PageItem<Item> getData() {
  return data;
 }

 /**
  * Sets the data.
  *
  * @param data
  *         the data
  */
 public void setData(PageItem<Item> data) {
  this.data = data;
 }

 /**
  * Sets the total.
  *
  * @param total
  *         the total
  */
 public void setTotal(Long total) {
  this.data.total(total);
 }

 /**
  * Sets the page num.
  *
  * @param pageNum
  *         the page num
  */
 public void setPageNum(Integer pageNum) {
  this.data.pageNum(pageNum);
 }

 /**
  * Sets the items.
  *
  * @param items
  *         the items
  */
 public void setItems(List<Item> items) {
  this.data.items(items);
 }

 /**
  * First item.
  *
  * @return the item
  */
 @JsonIgnore
 public Item firstItem() {
  if (this.data.getItems() != null && this.data.getItems().size() > 0) {
   return this.data.getItems().get(0);
  } else {
   return null;
  }
 }

 /**
  * Last item.
  *
  * @return the item
  */
 @JsonIgnore
 public Item lastItem() {
  if (this.data.getItems() != null && this.data.getItems().size() > 0) {
   return this.data.getItems().get(this.data.getItems().size() - 1);
  } else {
   return null;
  }
 }
}
