package com.udream.support.wechatpay;

import java.util.HashMap;
import java.util.Map;

/**
 * 退款申请请求
 */
public class RefundTradeRequest {
 /**
  * 微信订单号
  */
 private String transactionId;
 /**
  * 商户订单号
  */
 private String outTradeNo;
 /**
  * 商户退款订单号
  */
 private String outRefundNo;
 /**
  * 订单金额（单位：分）
  */
 private String totalFee;
 /**
  * 退款金额(单位：分)
  */
 private String refundFee;
 /**
  * 货币种类
  */
 private String refundFeeType = "CNY";
 /**
  * 退款原因
  */
 private String refundDesc;
 /**
  * 退款资金来源：仅针对老资金流商户使用
  * REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）
  * REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
  */
 private String refundAccount;
 
 private Map<String, String> data = new HashMap<>();
 
 public String getTransactionId() {
  return transactionId;
 }
 
 public void setTransactionId(String transactionId) {
  this.transactionId = transactionId;
 }
 
 public String getOutTradeNo() {
  return outTradeNo;
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getOutRefundNo() {
  return outRefundNo;
 }
 
 public void setOutRefundNo(String outRefundNo) {
  this.outRefundNo = outRefundNo;
 }
 
 public String getTotalFee() {
  return totalFee;
 }
 
 public void setTotalFee(String totalFee) {
  this.totalFee = totalFee;
 }
 
 public String getRefundFee() {
  return refundFee;
 }
 
 public void setRefundFee(String refundFee) {
  this.refundFee = refundFee;
 }
 
 public String getRefundFeeType() {
  return refundFeeType;
 }
 
 public void setRefundFeeType(String refundFeeType) {
  this.refundFeeType = refundFeeType;
 }
 
 public String getRefundDesc() {
  return refundDesc;
 }
 
 public void setRefundDesc(String refundDesc) {
  this.refundDesc = refundDesc;
 }
 
 public String getRefundAccount() {
  return refundAccount;
 }
 
 public void setRefundAccount(String refundAccount) {
  this.refundAccount = refundAccount;
 }
 
 public Map<String, String> getData() {
  if (transactionId != null)
   data.put("transaction_id", transactionId);
  if (null != getOutTradeNo())
   data.put("out_trade_no", outRefundNo);
  if (null != outRefundNo)
   data.put("out_refund_no", outRefundNo);
  if (null != totalFee)
   data.put("total_fee", totalFee);
  if (null != refundFee)
   data.put("refund_fee", refundFee);
  if (null != refundFeeType)
   data.put("refund_fee_type", refundFeeType);
  if (null != refundDesc)
   data.put("refund_desc", refundDesc);
  if (null != refundAccount)
   data.put("refund_account", refundAccount);
  return data;
 }
 
 public void setData(Map<String, String> data) {
  this.data = data;
 }
}
