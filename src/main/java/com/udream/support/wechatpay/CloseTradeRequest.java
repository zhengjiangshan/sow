package com.udream.support.wechatpay;

import java.util.Map;

/**
 * 关闭订单请求
 */
public class CloseTradeRequest {
 /**
  * 商户订单号
  */
 private String outTradeNo;
 
 private Map<String, String> data;
 
 
 public String getOutTradeNo() {
  return outTradeNo;
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public Map<String, String> getData() {
  if (outTradeNo != null)
   data.put("out_trade_no",outTradeNo);
  return data;
 }
 
 public void setData(Map<String, String> data) {
  this.data = data;
 }
}
