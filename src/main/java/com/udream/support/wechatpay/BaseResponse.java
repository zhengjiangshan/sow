package com.udream.support.wechatpay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 微信支付接口调用基础响应信息
 */
public class BaseResponse {
 /**
  * 通信是否成功
  */
 private Boolean connectSuccess;
 /**
  * 返回状态码
  */
 private String returnCode;
 /**
  * 返回信息
  */
 private String returnMsg;
 /**
  * 公众账号ID
  */
 private String appId;
 /**
  * 商户号
  */
 private String merchantId;
 /**
  * 设备号
  */
 private String deviceInfo;
 /**
  * 随机字符串
  */
 private String nonceStr;
 /**
  * 签名
  */
 private String sign;
 /**
  * 业务结果
  */
 private String resultCode;
 
 /**
  * 是否调用成功
  */
 private Boolean isSuccess;
 /**
  * 错误代码
  */
 private String errorCode;
 /**
  * 错误代码描述
  */
 private String errorCodeDes;
 
 /**
  * 接收响应
  */
 private Map<String, String> response;
 
 
 public String getReturnCode() {
  return this.exchange("return_code", returnCode);
 }
 
 public void setReturnCode(String returnCode) {
  this.returnCode = returnCode;
 }
 
 public String getReturnMsg() {
  return this.exchange("return_msg", returnMsg);
 }
 
 public void setReturnMsg(String returnMsg) {
  this.returnMsg = returnMsg;
 }
 
 public Boolean getConnectSuccess() {
  return this.getReturnCode().equals("SUCCESS");
 }
 
 public void setConnectSuccess(Boolean connectSuccess) {
  this.connectSuccess = connectSuccess;
 }
 
 public String getAppId() {
  return this.exchange("appid", appId);
 }
 
 public void setAppId(String appId) {
  this.appId = appId;
 }
 
 public String getMerchantId() {
  return this.exchange("mch_id", merchantId);
 }
 
 public void setMerchantId(String merchantId) {
  this.merchantId = merchantId;
 }
 
 public String getDeviceInfo() {
  return this.exchange("device_info", deviceInfo);
 }
 
 public void setDeviceInfo(String deviceInfo) {
  this.deviceInfo = deviceInfo;
 }
 
 public String getNonceStr() {
  return this.exchange("nonce_str", nonceStr);
 }
 
 public void setNonceStr(String nonceStr) {
  this.nonceStr = nonceStr;
 }
 
 public String getSign() {
  return this.exchange("sign", returnMsg);
 }
 
 public void setSign(String sign) {
  this.sign = sign;
 }
 
 public String getResultCode() {
  return this.exchange("result_code", resultCode);
 }
 
 public void setResultCode(String resultCode) {
  this.resultCode = resultCode;
 }
 
 public Boolean isSuccess() {
  return this.getResultCode().equals("SUCCESS");
 }
 
 public void setSuccess(Boolean success) {
  isSuccess = success;
 }
 
 public String getErrorCode() {
  return this.exchange("err_code", errorCode);
 }
 
 public void setErrorCode(String errorCode) {
  this.errorCode = errorCode;
 }
 
 public String getErrorCodeDes() {
  return this.exchange("err_code_des", errorCodeDes);
 }
 
 public void setErrorCodeDes(String errorCodeDes) {
  this.errorCodeDes = errorCodeDes;
 }
 
 public Map<String, String> getResponse() {
  return response;
 }
 
 public void setResponse(Map<String, String> response) {
  this.response = response;
 }
 
 /**
  * api转classfield
  *
  * @param apiFieldName
  * @param defaultValue
  * @return
  */
 String exchange(String apiFieldName, String defaultValue) {
  return this.getResponse().get(apiFieldName) == null ? defaultValue : this.getResponse().get(apiFieldName);
 }
 
 String exchange(String prefix, Integer index, String defaultValue) {
  return this.getResponse().get(prefix + index) == null ? defaultValue : this.getResponse().get(prefix + index);
 }
 
 String exchange(String prefix, Integer index$n, Integer index$m, String defaultValue) {
  String responseValue = this.getResponse().get(prefix + index$n + "_" + index$m);
  return responseValue == null ? defaultValue : responseValue;
 }
 
}
