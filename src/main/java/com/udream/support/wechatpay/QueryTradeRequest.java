package com.udream.support.wechatpay;

import java.util.HashMap;
import java.util.Map;

/**
 * 查询交易订单请求
 */
public class QueryTradeRequest {
 /**
  * 微信订单号
  */
 private String transactionId;
 /**
  * 商户订单号
  */
 private String outTradeNo;
 /**
  * 随机字符串
  */
 private String nonceStr;
 
 private Map<String, String> data = new HashMap<>();
 
 public String getTransactionId() {
  return transactionId;
 }
 
 public void setTransactionId(String transactionId) {
  this.transactionId = transactionId;
 }
 
 public String getOutTradeNo() {
  return outTradeNo;
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getNonceStr() {
  return nonceStr;
 }
 
 public void setNonceStr(String nonceStr) {
  this.nonceStr = nonceStr;
 }
 
 public Map<String, String> getData() {
  if (null != transactionId)
   data.put("transaction_id", transactionId);
  if (null != outTradeNo)
   data.put("out_trade_no", outTradeNo);
  if (null != nonceStr)
   data.put("nonce_str", nonceStr);
  return data;
 }
 
 public void setData(Map<String, String> data) {
  this.data = data;
 }
}
