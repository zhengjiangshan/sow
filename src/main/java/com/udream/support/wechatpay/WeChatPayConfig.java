package com.udream.support.wechatpay;

import com.github.wxpay.sdk.WXPayConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

/**
 * 微信支付配置类
 */
@Component("support.WechatPayConfig")
public class WeChatPayConfig implements WXPayConfig {
 
 @Value(value = "${wxpay.appid}")
 private String appId;
 @Value(value = "${wxpay.merchantId}")
 private String merchantId;
 @Value(value = "${wxpay.key}")
 private String key;
 @Value(value = "${wxpay.certfile.path}")
 private String classPathCertFilePath;
 @Value(value = "${wxpay.connect_timeout_ms}")
 private Integer connectTimeOut;
 @Value(value = "${wxpay.read_timeout_ms}")
 private Integer readTimeOut;
 
 @Override
 public String getAppID() {
  return this.appId;
 }
 
 @Override
 public String getMchID() {
  return this.merchantId;
 }
 
 @Override
 public String getKey() {
  return this.key;
 }
 
 @Override
 public InputStream getCertStream() {
  ClassPathResource classPathResource = new ClassPathResource(this.classPathCertFilePath);
  try {
   return classPathResource.getInputStream();
  } catch (IOException e) {
   e.printStackTrace();
  }
  return null;
 }
 
 @Override
 public int getHttpConnectTimeoutMs() {
  return this.connectTimeOut;
 }
 
 @Override
 public int getHttpReadTimeoutMs() {
  return this.readTimeOut;
 }
 
 public String getAppId() {
  return appId;
 }
 
 public void setAppId(String appId) {
  this.appId = appId;
 }
 
 public String getMerchantId() {
  return merchantId;
 }
 
 public void setMerchantId(String merchantId) {
  this.merchantId = merchantId;
 }
 
 public void setKey(String key) {
  this.key = key;
 }
 
 public String getClassPathCertFilePath() {
  return classPathCertFilePath;
 }
 
 public void setClassPathCertFilePath(String classPathCertFilePath) {
  this.classPathCertFilePath = classPathCertFilePath;
 }
 
 public Integer getConnectTimeOut() {
  return connectTimeOut;
 }
 
 public void setConnectTimeOut(Integer connectTimeOut) {
  this.connectTimeOut = connectTimeOut;
 }
 
 public Integer getReadTimeOut() {
  return readTimeOut;
 }
 
 public void setReadTimeOut(Integer readTimeOut) {
  this.readTimeOut = readTimeOut;
 }
}
