package com.udream.support.wechatpay;

import java.math.BigDecimal;
import java.util.Date;

/**
 * APP支付请求下单
 */
public class AppUnifiedOrderRequest extends UnifiedOrderRequest {
 public AppUnifiedOrderRequest body(String body) {
  this.setBody(body);
  return this;
 }
 
 public AppUnifiedOrderRequest detail(String detail) {
  this.setDetail(detail);
  return this;
 }
 
 public AppUnifiedOrderRequest attach(String attach) {
  this.setAttach(attach);
  return this;
 }
 
 public AppUnifiedOrderRequest outTradeNo(String outTradeNo) {
  this.setOutTradeNo(outTradeNo);
  return this;
 }
 
/* public AppUnifiedOrderRequest feeType(String feeType) {
  this.setFeeType(feeType);
  return this;
 }*/
 
 public AppUnifiedOrderRequest totalFee(BigDecimal totalFee) {
  this.setTotalFee(new String(totalFee.doubleValue() + ""));
  return this;
 }
 
 public AppUnifiedOrderRequest spBillCreateIp(String clientIp) {
  this.setSpBillCreateIp(clientIp);
  return this;
 }
 
 public AppUnifiedOrderRequest timeStart(Date timeStart) {
  this.setTimeStartDate(timeStart);
  return this;
 }
 
 public AppUnifiedOrderRequest timeExpire(Date timeExpire) {
  this.setTimeExpireDate(timeExpire);
  return this;
 }
 
 public AppUnifiedOrderRequest goodsTag(String goodsTag) {
  this.setGoodsTag(goodsTag);
  return this;
 }
 
 public AppUnifiedOrderRequest limitPay(String limitPay) {
  this.setLimitPay(limitPay);
  return this;
 }
 
 private AppUnifiedOrderRequest sceneInfo(String sceneInfo) {
  this.setSceneInfo(sceneInfo);
  return this;
 }
}
