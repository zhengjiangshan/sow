package com.udream.support.wechatpay;

import java.util.Map;

/**
 * 关闭订单响应
 */
public class CloseTradeResponse extends BaseResponse {
 public CloseTradeResponse() {
 }
 
 public CloseTradeResponse(Map<String, String> response) {
  this.setResponse(response);
 }
 
}
