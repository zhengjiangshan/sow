package com.udream.support.wechatpay;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 交易订单查询返回
 */
public class QueryTradeResponse extends BaseResponse {
 
 /**
  * 用户标识:用户在商户appid下的唯一标识
  */
 private String openId;
 
 /**
  * 是否关注公众账号
  */
 private String isSubscribe;
 
 /**
  * 是否关注公众账号
  */
 private Boolean subscribe;
 
 /**
  * 交易类型:	调用接口提交的交易类型
  */
 private String tradeType;
 
 /**
  * 交易状态:SUCCESS—支付成功
  * REFUND—转入退款
  * NOTPAY—未支付
  * CLOSED—已关闭
  * REVOKED—已撤销（刷卡支付）
  * USERPAYING--用户支付中
  * PAYERROR--支付失败(其他原因，如银行返回失败)
  */
 private String tradeState;
 
 /**
  * 付款银行:
  */
 private String bankType;
 
 /**
  * 总金额（分）
  */
 private String totalFee;
 
 /**
  * 总金额:(元)
  */
 private BigDecimal totalFeeCNY;
 
 /**
  * 现金支付金额
  */
 private String cashFee;
 
 /**
  * 货币种类
  */
 private String feeType;
 
 /**
  * 现金支付货币类型
  */
 private String cashFeeType;
 
 /**
  * 应结订单金额
  */
 private String settlementTotalFee;
 
 /**
  * 代金券金额
  */
 private String couponFee;
 
 /**
  * 代金券使用数量
  */
 private String couponCount;
 
 /**
  * 微信支付订单号
  */
 private String transactionId;
 
 /**
  * 商户订单号
  */
 private String outTradeNo;
 
 /**
  * 附加数据
  */
 private String attach;
 
 /**
  * 支付完成时间
  */
 private String timeEnd;
 
 /**
  * 支付完成时间
  */
 private Date timeEndDate;
 
 /**
  * 交易状态描述
  */
 private String tradeStateDesc;
 
 /**
  * 代金券信息
  */
 private List<ResponseCoupons> coupons;
 
 private Map<String, String> response;
 
 public QueryTradeResponse() {
 
 }
 
 public QueryTradeResponse(Map<String, String> data) {
  this.setResponse(data);
 }
 
 
 public String getOpenId() {
  return this.exchange("openid", openId);
 }
 
 public void setOpenId(String openId) {
  this.openId = openId;
 }
 
 public String getIsSubscribe() {
  return this.exchange("is_subscribe", isSubscribe);
 }
 
 public void setIsSubscribe(String isSubscribe) {
  this.isSubscribe = isSubscribe;
 }
 
 public Boolean getSubscribe() {
  return this.getIsSubscribe().equals("Y");
 }
 
 public void setSubscribe(Boolean subscribe) {
  this.subscribe = subscribe;
 }
 
 public String getTradeType() {
  return this.exchange("trade_type", tradeType);
 }
 
 public void setTradeType(String tradeType) {
  this.tradeType = tradeType;
 }
 
 public String getTradeState() {
  return this.exchange("trade_state", tradeState);
 }
 
 public void setTradeState(String tradeState) {
  this.tradeState = tradeState;
 }
 
 public String getBankType() {
  return this.exchange("bank_type", bankType);
 }
 
 public void setBankType(String bankType) {
  this.bankType = bankType;
 }
 
 public String getTotalFee() {
  return this.exchange("total_fee", totalFee);
 }
 
 public void setTotalFee(String totalFee) {
  this.totalFee = totalFee;
 }
 
 public BigDecimal getTotalFeeCNY() {
  if (this.getTotalFee() != null) {
   return new BigDecimal(Integer.valueOf(this.getTotalFee()));
  }
  return totalFeeCNY;
 }
 
 public void setTotalFeeCNY(BigDecimal totalFeeCNY) {
  this.totalFeeCNY = totalFeeCNY;
 }
 
 public String getCashFee() {
  return this.exchange("cash_fee", cashFee);
 }
 
 public void setCashFee(String cashFee) {
  this.cashFee = cashFee;
 }
 
 public String getFeeType() {
  return this.exchange("fee_type", feeType);
 }
 
 public void setFeeType(String feeType) {
  this.feeType = feeType;
 }
 
 public String getCashFeeType() {
  return this.exchange("cash_fee_type", cashFeeType);
 }
 
 public void setCashFeeType(String cashFeeType) {
  this.cashFeeType = cashFeeType;
 }
 
 public String getSettlementTotalFee() {
  return this.exchange("settlement_total_fee", settlementTotalFee);
 }
 
 public void setSettlementTotalFee(String settlementTotalFee) {
  this.settlementTotalFee = settlementTotalFee;
 }
 
 public String getCouponFee() {
  return this.exchange("coupon_fee", couponFee);
 }
 
 public void setCouponFee(String couponFee) {
  this.couponFee = couponFee;
 }
 
 public String getCouponCount() {
  return this.exchange("coupon_count", couponCount);
 }
 
 public void setCouponCount(String couponCount) {
  this.couponCount = couponCount;
 }
 
 public String getTransactionId() {
  return this.exchange("transaction_id", transactionId);
 }
 
 public void setTransactionId(String transactionId) {
  this.transactionId = transactionId;
 }
 
 public String getOutTradeNo() {
  return this.exchange("out_trade_no", outTradeNo);
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getAttach() {
  return this.exchange("attach", attach);
 }
 
 public void setAttach(String attach) {
  this.attach = attach;
 }
 
 public String getTimeEnd() {
  return this.exchange("time_end", timeEnd);
 }
 
 public void setTimeEnd(String timeEnd) {
  this.timeEnd = timeEnd;
 }
 
 public Date getTimeEndDate() {
  if (null != timeEnd) {
   try {
    return new SimpleDateFormat("yyyyMMddHHmmss").parse(this.getTimeEnd());
   } catch (ParseException e) {
    e.printStackTrace();
   }
  }
  return timeEndDate;
 }
 
 public void setTimeEndDate(Date timeEndDate) {
  this.timeEndDate = timeEndDate;
 }
 
 public String getTradeStateDesc() {
  return this.exchange("trade_state_desc", tradeStateDesc);
 }
 
 public void setTradeStateDesc(String tradeStateDesc) {
  this.tradeStateDesc = tradeStateDesc;
 }
 
 public Map<String, String> getResponse() {
  return response;
 }
 
 public void setResponse(Map<String, String> response) {
  this.response = response;
 }
 
 
 public List<ResponseCoupons> getCoupons() {
  //封装代金券信息
  Integer couponCountInt = Integer.valueOf(this.getCouponCount());
  if (couponCountInt <= 0)
   return null;
  List<ResponseCoupons> coupons = new ArrayList<>();
  for (int i = 0; i < couponCountInt; i++) {
   ResponseCoupons coupon = new ResponseCoupons();
   coupon.setCouponId(this.getResponse().get("coupon_id" + i));
   coupon.setCouponType(this.getResponse().get("coupon_type_" + i));
   coupon.setCouponFee(this.getResponse().get("coupon_fee_" + i));
   coupons.add(coupon);
  }
  return coupons;
 }
 
 public void setCoupons(List<ResponseCoupons> coupons) {
  this.coupons = coupons;
 }
 
}
