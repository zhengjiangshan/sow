package com.udream.support.wechatpay;

import java.util.Map;

/**
 * 申请退款响应
 */
public class RefundTradeResponse extends BaseResponse {
 /**
  * 微信订单号
  */
 private String transactionId;
 /**
  * 商户订单号
  */
 private String outTradeNo;
 /**
  * 商户退款单号
  */
 private String outRefundNo;
 /**
  * 微信退款单号
  */
 private String refundId;
 /**
  * 退款金额
  */
 private String refundFee;
 /**
  * 应结退款金额
  */
 private String settlementRefundFee;
 /**
  * 标价金额
  */
 private String totalFee;
 /**
  * 应结订单金额
  */
 private String settlementTotalFee;
 /**
  * 标价币种
  */
 private String feeType;
 /**
  * 现金支付金额
  */
 private String cashFee;
 /**
  * 现金支付币种
  */
 private String cashFeeType;
 /**
  * 现金退款金额
  */
 private String cashRefundFee;
 /**
  * 代金券类型
  */
 private String couponType$n;
 /**
  * 代金券退款总金额
  */
 private String couponRefundFee;
 /**
  * 单个代金券退款金额
  */
 private String couponRefundFee$n;
 /**
  * 退款代金券使用数量
  */
 private String couponRefundCount;
 /**
  * 退款代金券ID
  */
 private String couponRefundId$n;
 
 public RefundTradeResponse() {
 }
 
 public RefundTradeResponse(Map<String, String> data) {
  this.setResponse(data);
 }
 
 public String getTransactionId() {
  return this.exchange("transaction_id", transactionId);
 }
 
 public void setTransactionId(String transactionId) {
  this.transactionId = transactionId;
 }
 
 public String getOutTradeNo() {
  return this.exchange("out_trade_no", outTradeNo);
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getOutRefundNo() {
  return this.exchange("out_refund_no", outRefundNo);
 }
 
 public void setOutRefundNo(String outRefundNo) {
  this.outRefundNo = outRefundNo;
 }
 
 public String getRefundId() {
  return this.exchange("refund_id", refundId);
 }
 
 public void setRefundId(String refundId) {
  this.refundId = refundId;
 }
 
 public String getRefundFee() {
  return this.exchange("refund_fee", refundFee);
 }
 
 public void setRefundFee(String refundFee) {
  this.refundFee = refundFee;
 }
 
 public String getSettlementRefundFee() {
  return this.exchange("settlement_refund_fee", settlementRefundFee);
 }
 
 public void setSettlementRefundFee(String settlementRefundFee) {
  this.settlementRefundFee = settlementRefundFee;
 }
 
 public String getTotalFee() {
  return this.exchange("total_fee", totalFee);
 }
 
 public void setTotalFee(String totalFee) {
  this.totalFee = totalFee;
 }
 
 public String getSettlementTotalFee() {
  return this.exchange("settlement_total_fee", settlementTotalFee);
 }
 
 public void setSettlementTotalFee(String settlementTotalFee) {
  this.settlementTotalFee = settlementTotalFee;
 }
 
 public String getFeeType() {
  return this.exchange("fee_type", feeType);
 }
 
 public void setFeeType(String feeType) {
  this.feeType = feeType;
 }
 
 public String getCashFee() {
  return this.exchange("cash_fee", cashFee);
 }
 
 public void setCashFee(String cashFee) {
  this.cashFee = cashFee;
 }
 
 public String getCashFeeType() {
  return this.exchange("cash_fee_type", cashFeeType);
 }
 
 public void setCashFeeType(String cashFeeType) {
  this.cashFeeType = cashFeeType;
 }
 
 public String getCashRefundFee() {
  return this.exchange("cash_refund_fee", cashRefundFee);
 }
 
 public void setCashRefundFee(String cashRefundFee) {
  this.cashRefundFee = cashRefundFee;
 }
 
 public String getCouponType$n() {
  return this.exchange("coupon_type_$n", couponType$n);
 }
 
 public void setCouponType$n(String couponType$n) {
  this.couponType$n = couponType$n;
 }
 
 public String getCouponRefundFee() {
  return this.exchange("coupon_refund_fee", couponRefundFee);
 }
 
 public void setCouponRefundFee(String couponRefundFee) {
  this.couponRefundFee = couponRefundFee;
 }
 
 public String getCouponRefundFee$n() {
  return this.exchange("coupon_refund_fee_$n", couponRefundFee$n);
 }
 
 public void setCouponRefundFee$n(String couponRefundFee$n) {
  this.couponRefundFee$n = couponRefundFee$n;
 }
 
 public String getCouponRefundCount() {
  return this.exchange("coupon_refund_count", couponRefundCount);
 }
 
 public void setCouponRefundCount(String couponRefundCount) {
  this.couponRefundCount = couponRefundCount;
 }
 
 public String getCouponRefundId$n() {
  return this.exchange("coupon_refund_id_$n", couponRefundId$n);
 }
 
 public void setCouponRefundId$n(String couponRefundId$n) {
  this.couponRefundId$n = couponRefundId$n;
 }
}
