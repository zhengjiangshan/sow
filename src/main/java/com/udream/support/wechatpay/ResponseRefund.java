package com.udream.support.wechatpay;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 响应退款信息封装
 */
public class ResponseRefund {
 /**
  * 商户退款单号
  */
 private String outRefundNo;
 /**
  * 微信退款单号
  */
 private String refundId;
 /**
  * 退款渠道：
  * ORIGINAL—原路退款
  * BALANCE—退回到余额
  * OTHER_BALANCE—原账户异常退到其他余额账户
  * OTHER_BANKCARD—原银行卡异常退到其他银行卡
  */
 private String refundChannel;
 /**
  * 申请退款金额:
  * 退款总金额,单位为分,可以做部分退款
  */
 private String refundFee;
 /**
  * 退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
  */
 private String settlementRefundFee;
 /**
  * 代金券类型:
  * CASH--充值代金券
  * NO_CASH---非充值优惠券
  * 开通免充值券功能，并且订单使用了优惠券后有返回
  */
 private String couponType;
 /**
  * 总代金券退款金额:
  * 代金券退款金额<=退款金额，退款金额-代金券或立减优惠退款金额为现金
  */
 private String couponRefundFee;
 /**
  * 退款代金券使用数量
  */
 private String couponRefundCount;
 
 /**
  * 退款状态：
  * 退款状态：
  * SUCCESS—退款成功
  * REFUNDCLOSE—退款关闭。
  * PROCESSING—退款处理中
  * CHANGE—退款异常，退款到银行发现用户的卡作废或者冻结了，
  * 导致原路退款银行卡失败，
  * 可前往商户平台（pay.weixin.qq.com）-交易中心，手动处理此笔退款。
  */
 private String refundStatus;
 
 /**
  * 退款资金来源:
  * REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款/基本账户
  * REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款
  */
 private String refundAccount;
 
 /**
  * 退款入账账户:
  * 取当前退款单的退款入账方
  * 1）退回银行卡：
  * {银行名称}{卡类型}{卡尾号}
  * 2）退回支付用户零钱:
  * 支付用户零钱
  * 3）退还商户:
  * 商户基本账户
  * 商户结算银行账户
  */
 private String refundRecvAccount;
 
 /**
  * 退款成功时间:
  * 退款成功时间，当退款状态为退款成功时有返回
  */
 private String refundSuccessTime;
 
 /**
  * 退款成功时间(java.utl.Date):已经格式化
  * 退款成功时间，当退款状态为退款成功时有返回
  */
 private Date refundSuccessTimeDate;
 
 /**
  * 退款代金券信息
  */
 private List<ResponseCoupons> coupons;
 
 public String getOutRefundNo() {
  return outRefundNo;
 }
 
 public void setOutRefundNo(String outRefundNo) {
  this.outRefundNo = outRefundNo;
 }
 
 public String getRefundId() {
  return refundId;
 }
 
 public void setRefundId(String refundId) {
  this.refundId = refundId;
 }
 
 public String getRefundChannel() {
  return refundChannel;
 }
 
 public void setRefundChannel(String refundChannel) {
  this.refundChannel = refundChannel;
 }
 
 public String getRefundFee() {
  return refundFee;
 }
 
 public void setRefundFee(String refundFee) {
  this.refundFee = refundFee;
 }
 
 public String getSettlementRefundFee() {
  return settlementRefundFee;
 }
 
 public void setSettlementRefundFee(String settlementRefundFee) {
  this.settlementRefundFee = settlementRefundFee;
 }
 
 public String getCouponType() {
  return couponType;
 }
 
 public void setCouponType(String couponType) {
  this.couponType = couponType;
 }
 
 public String getCouponRefundFee() {
  return couponRefundFee;
 }
 
 public void setCouponRefundFee(String couponRefundFee) {
  this.couponRefundFee = couponRefundFee;
 }
 
 public String getCouponRefundCount() {
  return couponRefundCount;
 }
 
 public void setCouponRefundCount(String couponRefundCount) {
  this.couponRefundCount = couponRefundCount;
 }
 
 public String getRefundStatus() {
  return refundStatus;
 }
 
 public void setRefundStatus(String refundStatus) {
  this.refundStatus = refundStatus;
 }
 
 public String getRefundAccount() {
  return refundAccount;
 }
 
 public void setRefundAccount(String refundAccount) {
  this.refundAccount = refundAccount;
 }
 
 public String getRefundRecvAccount() {
  return refundRecvAccount;
 }
 
 public void setRefundRecvAccount(String refundRecvAccount) {
  this.refundRecvAccount = refundRecvAccount;
 }
 
 public String getRefundSuccessTime() {
  return refundSuccessTime;
 }
 
 public void setRefundSuccessTime(String refundSuccessTime) {
  this.refundSuccessTime = refundSuccessTime;
 }
 
 public Date getRefundSuccessTimeDate() {
  try {
   return new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(this.getRefundSuccessTime());
  } catch (ParseException e) {
   e.printStackTrace();
  }
  return null;
 }
 
 public void setRefundSuccessTimeDate(Date refundSuccessTimeDate) {
  this.refundSuccessTimeDate = refundSuccessTimeDate;
 }
 
 public List<ResponseCoupons> getCoupons() {
  return coupons;
 }
 
 public void setCoupons(List<ResponseCoupons> coupons) {
  this.coupons = coupons;
 }
}
