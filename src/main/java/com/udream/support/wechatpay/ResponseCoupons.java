package com.udream.support.wechatpay;

/**
 * 响应代金券信息
 */
public class ResponseCoupons {
 private String couponId;
 private String couponType;
 private String couponFee;
 
 public String getCouponId() {
  return couponId;
 }
 
 public void setCouponId(String couponId) {
  this.couponId = couponId;
 }
 
 public String getCouponType() {
  return couponType;
 }
 
 public void setCouponType(String couponType) {
  this.couponType = couponType;
 }
 
 public String getCouponFee() {
  return couponFee;
 }
 
 public void setCouponFee(String couponFee) {
  this.couponFee = couponFee;
 }
}
