package com.udream.support.wechatpay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 退款订单查询响应
 */
public class QueryRefundTradeResponse extends BaseResponse {
 /**
  * 微信订单号
  */
 private String transactionId;
 /**
  * 商户订单号
  */
 private String outTradeNo;
 /**
  * 订单总金额
  */
 private String totalFee;
 /**
  * 订单金额货币种类
  */
 private String feeType;
 /**
  * 现金支付金额 现金支付金额，单位为分，只能为整数
  */
 private String cashFee;
 /**
  * 现金支付货币类型
  */
 private String cashFeeType;
 /**
  * 应结订单金额:当订单使用了免充值型优惠券后返回该参数，应结订单金额=订单金额-免充值优惠券金额。
  */
 private String settlementTotalFee;
 /**
  * 退款笔数:退款记录数
  */
 private String refundCount;
 /**
  * 退款信息列表
  */
 private List<ResponseRefund> refunds;
 
 public QueryRefundTradeResponse() {
 }
 
 public QueryRefundTradeResponse(Map<String, String> response) {
  this.setResponse(response);
 }
 
 public String getTransactionId() {
  return this.exchange("transaction_id", transactionId);
 }
 
 public void setTransactionId(String transactionId) {
  this.transactionId = transactionId;
 }
 
 public String getOutTradeNo() {
  return this.exchange("out_trade_no", outTradeNo);
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getTotalFee() {
  return this.exchange("total_fee", totalFee);
 }
 
 public void setTotalFee(String totalFee) {
  this.totalFee = totalFee;
 }
 
 public String getFeeType() {
  return this.exchange("fee_type", feeType);
 }
 
 public void setFeeType(String feeType) {
  this.feeType = feeType;
 }
 
 public String getCashFee() {
  return this.exchange("cash_fee", cashFee);
 }
 
 public void setCashFee(String cashFee) {
  this.cashFee = cashFee;
 }
 
 public String getCashFeeType() {
  return this.exchange("cash_fee_type", cashFeeType);
 }
 
 public void setCashFeeType(String cashFeeType) {
  this.cashFeeType = cashFeeType;
 }
 
 public String getSettlementTotalFee() {
  return this.exchange("settlement_total_fee", settlementTotalFee);
 }
 
 public void setSettlementTotalFee(String settlementTotalFee) {
  this.settlementTotalFee = settlementTotalFee;
 }
 
 public String getRefundCount() {
  return this.exchange("refund_count", refundCount);
 }
 
 public void setRefundCount(String refundCount) {
  this.refundCount = refundCount;
 }
 
 public List<ResponseRefund> getRefunds() {
  Integer refundCountInt = Integer.valueOf(this.getRefundCount());
  if (refundCountInt <= 0)
   return null;
  for (int i = 0; i < refundCountInt; i++) {
   ResponseRefund responseRefund = new ResponseRefund();
   responseRefund.setOutRefundNo(this.exchange("out_refund_no_", i, null));
   responseRefund.setRefundId(this.exchange("refund_id_", i, null));
   responseRefund.setRefundChannel(this.exchange("refund_channel_", i, null));
   responseRefund.setRefundFee(this.exchange("refund_fee_", i, null));
   responseRefund.setSettlementRefundFee(this.exchange("settlement_refund_fee_", i, null));
   responseRefund.setCouponType(this.exchange("coupon_type_", i, null));
   responseRefund.setCouponRefundFee(this.exchange("coupon_refund_fee_", i, null));
   responseRefund.setCouponRefundCount(this.exchange("coupon_refund_count_", i, null));
   responseRefund.setRefundStatus(this.exchange("refund_status_", 1, null));
   responseRefund.setRefundAccount(this.exchange("refund_account_", i, null));
   responseRefund.setRefundRecvAccount(this.exchange("refund_recv_accout_", i, null));
   responseRefund.setRefundSuccessTime(this.exchange("refund_success_time_", i, null));
   Integer couponCount = Integer.valueOf(responseRefund.getCouponRefundCount());
   if (couponCount > 0) {
    List<ResponseCoupons> coupons = new ArrayList<>();
    for (int j = 0; j < couponCount; j++) {
     ResponseCoupons coupon = new ResponseCoupons();
     coupon.setCouponId(this.exchange("coupon_refund_id_", i, j, null));
     coupon.setCouponFee(this.exchange("coupon_refund_fee_", i, j, null));
    }
    responseRefund.setCoupons(coupons);
   }
  }
  return refunds;
 }
 
 public void setRefunds(List<ResponseRefund> refunds) {
  this.refunds = refunds;
 }
}
