package com.udream.support.wechatpay;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信支付统一下单请求
 */
public class UnifiedOrderRequest {
 
 /**
  * 设备号:终端设备号(门店号或收银设备ID)，默认请传"WEB"
  */
 private String deviceInfo = "WEB";
 
 /**
  * 随机字符串:随机字符串，不长于32位。推荐随机数生成算法
  */
 private String noceInfo;
 
 /**
  * 商品描述:商品描述交易字段格式根据不同的应用场景按照以下格式：
  * APP——需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。
  */
 private String body;
 
 /**
  * 商品详情:商品详细描述，对于使用单品优惠的商户，改字段必须按照规范上传，
  * 详见“<a href='https://pay.weixin.qq.com/wiki/doc/api/danpin.php?chapter=9_102&index=2'>单品优惠参数说明</a>”
  */
 private String detail;
 
 /**
  * 附加数据:附加数据，在查询API和支付通知中原样返回，该字段主要用于商户携带订单的自定义数据
  */
 private String attach;
 
 /**
  * 商户订单号:商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|*@ ，且在同一个商户号下唯一。
  * 详见<a href='https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=4_2'>商户订单号</a>
  */
 private String outTradeNo;
 
 /**
  * 货币类型:默认 CNY
  */
 private String feeType = "CNY";
 
 /**
  * 总金额:订单总金额，单位为分，
  * 详见：<a href='https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=4_2'>支付金额</a>
  */
 private String totalFee;
 
 /**
  * 终端IP:用户端实际ip
  */
 private String spBillCreateIp;
 
 /**
  * 交易起始时间	:订单生成时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。
  * 其他详见<a href='https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=4_2'>时间规则</a>
  */
 private String timeStart;
 
 private Date timeStartDate;
 /**
  * 交易结束时间:订单失效时间，格式为yyyyMMddHHmmss，如2009年12月25日9点10分10秒表示为20091225091010。
  * <strong>注意：最短失效时间间隔必须大于5分钟</strong>
  */
 private String timeExpire;
 
 private Date timeExpireDate;
 
 /**
  * 订单优惠标记:订单优惠标记，代金券或立减优惠功能的参数，
  * 说明详见<a href='https://pay.weixin.qq.com/wiki/doc/api/tools/sp_coupon.php?chapter=12_1'>代金券或立减优惠</a>
  */
 private String goodsTag;
 
 /**
  * 通知地址:接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。
  */
 private String notifyUrl;
 
 /**
  * 交易类型:JSAPI--公众号支付、NATIVE--原生扫码支付、APP--app支付
  */
 private String tradeType;
 
 /**
  * 商品ID:trade_type=NATIVE时（即扫码支付），此参数必传。此参数为二维码中包含的商品ID，商户自行定义。
  */
 private String productId;
 
 /**
  * 指定支付方式:no_credit--指定不能使用信用卡支付
  */
 private String limitPay;
 
 /**
  * 场景信息:该字段用于统一下单时上报场景信息，目前支持上报实际门店信息。
  * <code>
  * {
  * "store_id": "", //门店唯一标识，选填，String(32)
  * "store_name":"”//门店名称，选填，String(64)
  * }</code>
  */
 private String sceneInfo;
 
 /**
  * rade_type=JSAPI时（即公众号支付），此参数必传，此参数为微信用户在商户对应appid下的唯一标识。
  * openid如何获取，可参考【获取openid】。企业号请使用【企业号OAuth2.0接口】获取企业号内成员userid，再调用【企业号userid转openid接口】进行转换
  */
 private String openId;
 
 
 /**
  * 转换请求参数为map类型
  */
 private Map<String, String> data = new HashMap<>();
 
 public String getDeviceInfo() {
  return deviceInfo;
 }
 
 public void setDeviceInfo(String deviceInfo) {
  this.deviceInfo = deviceInfo;
 }
 
 public String getNoceInfo() {
  return noceInfo;
 }
 
 public void setNoceInfo(String noceInfo) {
  this.noceInfo = noceInfo;
 }
 
 public String getBody() {
  return body;
 }
 
 public void setBody(String body) {
  this.body = body;
 }
 
 public String getDetail() {
  return detail;
 }
 
 public void setDetail(String detail) {
  this.detail = detail;
 }
 
 public String getAttach() {
  return attach;
 }
 
 public void setAttach(String attach) {
  this.attach = attach;
 }
 
 public String getOutTradeNo() {
  return outTradeNo;
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getFeeType() {
  return feeType;
 }
 
 public void setFeeType(String feeType) {
  this.feeType = feeType;
 }
 
 public String getTotalFee() {
  return totalFee;
 }
 
 public void setTotalFee(String totalFee) {
  this.totalFee = totalFee;
 }
 
 public String getSpBillCreateIp() {
  return spBillCreateIp;
 }
 
 public void setSpBillCreateIp(String spBillCreateIp) {
  this.spBillCreateIp = spBillCreateIp;
 }
 
 public String getTimeStart() {
  return timeStart;
 }
 
 public void setTimeStart(String timeStart) {
  this.timeStart = new SimpleDateFormat("yyyyMMddHHmmss").format(this.timeStart);
 }
 
 public Date getTimeStartDate() {
  return timeStartDate;
 }
 
 public void setTimeStartDate(Date timeStartDate) {
  this.timeStartDate = timeStartDate;
 }
 
 public String getTimeExpire() {
  return timeExpire;
 }
 
 public void setTimeExpire(String timeExpire) {
  this.timeExpire = new SimpleDateFormat("yyyyMMddHHmmss").format(this.timeExpireDate);
 }
 
 public Date getTimeExpireDate() {
  return timeExpireDate;
 }
 
 public void setTimeExpireDate(Date timeExpireDate) {
  this.timeExpireDate = timeExpireDate;
 }
 
 public String getGoodsTag() {
  return goodsTag;
 }
 
 public void setGoodsTag(String goodsTag) {
  this.goodsTag = goodsTag;
 }
 
 public String getNotifyUrl() {
  return notifyUrl;
 }
 
 public void setNotifyUrl(String notifyUrl) {
  this.notifyUrl = notifyUrl;
 }
 
 public String getTradeType() {
  return tradeType;
 }
 
 public void setTradeType(String tradeType) {
  this.tradeType = tradeType;
 }
 
 public String getLimitPay() {
  return limitPay;
 }
 
 public void setLimitPay(String limitPay) {
  this.limitPay = limitPay;
 }
 
 public String getSceneInfo() {
  return sceneInfo;
 }
 
 public void setSceneInfo(String sceneInfo) {
  this.sceneInfo = sceneInfo;
 }
 
 public String getOpenId() {
  return openId;
 }
 
 public void setOpenId(String openId) {
  this.openId = openId;
 }
 
 public String getProductId() {
  return productId;
 }
 
 public void setProductId(String productId) {
  this.productId = productId;
 }
 
 public Map<String, String> getData() {
  if (null != this.deviceInfo)
   data.put("device_info", this.deviceInfo);
  if (null != this.noceInfo)
   data.put("nonce_str", this.noceInfo);
  if (null != body)
   data.put("body", body);
  if (null != detail)
   data.put("detail", detail);
  if (null != attach)
   data.put("attach", attach);
  if (null != outTradeNo)
   data.put("out_trade_no", this.outTradeNo);
  if (null != feeType)
   data.put("fee_type", this.feeType);
  if (null != totalFee)
   data.put("total_fee", this.totalFee);
  if (null != this.spBillCreateIp)
   data.put("spbill_create_ip", this.spBillCreateIp);
  if (null != this.getTimeStart())
   data.put("time_start", this.getTimeStart());
  if (null != this.getTimeExpire())
   data.put("time_expire", this.getTimeExpire());
  if (null != this.goodsTag)
   data.put("goods_tag", this.goodsTag);
  if (null != this.notifyUrl)
   data.put("notify_url", this.notifyUrl);
  if (null != this.tradeType)
   data.put("trade_type", this.tradeType);
  if (null != productId)
   data.put("product_id", this.productId);
  if (null != this.limitPay)
   data.put("limit_pay", this.limitPay);
  if (null != this.openId)
   data.put("openid", openId);
  if (null != this.sceneInfo)
   data.put("scene_info", this.sceneInfo);
  return data;
 }
 
 public void setData(Map<String, String> data) {
  this.data = data;
 }
}
