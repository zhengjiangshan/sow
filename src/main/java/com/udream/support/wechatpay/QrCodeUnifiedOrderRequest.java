package com.udream.support.wechatpay;

/**
 * 二维码支付请求(部分参数与App支付参数相同)
 */
public class QrCodeUnifiedOrderRequest extends AppUnifiedOrderRequest {
 
 /**
  * 扫码支付必须设置商户系统商品ID
  *
  * @param productId
  * @return
  */
 public QrCodeUnifiedOrderRequest productId(String productId) {
  this.setProductId(productId);
  return this;
 }
 
}
