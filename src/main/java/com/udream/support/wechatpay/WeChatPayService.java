package com.udream.support.wechatpay;

import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * `微信支付服务
 */
@Component(value = "support.weChatPay.Service")
public class WeChatPayService {
 
 @Autowired
 private WeChatPayConfig weChatPayConfig;
 
 @Value(value = "${wxpay.notify_url}")
 private String notifyUrl;
 
 @Value(value = "${wxpay.sign_type}")
 private String signType;
 
 @Value(value = "${wxpay.use_sandbox}")
 private Boolean useSandbox;
 
 private WXPay wxPay;
 
 public WXPay getWxPay() {
  if (null != signType)
   wxPay = new WXPay(weChatPayConfig, signType.equals("MD5") ? WXPayConstants.SignType.MD5 : WXPayConstants.SignType.HMACSHA256);
  if (null != this.getUseSandbox() && null != this.getSignType())
   wxPay = new WXPay(weChatPayConfig, signType.equals("MD5") ? WXPayConstants.SignType.MD5 : WXPayConstants.SignType.HMACSHA256, useSandbox);
  return wxPay;
 }
 
 /**
  * 统一下单接口服务
  *
  * @param request
  * @return
  * @throws Exception
  */
 UnifiedOrderResponse doUnifiedOrder(UnifiedOrderRequest request) throws Exception {
  WXPay wxPay = getWxPay();
  request.setNotifyUrl(this.notifyUrl);
  Map<String, String> response = wxPay.unifiedOrder(request.getData());
  return new UnifiedOrderResponse(response);
 }
 
 /**
  * 原生扫码支付下单
  *
  * @param request
  * @return
  * @throws Exception
  */
 UnifiedOrderResponse doUnifiedOrderNative(QrCodeUnifiedOrderRequest request) throws Exception {
  request.setTradeType("NATIVE");
  return doUnifiedOrder(request);
 }
 
 /**
  * 公众号支付下单
  *
  * @param request
  * @return
  * @throws Exception
  */
 UnifiedOrderResponse doUnifiedOrderJsApi(JSApiUnifiedOrderRequest request) throws Exception {
  request.setTradeType("JSAPI");
  return doUnifiedOrder(request);
 }
 
 /**
  * APP支付下单
  *
  * @param request
  * @return
  * @throws Exception
  */
 UnifiedOrderResponse doUnifiedOrderApp(AppUnifiedOrderRequest request) throws Exception {
  request.setTradeType("APP");
  return doUnifiedOrder(request);
 }
 
 /**
  * 微信订单查询接口服务
  *
  * @param request
  * @return
  * @throws Exception
  */
 QueryTradeResponse doQueryTrade(QueryTradeRequest request) throws Exception {
  WXPay wxPay = getWxPay();
  return new QueryTradeResponse(wxPay.orderQuery(request.getData()));
 }
 
 /**
  * 关闭订单服务
  *
  * @param request
  * @return
  * @throws Exception
  */
 CloseTradeResponse doCloseTrade(CloseTradeRequest request) throws Exception {
  WXPay wxPay = getWxPay();
  return new CloseTradeResponse(wxPay.closeOrder(request.getData()));
 }
 
 /**
  * 申请退款接口服务
  *
  * @param request
  * @return
  * @throws Exception
  */
 RefundTradeResponse doRefundTrade(RefundTradeRequest request) throws Exception {
  WXPay wxPay = getWxPay();
  return new RefundTradeResponse(wxPay.refund(request.getData()));
 }
 
 /**
  * 查询退款接口服务
  *
  * @param request
  * @return
  * @throws Exception
  */
 QueryRefundTradeResponse doQueryRefund(QueryRefundTradeRequest request) throws Exception {
  WXPay wxPay = getWxPay();
  return new QueryRefundTradeResponse(wxPay.refundQuery(request.getData()));
 }
 
 public WeChatPayConfig getWeChatPayConfig() {
  return weChatPayConfig;
 }
 
 public void setWeChatPayConfig(WeChatPayConfig weChatPayConfig) {
  this.weChatPayConfig = weChatPayConfig;
 }
 
 public String getNotifyUrl() {
  return notifyUrl;
 }
 
 public void setNotifyUrl(String notifyUrl) {
  this.notifyUrl = notifyUrl;
 }
 
 public String getSignType() {
  return signType;
 }
 
 public void setSignType(String signType) {
  this.signType = signType;
 }
 
 public Boolean getUseSandbox() {
  return useSandbox;
 }
 
 public void setUseSandbox(Boolean useSandbox) {
  this.useSandbox = useSandbox;
 }
 
}
