package com.udream.support.wechatpay;

import java.util.Map;

/**
 * 统一下单响应
 */
public class UnifiedOrderResponse extends BaseResponse {
 
 public UnifiedOrderResponse() {
 }
 
 public UnifiedOrderResponse(Map<String, String> response) {
  this.setResponse(response);
 }
 
 
 /**
  * 交易类型
  */
 private String tradeType;
 
 /**
  * 预支付交易会话标识
  */
 private String prePayId;
 
 /**
  * 二维码链接(扫码支付)
  */
 private String codeUrl;
 
 /**
  * H5支付跳转链接（H5支付）
  */
 private String mWebUrl;
 
 public String getTradeType() {
  return this.exchange("trade_type", tradeType);
 }
 
 public void setTradeType(String tradeType) {
  this.tradeType = tradeType;
 }
 
 public String getPrePayId() {
  return this.exchange("prepay_id", prePayId);
 }
 
 public void setPrePayId(String prePayId) {
  this.prePayId = prePayId;
 }
 
 public String getCodeUrl() {
  return this.exchange("code_url", codeUrl);
 }
 
 
 public void setCodeUrl(String codeUrl) {
  this.codeUrl = codeUrl;
 }
 
 public String getmWebUrl() {
  return this.exchange("mweb_url", mWebUrl);
 }
 
 public void setmWebUrl(String mWebUrl) {
  this.mWebUrl = mWebUrl;
 }
}
