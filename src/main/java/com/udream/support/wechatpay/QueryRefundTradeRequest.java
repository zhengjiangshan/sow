package com.udream.support.wechatpay;

import java.util.Map;

/**
 * 退款订单查询
 */
public class QueryRefundTradeRequest {
 
 /**
  * 微信订单号
  */
 private String transactionId;
 /**
  * 商户订单号
  */
 private String outTradeNo;
 /**
  * 商户退款单号
  */
 private String outRefundNo;
 /**
  * 微信退款单号
  */
 private String refundId;
 
 private Map<String, String> data;
 
 public String getTransactionId() {
  return transactionId;
 }
 
 public void setTransactionId(String transactionId) {
  this.transactionId = transactionId;
 }
 
 public String getOutTradeNo() {
  return outTradeNo;
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getOutRefundNo() {
  return outRefundNo;
 }
 
 public void setOutRefundNo(String outRefundNo) {
  this.outRefundNo = outRefundNo;
 }
 
 public String getRefundId() {
  return refundId;
 }
 
 public void setRefundId(String refundId) {
  this.refundId = refundId;
 }
 
 public Map<String, String> getData() {
  if (null != transactionId)
   data.put("transaction_id", transactionId);
  if (null != outTradeNo)
   data.put("out_trade_no", outTradeNo);
  if (null != outRefundNo)
   data.put("out_refund_no", outRefundNo);
  if (null != refundId)
   data.put("refund_id", refundId);
  return data;
 }
 
 public void setData(Map<String, String> data) {
  this.data = data;
 }
}
