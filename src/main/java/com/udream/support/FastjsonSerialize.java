package com.udream.support;

import com.alibaba.fastjson.JSON;

/**
 * FastjsonSerialize.
 * 
 * @author zhoupan.
 * 
 */
public class FastjsonSerialize implements JsonSerialize {

 @Override
 public String toJson(Object obj) {
  return JSON.toJSONString(obj);
 }

 @Override
 public <T> T fromJson(String json, Class<T> classOfT) {
  return JSON.parseObject(json, classOfT);
 }
}
