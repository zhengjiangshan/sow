package com.udream.support;

/**
 * SaveAware.
 * 
 * @author zhoupan.
 */
public interface SaveAware {

 /**
  * Will save.
  */
 public void willSave();

 /**
  * Did save.
  */
 public void didSave();

}
