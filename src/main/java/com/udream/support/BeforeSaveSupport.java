package com.udream.support;

public interface BeforeSaveSupport {

 public void beforeSave(FormResultSupport result);

}
