package com.udream.support.qiniu;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.udream.support.FileStoreService;
import com.udream.support.LogUtils;
import com.udream.support.QiniuKit.QiniuPutResponse;
import com.udream.support.QiniuKit.QiniuSupport;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Service("qiniuFileStoreService")
public class QiniuFileStoreService implements FileStoreService {

 @Value("${qiniu.store.accessKey}")
 protected String accessKey;
 @Value("${qiniu.store.secretKey}")
 protected String secretKey;
 @Value("${qiniu.store.bucketName}")
 protected String bucketName;
 @Value("${qiniu.store.domain}")
 protected String domain;
 protected QiniuSupport qiniu;

 public QiniuSupport getQiniu() {
  if (qiniu == null) {
   qiniu = new QiniuSupport();
   qiniu.setAccessKey(this.accessKey);
   qiniu.setSecretKey(this.secretKey);
   qiniu.setBucketName(this.bucketName);
   qiniu.setDomain(this.domain);
   LogUtils.performance.info("init qiniu:ak={} sk={} bk={} domain={}",
    new Object[] { this.accessKey, this.secretKey, this.bucketName, this.domain });
  }
  return qiniu;
 }

 public void setQiniu(QiniuSupport qiniu) {
  this.qiniu = qiniu;
 }

 @Override
 public byte[] readToByteArray(String path) throws IOException {
  String httpUrl = this.getInternetUrl(path);
  LogUtils.performance.info("qiniu:readToByteArray:{}", httpUrl);
  Request request = new Request.Builder().url(httpUrl).build();
  Response response = new OkHttpClient().newCall(request).execute();
  return response.body().bytes();
 }

 @Override
 public InputStream readToInputStream(String path) throws IOException {
  String httpUrl = this.getInternetUrl(path);
  LogUtils.performance.info("qiniu:readFileToInputStream:{}", httpUrl);
  Request request = new Request.Builder().url(httpUrl).build();
  Response response = new OkHttpClient().newCall(request).execute();
  return response.body().byteStream();
 }

 @Override
 public boolean existed(String path) throws IOException {
  return getQiniu().existed(path);
 }

 @Override
 public void write(String path, byte[] content) throws IOException {
  LogUtils.performance.info("qiniu:write:{} byte[] content ", path);
  QiniuPutResponse putResponse = getQiniu().put(content, path);
  if (!putResponse.isSuccess()) {
   throw new IOException(putResponse.getError());
  }
 }

 @Override
 public void write(String path, InputStream content) throws IOException {
  LogUtils.performance.info("qiniu:write:{} InputStream content", path);
  QiniuPutResponse putResponse = getQiniu().put(content, path);
  if (!putResponse.isSuccess()) {
   throw new IOException(putResponse.getError());
  }
 }

 @Override
 public void write(String path, File content) throws IOException {
  LogUtils.performance.info("qiniu:write:{} File content", path);
  QiniuPutResponse putResponse = getQiniu().put(content, path);
  if (!putResponse.isSuccess()) {
   throw new IOException(putResponse.getError());
  }
 }

 @Override
 public void delete(String path) throws IOException {
  LogUtils.performance.info("qiniu:delete:{}", path);
  getQiniu().delete(path);
 }

 /**
  * 返回外网访问的url.
  * 
  * @param filePath
  * @return
  */
 @Override
 public String getInternetUrl(String filePath) {
  return getQiniu().getInternetUrl(filePath);
 }

 /**
  * 
  * @Title: getPath
  * @Description: 返回域名外的相对路径
  * @param request
  * @return String
  * @throws @author
  *          zhoupan
  *
  * @date May 10, 2016 4:36:09 PM
  */
 public String getPath(String filePath) {
  return getQiniu().getPath(filePath);
 }

 @Override
 public long getStoreId() {
  return 1;
 }

 @Override
 public String getStoreName() {
  return "Qiniu文件存储";
 }

}
