package com.udream.support.juhe;

import java.io.Serializable;

public class BankCardEntity implements Serializable {

 /**
  * serialVersionUID
  */
 private static final long serialVersionUID = 1L;

 /**
  * 所属银行
  */
 private String bank;

 /**
  * 银行卡类型
  */
 private String type;

 /**
  * 客服电话
  */
 private String tel;

 /**
  * 银行logo
  */
 private String logo;

 /**
  * @return the bank
  */
 public String getBank() {
  return bank;
 }

 /**
  * @param bank
  *         the bank to set
  */
 public void setBank(String bank) {
  this.bank = bank;
 }

 /**
  * @return the type
  */
 public String getType() {
  return type;
 }

 /**
  * @param type
  *         the type to set
  */
 public void setType(String type) {
  this.type = type;
 }

 /**
  * @return the tel
  */
 public String getTel() {
  return tel;
 }

 /**
  * @param tel
  *         the tel to set
  */
 public void setTel(String tel) {
  this.tel = tel;
 }

 /**
  * @return the logo
  */
 public String getLogo() {
  return "http://images.juheapi.com/banklogo/" + logo;
 }

 /**
  * @param logo
  *         the logo to set
  */
 public void setLogo(String logo) {
  this.logo = logo;
 }

}
