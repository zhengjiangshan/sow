package com.udream.support.juhe;

import java.io.Serializable;

public class BankCardRespose implements Serializable{

 private static final long serialVersionUID = -2405266055448850200L;

 private String reason;
 private BankCardEntity result;
 private Integer error_code;

 public String getReason() {
  return reason;
 }

 public void setReason(String reason) {
  this.reason = reason;
 }

 public BankCardEntity getResult() {
  return result;
 }

 public void setResult(BankCardEntity result) {
  this.result = result;
 }

 public Integer getError_code() {
  return error_code;
 }

 public void setError_code(Integer error_code) {
  this.error_code = error_code;
 }
}
