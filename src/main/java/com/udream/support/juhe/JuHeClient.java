package com.udream.support.juhe;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.udream.common.DataItemResponse;
import com.udream.support.RestTemplateSupport;

@Service
public class JuHeClient extends RestTemplateSupport {
 /** The Constant DEFAULT_BASE_URL. */
 public static final String DEFAULT_BASE_URL = "http://bankcardsilk.api.juhe.cn";
 public static final String KEY = "c1d8dd71bb54741d1c06af4fa570de3b";

 /**
  * Instantiates a new kuaidi100 client.
  */
 public JuHeClient() {
  super();
  this.setBaseUrl(DEFAULT_BASE_URL);
 }

 public DataItemResponse<BankCardRespose> queryBankCardByNum(String num) {
  DataItemResponse<BankCardRespose> dataResponse = new DataItemResponse<BankCardRespose>();
  try {
   Map<String, String> params = new HashMap<String, String>();
   params.put("num", num);
   params.put("key", KEY);
   return this.getItem("/bankcardsilk/query.php?num={num}&key={key}", params, BankCardRespose.class);
  } catch (Throwable e) {
   dataResponse.onException(e);
  }
  return dataResponse;
 }

 public static void main(String[] args) {
  JuHeClient client = new JuHeClient();
  DataItemResponse<BankCardRespose> result = client.queryBankCardByNum("6226097804624238");
  System.out.println(result.toString());
 }
}
