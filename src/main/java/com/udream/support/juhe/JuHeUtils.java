package com.udream.support.juhe;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.udream.common.Constant;
import com.udream.common.DataResponse;

public class JuHeUtils {

	private static final Logger log = LoggerFactory.getLogger(JuHeUtils.class);

	private static final String CHARSET = "utf-8";

	private static String BANK_CARD_URL = "http://bankcardsilk.api.juhe.cn/bankcardsilk/query.php?num=NUM&key=KEY";

	private static String ID_CARD_URL = "http://apis.juhe.cn/idcard/index?key=KEY&cardno=NUM&dtype=json";

	private static JuHeUtils instance = null;

	private JuHeUtils() {
	};

	public static synchronized JuHeUtils getInstance() {
		if (null == instance) {
			instance = new JuHeUtils();
		}
		return instance;
	}

	/**
	 * 根据银行卡号查询对应银行归属信息
	 * <p>
	 * 根据correct属性判断银行卡是否正确
	 * </p>
	 * 
	 * @param num
	 *            银行卡号信息
	 */
	public static BankCardEntity queryBankCardByNum(String num) {
		log.info("bank card:{}", num);
		BANK_CARD_URL = BANK_CARD_URL.replace("NUM", num);
		BANK_CARD_URL = BANK_CARD_URL.replace("KEY", "c1d8dd71bb54741d1c06af4fa570de3b");
		BankCardEntity bankCard = new BankCardEntity();
		try {
			HttpGet request = new HttpGet(BANK_CARD_URL);
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpResponse response = httpClient.execute(request);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity(), CHARSET);
				JSONObject jsonObject = JSONObject.parseObject(result);
				String errorCode = jsonObject.get("error_code").toString();
				if (errorCode.equals("0")) {
					String _result = jsonObject.get("result").toString();
					JSONObject json = JSONObject.parseObject(_result);
					bankCard = JSONObject.toJavaObject(json, BankCardEntity.class);
				} else {
				}
				log.info("Query Bank card Response: " + result);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Query Bank card exception: " + e.getMessage());
		}
		return bankCard;
	}

	/**
	 * 身份证有效性验证
	 * 
	 * @param num
	 *            身份证号码
	 * @return
	 */
	public static DataResponse queryIdCardByNum(String num) {
		log.info("id card:{}", num);
		ID_CARD_URL = ID_CARD_URL.replace("NUM", num);
		ID_CARD_URL = ID_CARD_URL.replace("KEY", "9a76e3b4afa46eee23d8f01b5c92fc0b");
		DataResponse response = new DataResponse();
		try {
			HttpGet request = new HttpGet(ID_CARD_URL);
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpResponse result = httpClient.execute(request);
			if (result.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				String resultStr = EntityUtils.toString(result.getEntity(), CHARSET);
				JSONObject jsonObject = JSONObject.parseObject(resultStr);
				String errorCode = jsonObject.get("error_code").toString();
				String reason = jsonObject.get("reason").toString();
				if (errorCode.equals("0")) {
					response.setStatus(Constant.RETURN_CODE_SUCCESS);
					response.setMsg(reason);
				} else {
					response.setStatus(Constant.RETURN_CODE_FAIL);
					response.setMsg(reason);
				}
			} else {
				response.setStatus(Constant.RETURN_CODE_FAIL);
				response.setMsg("调用第三方失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Query ID card exception: " + e.getMessage());
		}
		return response;
	}
}
