
package com.udream.support.alidayu;

import com.taobao.api.ApiException;
import com.taobao.api.domain.BizResult;

/**
 * BizResultExtend.
 */
public class BizResultExtend extends BizResult {

 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;

 /**
  * 异常的时候设置错误代码、消息和是否成功.
  *
  * @param e
  *         异常
  * @return the biz result extend
  */
 public BizResultExtend onException(ApiException e) {
  this.setErrCode(e.getErrCode());
  this.setMsg(e.getErrMsg());
  this.setSuccess(false);
  return this;
 }

}
