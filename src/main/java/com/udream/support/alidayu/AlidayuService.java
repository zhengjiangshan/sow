package com.udream.support.alidayu;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The Class AlidayuService.
 */
@Component
public class AlidayuService {

 /** The url. */
 @Value("${alidayu.url}")
 private String url;

 /** The appkey. */
 @Value("${alidayu.appkey}")
 private String appkey;

 /** The secret. */
 @Value("${alidayu.secret}")
 private String secret;

 /**
  * 默认签名.
  */
 @Value("${alidayu.signName}")
 private String signName;

 /**
  * 验证码模块代码.
  */
 @Value("${alidayu.verityCodeTemplateCode}")
 private String verityCodeTemplateCode;

 /** The alidayu support. */
 AlidayuSupport alidayuSupport;

 /**
  * Gets the alidayu support.
  *
  * @return the alidayu support
  */
 public AlidayuSupport getAlidayuSupport() {
  if (alidayuSupport == null) {
   alidayuSupport = this.onBuildAlidayuSupport();
  }
  return alidayuSupport;
 }

 /**
  * On build alidayu support.
  *
  * @return the alidayu support
  */
 public AlidayuSupport onBuildAlidayuSupport() {
  AlidayuSupport alidayuSupport = new AlidayuSupport();
  alidayuSupport.setUrl(url);
  alidayuSupport.setAppkey(appkey);
  alidayuSupport.setSecret(secret);
  alidayuSupport.setSignName(signName);
  alidayuSupport.setVerityCodeTemplateCode(verityCodeTemplateCode);
  return alidayuSupport;
 }

}
