package com.udream.support.alidayu;

import java.util.HashMap;
import java.util.Map;

import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.TaobaoResponse;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import com.udream.support.CommonSupport;
import com.udream.support.FastjsonUtils;

/**
 * AlidayuSupport.
 */
public class AlidayuSupport {

 /** The Constant EXTEND_DEFAULT. */
 public static final String EXTEND_DEFAULT = "123456";

 /** The Constant SMS_TYPE_NORMAL. */
 public static final String SMS_TYPE_NORMAL = "normal";

 /** The Constant URL_DEFAULT. */
 public static final String URL_DEFAULT = "http://gw.api.taobao.com/router/rest";
 /** The url. */
 private String url = URL_DEFAULT;

 /** The appkey. */
 private String appkey;

 /** The secret. */
 private String secret;
 /**
  * 默认签名.
  */
 private String signName;

 /**
  * 验证码模块代码.
  */
 private String verityCodeTemplateCode;

 /**
  * Gets the url.
  *
  * @return the url
  */
 public String getUrl() {
  return url;
 }

 /**
  * Sets the url.
  *
  * @param url
  *         the url
  */
 public void setUrl(String url) {
  this.url = url;
 }

 /**
  * Gets the appkey.
  *
  * @return the appkey
  */
 public String getAppkey() {
  return appkey;
 }

 /**
  * Sets the appkey.
  *
  * @param appkey
  *         the appkey
  */
 public void setAppkey(String appkey) {
  this.appkey = appkey;
 }

 /**
  * Gets the secret.
  *
  * @return the secret
  */
 public String getSecret() {
  return secret;
 }

 /**
  * Sets the secret.
  *
  * @param secret
  *         the secret
  */
 public void setSecret(String secret) {
  this.secret = secret;
 }

 /**
  * Gets the sign name.
  *
  * @return the sign name
  */
 public String getSignName() {
  return signName;
 }

 /**
  * Sets the sign name.
  *
  * @param signName
  *         the sign name
  */
 public void setSignName(String signName) {
  this.signName = signName;
 }

 /**
  * Gets the verity code template code.
  *
  * @return the verity code template code
  */
 public String getVerityCodeTemplateCode() {
  return verityCodeTemplateCode;
 }

 /**
  * Sets the verity code template code.
  *
  * @param verityCodeTemplateCode
  *         the verity code template code
  */
 public void setVerityCodeTemplateCode(String verityCodeTemplateCode) {
  this.verityCodeTemplateCode = verityCodeTemplateCode;
 }

 /** The client. */
 TaobaoClient client = null;

 /**
  * Gets the client.
  *
  * @return the client
  */
 public TaobaoClient getClient() {
  if (client == null) {
   this.client = this.buildClient();
  }
  return this.client;
 }

 /**
  * Builds the client.
  *
  * @return the taobao client
  */
 public TaobaoClient buildClient() {
  return new DefaultTaobaoClient(url, appkey, secret);
 }

 /**
  * 发送短信.
  *
  * @param signName
  *         签名
  * @param mobile
  *         目标手机号码
  * @param templateCode
  *         短信模板
  * @param params
  *         参数(替换模板里面定义的变量)
  * @return 发送结果
  */
 public AlibabaAliqinFcSmsNumSendResponse send(String signName, String mobile, String templateCode, Map<String, String> params) {
  AlibabaAliqinFcSmsNumSendResponseExtend res = new AlibabaAliqinFcSmsNumSendResponseExtend();
  AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
  req.setExtend(EXTEND_DEFAULT);
  req.setSmsType(SMS_TYPE_NORMAL);
  req.setSmsFreeSignName(signName);
  if (params != null && !params.isEmpty()) {
   req.setSmsParamString(FastjsonUtils.string(params));
  }
  req.setRecNum(mobile);
  req.setSmsTemplateCode(templateCode);
  try {
   return this.getClient().execute(req);
  } catch (ApiException e) {
   res.onException(e);
  }
  return res;
 }

 /**
  * 发送默认验证码短信.
  *
  * @param mobile
  *         目标手机号码.
  * @param code
  *         验证码.
  * @return 发送结果.
  */
 public AlibabaAliqinFcSmsNumSendResponse doSendVerifyCode(String mobile, String code) {
  Map<String, String> params = new HashMap<String, String>();
  params.put("code", code);
  params.put("number", code);
  return this.send(signName, mobile, verityCodeTemplateCode, params);
 }

 /**
  * 返回用户友好的消息提示.
  * 
  * @return
  */
 public String getDisplayMessage(TaobaoResponse response) {
  StringBuilder sb = new StringBuilder();
  if (CommonSupport.isNotBlank(response.getSubMsg())) {
   sb.append(response.getSubMsg());
  }
  String displayMessage = this.getDisplayMessage(response.getSubCode());
  if (CommonSupport.isNotBlank(displayMessage)) {
   sb.append("(").append(displayMessage).append(")");
  }
  return sb.toString();
 }

 /**
  * 返回用户友好的消息提示.
  * 
  * @param subCode
  * @return
  */
 public String getDisplayMessage(String subCode) {
  // isv.BUSINESS_LIMIT_CONTROL 触发业务流控限制
  // 短信验证码，使用同一个签名，对同一个手机号码发送短信验证码，支持1条/分钟，5条/小时，10条/天。一个手机号码通过阿里大于平台只能收到40条/天。
  // 短信通知，使用同一签名、同一模板，对同一手机号发送短信通知，允许每天50条（自然日）。
  if (CommonSupport.equalsIgnoreCase("isv.BUSINESS_LIMIT_CONTROL", subCode)) {
   return "短信验证码，使用同一个签名，对同一个手机号码发送短信验证码，支持1条/分钟，5条/小时，10条/天。一个手机号码通过平台只能收到40条/天。";
  }
  return "";
 }

}
