
package com.udream.support.alidayu;

import com.taobao.api.ApiException;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

/**
 * 扩展阿里短信发送结果.
 */
public class AlibabaAliqinFcSmsNumSendResponseExtend extends AlibabaAliqinFcSmsNumSendResponse {

 /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;

 /**
  * 异常的时候,设置错误代码和消息.
  *
  * @param e
  *         the e
  */
 public void onException(ApiException e) {
  this.setErrorCode(e.getErrCode());
  this.setMsg(e.getErrMsg());
  this.setResult(new BizResultExtend().onException(e));
 }
}
