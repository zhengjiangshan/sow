package com.udream.support;

/**
 * GenericMybatisDao.
 * 
 * @param <T>
 *         the generic type
 * @author zhoupan.
 */
public abstract class GenericRunnable<T> implements Runnable {
 T owner;

 public GenericRunnable(T owner) {
  super();
  this.owner = owner;
 }

 @Override
 public void run() {
  this.run(this.owner);
 }

 public abstract void run(T owner);

}