package com.udream.support.kuaidi100;

import java.util.ArrayList;
import java.util.List;

/**
 * 快递详情.
 * 
 * @author zhoupan
 *
 */
public class KuaidiDetail {

 private String message = "ok";
 private String nu = "";
 private String ischeck;
 private String condition;
 private String com;
 private String status;
 private String state;
 List<KuaidiStep> data = new ArrayList<KuaidiStep>();

 public String getMessage() {
  return message;
 }

 public void setMessage(String message) {
  this.message = message;
 }

 public String getNu() {
  return nu;
 }

 public void setNu(String nu) {
  this.nu = nu;
 }

 public String getIscheck() {
  return ischeck;
 }

 public void setIscheck(String ischeck) {
  this.ischeck = ischeck;
 }

 public String getCondition() {
  return condition;
 }

 public void setCondition(String condition) {
  this.condition = condition;
 }

 public String getCom() {
  return com;
 }

 public void setCom(String com) {
  this.com = com;
 }

 public String getStatus() {
  return status;
 }

 public void setStatus(String status) {
  this.status = status;
 }

 public String getState() {
  return state;
 }

 public void setState(String state) {
  this.state = state;
 }

 public List<KuaidiStep> getData() {
  return data;
 }

 public void setData(List<KuaidiStep> data) {
  this.data = data;
 }

}
