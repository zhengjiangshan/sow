package com.udream.support.kuaidi100;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.udream.common.DataItemResponse;
import com.udream.support.CommonSupport;
import com.udream.support.DateUtils;
import com.udream.support.RestTemplateSupport;

/**
 * Kuaidi100Client.
 * 
 * @author zhoupan.
 */
@Service
public class Kuaidi100Client extends RestTemplateSupport {

	/** The Constant URL_TEMPLATE_QUERY. */
	public static final String URL_TEMPLATE_QUERY = "/query?type={type}&postid={postid}&id=1&valicode&temp=0.08001715072286408";

	/** The Constant DEFAULT_BASE_URL. */
	public static final String DEFAULT_BASE_URL = "http://www.kuaidi100.com";

	/**
	 * Instantiates a new kuaidi100 client.
	 */
	public Kuaidi100Client() {
		super();
		this.setBaseUrl(DEFAULT_BASE_URL);
	}

	/**
	 * 获取快递公司信息.
	 *
	 * @param num
	 *            the num
	 * @return the data item response
	 */
	public DataItemResponse<List<KuaidiNum>> kuaidiNum(String num) {
		DataItemResponse<List<KuaidiNum>> dataResponse = new DataItemResponse<List<KuaidiNum>>();
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("num", num);
			return this.getItems("/autonumber/auto?num={num}", params, KuaidiNum.class);
		} catch (Throwable e) {
			dataResponse.onException(e);
		}
		return dataResponse;
	}

	/**
	 * 获取快递物流明细.
	 *
	 * @param num
	 *            the num
	 * @return the data item response
	 */
	public DataItemResponse<KuaidiDetail> kuaidiDetail(String num) {
		DataItemResponse<KuaidiDetail> dataResponse = new DataItemResponse<KuaidiDetail>();
		try {
			DataItemResponse<List<KuaidiNum>> numResponse = this.kuaidiNum(num);
			CommonSupport.checkState(1, numResponse.success(), numResponse.getMsg());
			CommonSupport.checkState(1, numResponse.getItem() != null, "无法获取快递单物流公司信息.");
			CommonSupport.checkState(1, numResponse.getItem().size() > 0, "无法获取快递单物流公司信息.");
			String type = numResponse.getItem().get(0).getCode();
			CommonSupport.checkState(1, CommonSupport.isNotBlank(type), "无法获取快递物流公司类型.");
			Map<String, String> params = new HashMap<String, String>();
			params.put("type", type);
			params.put("postid", num);
			return this.getItem(URL_TEMPLATE_QUERY, params, KuaidiDetail.class);
		} catch (Throwable e) {
			dataResponse.onException(e);
		}
		return dataResponse;
	}

	/**
	 * 物流信息追踪
	 * 
	 * @param num
	 *            物流单号
	 * @return
	 */
	public String kuaidiTrace(String num) {
		StringBuffer sb = new StringBuffer();
		DataItemResponse<List<KuaidiNum>> numResponse = this.kuaidiNum(num);
		if (null == numResponse.getItem() || !(numResponse.getItem().size() > 0)) {
			sb.append("无法获取快递单物流公司信息.");
		} else if (CommonSupport.isBlank(numResponse.getItem().get(0).getCode())) {
			sb.append("无法获取快递物流公司类型.");
		} else {
			DataItemResponse<KuaidiDetail> detailResponse = this.kuaidiDetail(num);
			List<KuaidiStep> data = detailResponse.getItem().getData();
			sb.append("<ul>");
			if (null == data || data.size() == 0) {
				sb.append("<li class=\"first\">");
				sb.append("<i class=\"node-icon\"></i><span class=\"time\">");
				sb.append(DateUtils.formatDate(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
				sb.append("</span><span class=\"txt\">");
				sb.append("暂无物流信息！");
				sb.append("</span></li>");
			} else {
				int i = 0;
				for (KuaidiStep step : data) {
					if (i == 0) {
						sb.append("<li class=\"first\">");
					} else {
						sb.append("<li>");
					}
					sb.append("<i class=\"node-icon\"></i><span class=\"time\">");
					sb.append(step.getTime());
					sb.append("</span><span class=\"txt\">");
					sb.append(step.getContext());
					sb.append("</span></li>");
					i++;
				}
			}
			sb.append("</ul>");
		}
		return sb.toString();
	}
}
