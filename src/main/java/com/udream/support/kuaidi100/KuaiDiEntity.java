package com.udream.support.kuaidi100;

/**
 * Created by Administrator on 2017/6/22.
 */

public class KuaiDiEntity {

    /**
     *         "companyname": "申通快递",
     "shortname": "申通",
     "tel": "400-889-5543",
     "url": "st",
     "code": "shentong",
     "hasvali": 0,
     "comurl": "http://www.sto.cn",
     "isavailable": "0",
     "promptinfo": "系统升级，请到申通官网查询",
     "testnu": "668031148649",
     "freg": "^[0-9]{12}$",
     "freginfo": "申通单号由12位数字组成，常见以268*、368*、58*等开头",
     "telcomplaintnum": "95543",
     "queryurl": "http://q.sto.cn//result.aspx?data%5BSearch%5D%5Btype%5D=single&wen=",
     "serversite": ""
     */
    private String companyname;
    private String shortname;
    private String tel;
    private String url;
    private String code;
    private String hasvali;
    private String comurl;
    private String isavailable;
    private String promptinfo;
    private String testnu;
    private String freg;
    private String freginfo;
    private String telcomplaintnum;
    private String queryurl;
    private String serversite;

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getHasvali() {
        return hasvali;
    }

    public void setHasvali(String hasvali) {
        this.hasvali = hasvali;
    }

    public String getComurl() {
        return comurl;
    }

    public void setComurl(String comurl) {
        this.comurl = comurl;
    }

    public String getIsavailable() {
        return isavailable;
    }

    public void setIsavailable(String isavailable) {
        this.isavailable = isavailable;
    }

    public String getPromptinfo() {
        return promptinfo;
    }

    public void setPromptinfo(String promptinfo) {
        this.promptinfo = promptinfo;
    }

    public String getTestnu() {
        return testnu;
    }

    public void setTestnu(String testnu) {
        this.testnu = testnu;
    }

    public String getFreg() {
        return freg;
    }

    public void setFreg(String freg) {
        this.freg = freg;
    }

    public String getFreginfo() {
        return freginfo;
    }

    public void setFreginfo(String freginfo) {
        this.freginfo = freginfo;
    }

    public String getTelcomplaintnum() {
        return telcomplaintnum;
    }

    public void setTelcomplaintnum(String telcomplaintnum) {
        this.telcomplaintnum = telcomplaintnum;
    }

    public String getQueryurl() {
        return queryurl;
    }

    public void setQueryurl(String queryurl) {
        this.queryurl = queryurl;
    }

    public String getServersite() {
        return serversite;
    }

    public void setServersite(String serversite) {
        this.serversite = serversite;
    }
}
