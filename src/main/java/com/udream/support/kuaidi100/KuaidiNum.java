package com.udream.support.kuaidi100;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 快递单物流公司信息.
 * 
 * @author zhoupan
 *
 */
public class KuaidiNum {

 @JsonProperty("comCode")
 private String code;
 private String id;
 @JsonProperty("noCount")
 private String count;
 @JsonProperty("noPre")
 private String pre;
 @JsonProperty("startTime")
 private String time;

 public String getCode() {
  return code;
 }

 public void setCode(String code) {
  this.code = code;
 }

 public String getId() {
  return id;
 }

 public void setId(String id) {
  this.id = id;
 }

 public String getCount() {
  return count;
 }

 public void setCount(String count) {
  this.count = count;
 }

 public String getPre() {
  return pre;
 }

 public void setPre(String pre) {
  this.pre = pre;
 }

 public String getTime() {
  return time;
 }

 public void setTime(String time) {
  this.time = time;
 }

}
