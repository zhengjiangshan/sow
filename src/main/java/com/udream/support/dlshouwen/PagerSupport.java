package com.udream.support.dlshouwen;

import com.udream.support.PaginationSupport;
import com.udream.support.QueryFilter;

/**
 * 
 * @author zhoupan
 *
 */
public interface PagerSupport {

 /**
  * Query.
  * 
  * @param filter
  *         the filter
  * @param pageNum
  *         the page num
  * @param pageSize
  *         the page size
  * @return the pagination support
  */
 public PaginationSupport query(QueryFilter filter, int pageNum, int pageSize);
}
