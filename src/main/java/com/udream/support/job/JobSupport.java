package com.udream.support.job;

/**
 * JobSupport.
 */
public interface JobSupport {

 /**
  * Do job.
  */
 public void doJob();
}
