package com.udream.support.job;

/**
 * JobSupport.
 */
public interface EveryDayJob {

 /**
  * Do job.
  */
 public void doJob();
}
