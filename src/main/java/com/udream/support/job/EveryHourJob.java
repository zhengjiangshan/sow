
package com.udream.support.job;

/**
 * EveryHour.
 */
public interface EveryHourJob {

 /**
  * Do job.
  */
 public void doJob();
}
