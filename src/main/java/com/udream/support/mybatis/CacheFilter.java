
package com.udream.support.mybatis;

public interface CacheFilter {

 public boolean accept(String appId, String cacheId);
}
