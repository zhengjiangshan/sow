package com.udream.support.alipay;

/**
 * Created by admin on 2017/6/19.
 */
public class CancelPayRequest extends CancelPayRequestBase {
 
 public CancelPayRequest outTradeNo(String outTradeNo) {
  this.setOut_trade_no(outTradeNo);
  return this;
 }
 
 public CancelPayRequest tradeNo(String tradeNo) {
  this.setTrade_no(tradeNo);
  return this;
 }
 
}
