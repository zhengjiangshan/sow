package com.udream.support.alipay;

/**
 * 订单查询请求
 */
public class QueryTradeRequestBase {
 
 /**
  * 订单支付时传入的商户订单号,和支付宝交易号不能同时为空。
  * trade_no,out_trade_no如果同时存在优先取trade_no
  */
 private String out_trade_no;
 private String trade_no;
 
 public String getOut_trade_no() {
  return out_trade_no;
 }
 
 public void setOut_trade_no(String out_trade_no) {
  this.out_trade_no = out_trade_no;
 }
 
 public String getTrade_no() {
  return trade_no;
 }
 
 public void setTrade_no(String trade_no) {
  this.trade_no = trade_no;
 }
}
