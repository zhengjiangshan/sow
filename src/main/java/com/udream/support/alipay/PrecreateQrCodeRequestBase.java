package com.udream.support.alipay;

import java.math.BigDecimal;

/**
 * 请求支付宝支付二维码请求
 */
public class PrecreateQrCodeRequestBase {
 
 /**
  * 商户订单号,64个字符以内、只能包含字母、数字、下划线；需保证在商户端不重复
  */
 private String out_trade_no;
 
 /**
  * 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000] 如果同时传入了【打折金额】，【不可打折金额】，【订单总金额】三者，则必须满足如下条件：【订单总金额】=【打折金额】+【不可打折金额】
  */
 private String total_amount;
 
 /**
  * 自动生成
  */
 private Long total_amount_cent;
 /**
  * 订单标题
  */
 private String subject;
 
 /**
  * 对交易或商品的描述(可选)
  */
 private String body;
 
 
 public String getOut_trade_no() {
  return out_trade_no;
 }
 
 public void setOut_trade_no(String out_trade_no) {
  this.out_trade_no = out_trade_no;
 }
 
 public String getTotal_amount() {
  return new BigDecimal(this.getTotal_amount_cent()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN).doubleValue() + "";
 }
 
 public void setTotal_amount(String total_amount) {
  this.total_amount = total_amount;
 }
 
 public Long getTotal_amount_cent() {
  return total_amount_cent;
 }
 
 public void setTotal_amount_cent(Long total_amount_cent) {
  this.total_amount_cent = total_amount_cent;
 }
 
 public String getSubject() {
  return subject;
 }
 
 public void setSubject(String subject) {
  this.subject = subject;
 }
 
 public String getBody() {
  return body;
 }
 
 public void setBody(String body) {
  this.body = body;
 }

 @Override
 public String toString() {
  return "PrecreateQrCodeRequestBase [out_trade_no=" + out_trade_no + ", total_amount=" + total_amount + ", total_amount_cent="
   + total_amount_cent + ", subject=" + subject + ", body=" + body + "]";
 }

}
