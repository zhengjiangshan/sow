package com.udream.support.alipay;

/**
 * Created by admin on 2017/6/19.
 */
public class AliPayBasicResopnse {
 
 private boolean result;
 private String code;
 private String msg;
 private String subCode;
 private String subMsg;
 
 public boolean isResult() {
  return result;
 }
 
 public void setResult(boolean result) {
  this.result = result;
 }
 
 public String getCode() {
  return code;
 }
 
 public void setCode(String code) {
  this.code = code;
 }
 
 public String getMsg() {
  return msg;
 }
 
 public void setMsg(String msg) {
  this.msg = msg;
 }
 
 public String getSubCode() {
  return subCode;
 }
 
 public void setSubCode(String subCode) {
  this.subCode = subCode;
 }
 
 public String getSubMsg() {
  return subMsg;
 }
 
 public void setSubMsg(String subMsg) {
  this.subMsg = subMsg;
 }
}
