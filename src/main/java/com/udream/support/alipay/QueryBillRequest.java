package com.udream.support.alipay;

/**
 * 查询账单请求
 */
public class QueryBillRequest extends QueryBillRequestBase {
 
 public QueryBillRequest billType(String billType) {
  this.setBillType(billType);
  return this;
 }
 
 public QueryBillRequest billDate(String billDate) {
  this.setBillDate(billDate);
  return this;
 }
 
}
