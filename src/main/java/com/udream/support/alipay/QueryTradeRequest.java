package com.udream.support.alipay;

/**
 * 支付宝查单请求
 */
public class QueryTradeRequest extends QueryTradeRequestBase {
 
 public QueryTradeRequestBase outTradeNo(String outTradeNo) {
  this.setOut_trade_no(outTradeNo);
  return this;
 }
 
 public QueryTradeRequest tradeNo(String tradeNo) {
  this.setTrade_no(tradeNo);
  return this;
 }
 
}
