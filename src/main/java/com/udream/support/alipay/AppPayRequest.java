package com.udream.support.alipay;

/**
 * Created by admin on 2017/6/16.
 */
public class AppPayRequest extends AppPayRequestBase {
 
 public AppPayRequest() {
 }
 
 public AppPayRequest tradNo(String tradeNumber) {
  this.setTradNo(tradeNumber);
  return this;
 }
 
 public AppPayRequest subject(String subject) {
  this.setSubject(subject);
  return this;
 }
 
 public AppPayRequest body(String body) {
  this.setBody(body);
  return this;
 }
 
 public AppPayRequest totalAmount(Long totalAmount) {
  this.setTotalAmount(totalAmount);
  return this;
 }
 
 public AppPayRequest totalAmountString(String totalAmountString) {
  this.setTotalAmountString(totalAmountString);
  return this;
 }
 
 public AppPayRequest passBackParams(String passBackParams) {
  this.setPassBackParams(passBackParams);
  return this;
 }
 
}
