package com.udream.support.alipay;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.udream.support.MoneyUtils;

/**
 * 请求生成支付宝支付报文请求
 */
public class AppPayRequestBase {
 /**
  * 系统订单号
  */
 private String tradNo;
 /**
  * 订单交易标题
  */
 private String subject;
 /**
  * 订单总价：单位：分
  */
 private Long totalAmount;
 
 /**
  * 订单总价：转换为String 单位为元
  */
 private String totalAmountString;
 /**
  * j交易描述信息
  */
 private String body;
 /**
  * 支付宝公共回传参数
  */
 private String passBackParams;
 
 public String getTradNo() {
  return tradNo;
 }
 
 public void setTradNo(String tradNo) {
  this.tradNo = tradNo;
 }
 
 public String getSubject() {
  return subject;
 }
 
 public void setSubject(String subject) {
  this.subject = subject;
 }
 
 public Long getTotalAmount() {
  return totalAmount;
 }
 
 public void setTotalAmount(Long totalAmount) {
  this.totalAmount = totalAmount;
 }
 
 public String getBody() {
  return body;
 }
 
 public void setBody(String body) {
  this.body = body;
 }
 
 public String getPassBackParams() {
  return passBackParams;
 }
 
 public void setPassBackParams(String passBackParams) {
  //公共回传参数，如果请求时传递了该参数，则返回给商户时会在异步通知时将该参数原样返回。本参数必须进行UrlEncode之后才可以发送给付宝
  try {
   this.passBackParams = URLEncoder.encode(passBackParams, "UTF-8");
  } catch (UnsupportedEncodingException e) {
   e.printStackTrace();
  }
 }
 
 public String getTotalAmountString() {
  return MoneyUtils.fenToYuan(this.getTotalAmount()).doubleValue() + "";
 }
 
 public void setTotalAmountString(String totalAmountString) {
  this.totalAmountString = totalAmountString;
 }
}
