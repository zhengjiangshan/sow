package com.udream.support.alipay;

import java.util.Date;

import com.udream.common.Constant;

/**
 * 支付宝查询支付订单响应接口
 */
public class QueryTradeResponse extends AliPayBasicResopnse {
 private String tradeNo;
 private String outTradeNo;
 private String buyerLoginId;
 private String tradeStatus;
 private String totalAmout;
 private Date sendPayDate;
 private Integer tradeStatusInteger;
 
 public String getTradeNo() {
  return tradeNo;
 }
 
 public void setTradeNo(String tradeNo) {
  this.tradeNo = tradeNo;
 }
 
 public String getOutTradeNo() {
  return outTradeNo;
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getBuyerLoginId() {
  return buyerLoginId;
 }
 
 public void setBuyerLoginId(String buyerLoginId) {
  this.buyerLoginId = buyerLoginId;
 }
 
 public String getTradeStatus() {
  return tradeStatus;
 }
 
 public void setTradeStatus(String tradeStatus) {
  this.tradeStatus = tradeStatus;
 }
 
 public String getTotalAmout() {
  return totalAmout;
 }
 
 public void setTotalAmout(String totalAmout) {
  this.totalAmout = totalAmout;
 }
 
 public Date getSendPayDate() {
  return sendPayDate;
 }
 
 public void setSendPayDate(Date sendPayDate) {
  this.sendPayDate = sendPayDate;
 }
 
 public Integer getTradeStatusInteger() {
  if (this.getTradeStatus().trim().equals("WAIT_BUYER_PAY"))
   return Constant.ALIPAY_TRADE_STATUS_WAIT_BUYER_PAY;
  else if (this.getTradeStatus().trim().equals("TRADE_CLOSED"))
   return Constant.ALIPAY_TRADE_STATUS_TRADE_CLOSED;
  else if (this.getTradeStatus().trim().equals("TRADE_SUCCESS"))
   return Constant.ALIPAY_TRADE_STATUS_TRADE_SUCCESS;
  else if (this.getTradeStatus().trim().equals("TRADE_FINISHED"))
   return Constant.ALIPAY_TRADE_STATUS_TRADE_FINISHED;
  return -1;
 }
 
 public void setTradeStatusInteger(Integer tradeStatusInteger) {
  this.tradeStatusInteger = tradeStatusInteger;
 }
}
