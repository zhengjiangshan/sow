package com.udream.support.alipay;

import java.util.Date;

/**
 * 退款响应
 */
public class RefundResponse extends AliPayBasicResopnse {
 
 /**
  * 支付宝交易号
  */
 private String tradeNo;
 
 /**
  * 商户订单号
  */
 private String outTradeNo;
 
 /**
  * 	买家支付宝用户号，该参数已废弃，请不要使用
  */
 private String openId;
 
 /**
  * 	用户的登录id
  */
 private String buyerLoginId;
 
 /**
  * 本次退款是否发生了资金变化
  */
 private String fundChange;
 
 /**
  * 	退款总金额
  */
 private String refundFee;
 
 /**
  * 	退款支付时间
  */
 private Date gmtRefundPay;
 
 /**
  * 	交易在支付时候的门店名称
  */
 private String storeName;
 
 /**
  * 	买家在支付宝的用户id
  */
 private String buyerUserId;
 
 public String getTradeNo() {
  return tradeNo;
 }
 
 public void setTradeNo(String tradeNo) {
  this.tradeNo = tradeNo;
 }
 
 public String getOutTradeNo() {
  return outTradeNo;
 }
 
 public void setOutTradeNo(String outTradeNo) {
  this.outTradeNo = outTradeNo;
 }
 
 public String getOpenId() {
  return openId;
 }
 
 public void setOpenId(String openId) {
  this.openId = openId;
 }
 
 public String getBuyerLoginId() {
  return buyerLoginId;
 }
 
 public void setBuyerLoginId(String buyerLoginId) {
  this.buyerLoginId = buyerLoginId;
 }
 
 public String getFundChange() {
  return fundChange;
 }
 
 public void setFundChange(String fundChange) {
  this.fundChange = fundChange;
 }
 
 public String getRefundFee() {
  return refundFee;
 }
 
 public void setRefundFee(String refundFee) {
  this.refundFee = refundFee;
 }
 
 public Date getGmtRefundPay() {
  return gmtRefundPay;
 }
 
 public void setGmtRefundPay(Date gmtRefundPay) {
  this.gmtRefundPay = gmtRefundPay;
 }
 
 public String getStoreName() {
  return storeName;
 }
 
 public void setStoreName(String storeName) {
  this.storeName = storeName;
 }
 
 public String getBuyerUserId() {
  return buyerUserId;
 }
 
 public void setBuyerUserId(String buyerUserId) {
  this.buyerUserId = buyerUserId;
 }
}
