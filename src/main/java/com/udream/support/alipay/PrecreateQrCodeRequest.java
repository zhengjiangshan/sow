package com.udream.support.alipay;

/**
 * Created by admin on 2017/6/19.
 */
public class PrecreateQrCodeRequest extends PrecreateQrCodeRequestBase {
 
 public PrecreateQrCodeRequest tradeNo(String tradeNo) {
  this.setOut_trade_no(tradeNo);
  return this;
 }
 
 public PrecreateQrCodeRequest totalAmount(Long totalAmount) {
  this.setTotal_amount_cent(totalAmount);
  return this;
 }
 
 public PrecreateQrCodeRequest subject(String subject) {
  this.setSubject(subject);
  return this;
 }
 
 public PrecreateQrCodeRequest body(String body) {
  this.setBody(body);
  return this;
 }
 
}
