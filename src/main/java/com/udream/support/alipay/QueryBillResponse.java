package com.udream.support.alipay;

/**
 * 查账响应
 */
public class QueryBillResponse extends AliPayBasicResopnse {
 
 private String billDownloadUrl;
 
 public String getBillDownloadUrl() {
  return billDownloadUrl;
 }
 
 public void setBillDownloadUrl(String billDownloadUrl) {
  this.billDownloadUrl = billDownloadUrl;
 }
}
