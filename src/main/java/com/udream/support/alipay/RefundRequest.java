package com.udream.support.alipay;

/**
 * 退款请求
 */
public class RefundRequest extends RefundRequestBase {
 
 public RefundRequest outTradeNo(String outTradeNo) {
  this.setOutTradeNo(outTradeNo);
  return this;
 }
 
 public RefundRequest tradeNo(String tradeNo) {
  this.setTradeNo(tradeNo);
  return this;
 }
 
 public RefundRequest refundAmount(String refundAmount) {
  this.setRefundAmount(refundAmount);
  return this;
 }
 
 public RefundRequest refundReason(String refundReason) {
  this.setRefundReason(refundReason);
  return this;
 }
 
 public RefundRequest outRequestNo(String outRequestNo) {
  this.setOutRequestNo(outRequestNo);
  return this;
 }
 
 public RefundRequest operatorId(String operatorId) {
  this.setOperatorId(operatorId);
  return this;
 }
 
 public RefundRequest storeId(String storeId) {
  this.setStoreId(storeId);
  return this;
 }
 
}
