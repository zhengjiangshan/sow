package com.udream.support.getui;

import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.AppMessage;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.exceptions.RequestException;
import com.gexin.rp.sdk.http.IGtPush;

/**
 * 包装个推SDK. <code>
 * 配置.
  String id = "";
  String key = "";
  String secret = "";
  String masterSecret = "";
  GetuiSupport getui = new GetuiSupport();
  getui.id = id;
  getui.key = key;
  getui.secret = secret;
  getui.masterSecret = masterSecret;
  
  // 发链接消息:定义"点击链接打开通知模板"，并设置标题、内容、链接
  LinkTemplateBuilder linkTemplate = getui.linkTemplate();
  linkTemplate.setStyle(getui.notifyStyle().title("欢迎使用个推!").text("这是一条推送消息~"));
  linkTemplate.setUrl("http://getui.com");
  // 透传消息:
  NotificationTemplateBuilder notificationTemplate = getui.notificationTemplate();
  notificationTemplate.forceStart(true);
  notificationTemplate.style(getui.notifyStyle().title("欢迎使用个推!").text("这是一条推送消息~"));
  notificationTemplate.content("PlainText or Json or xml");
  // 定义"AppMessage"类型消息对象，设置消息内容模板、发送的目标App列表、是否支持离线发送、以及离线消息有效期(单位毫秒)
  AppMessageBuilder appMessage = getui.appMessage();
  // appMessage.setData(linkTemplate);
  appMessage.setData(notificationTemplate);
  appMessage.setOffline(true);
  appMessage.setOfflineExpireTime(1000 * 600);
  // 按app发送:给所有app用户推送.
  IPushResult pushResult = getui.pushMessageToApp(appMessage);
  System.out.println(pushResult.getResponse().toString());
  System.out.println(FastjsonUtils.string(pushResult));
  // 给指定的app用户推送.
  String clientId = "dev";
  pushResult = getui
   .pushMessageToSingleClient(getui.singleMessage().template(linkTemplate).offline(true).offlineExpireTime(24, TimeUnit.HOURS), clientId);


 * </code>
 * 
 * @author zhoupan
 */
public class GetuiSupport {

 /** 由IGetui管理页面生成，是您的应用与SDK通信的标识之一，每个应用都对应一个唯一的AppID。. */
 private String id;

 /** 预先分配的第三方应用对应的Key，是您的应用与SDK通信的标识之一。. */
 private String key;

 /** 第三方客户端个推集成鉴权码，用于验证第三方合法性。在客户端集成SDK时需要提供。. */
 private String secret;

 /** 个推服务端API鉴权码，用于验证调用方合法性。在调用个推服务端API时需要提供。（请妥善保管，避免通道被盗用）. */
 private String masterSecret;

 /** 个推服务端api地址. */
 private String url = "http://sdk.open.api.igexin.com/apiex.htm";

 /**
  * Push.
  *
  * @return the i gt push
  */
 public IGtPush push() {
  IGtPush push = new IGtPush(url, key, masterSecret);
  return push;
 }

 /**
  * Link template.
  *
  * @return the link template builder
  */
 public LinkTemplateBuilder linkTemplate() {
  LinkTemplateBuilder template = new LinkTemplateBuilder();
  template.appId(this.id).appKey(this.key);
  return template;
 }

 /**
  * Notification template.
  *
  * @return the notification template builder
  */
 public NotificationTemplateBuilder notificationTemplate() {
  NotificationTemplateBuilder template = new NotificationTemplateBuilder();
  template.appId(this.id).appKey(this.key);
  // 透传默认样式:避免空指针异常
  template.style(this.style());
  return template;
 }

 /**
  * App message.
  *
  * @return the app message builder
  */
 public AppMessageBuilder appMessage() {
  AppMessageBuilder builder = new AppMessageBuilder();
  builder.appId(this.id);
  return builder;
 }

 /**
  * Single message.
  *
  * @return the single message builder
  */
 public SingleMessageBuilder singleMessage() {
  SingleMessageBuilder builder = new SingleMessageBuilder();
  return builder;
 }

 /**
  * Notify style.
  *
  * @return the notify style builder
  */
 public NotifyStyleBuilder style() {
  NotifyStyleBuilder builder = new NotifyStyleBuilder();
  return builder;
 }

 /**
  * Target client.
  *
  * @param clientId
  *         the client id
  * @return the target
  */
 public Target targetClient(String clientId) {
  Target target = new Target();
  target.setAppId(this.id);
  target.setClientId(clientId);
  return target;
 }

 /**
  * Target alias.
  *
  * @param alias
  *         the alias
  * @return the target
  */
 public Target targetAlias(String alias) {
  Target target = new Target();
  target.setAppId(this.id);
  target.setAlias(alias);
  return target;
 }

 /**
  * 对单个或多个指定应用的所有用户群发推送消息。.
  *
  * @param message
  *         the message
  * @return the i push result
  */
 public IPushResult pushMessageToApp(AppMessage message) {
  return this.push().pushMessageToApp(message);
 }

 /**
  * 向单个clientid或别名用户推送消息。 场景1：某用户发生了一笔交易，银行及时下发一条推送消息给该用户。
  * 场景2：用户定制了某本书的预订更新，当本书有更新时，需要向该用户及时下发一条更新提醒信息。
  * 这些需要向指定某个用户推送消息的场景，即需要使用对单个用户推送消息的接口。.
  *
  * @param message
  *         the message
  * @param target
  *         the target
  * @return the i push result
  */
 public IPushResult pushMessageToSingle(SingleMessage message, Target target) {
  IPushResult result = null;
  try {
   result = this.push().pushMessageToSingle(message, target);
  } catch (RequestException e) {
   result = this.push().pushMessageToSingle(message, target, e.getRequestId());
  }
  return result;
 }

 /**
  * 向单个clientid或别名用户推送消息。 场景1：某用户发生了一笔交易，银行及时下发一条推送消息给该用户。
  * 场景2：用户定制了某本书的预订更新，当本书有更新时，需要向该用户及时下发一条更新提醒信息。
  * 这些需要向指定某个用户推送消息的场景，即需要使用对单个用户推送消息的接口。.
  *
  * @param message
  *         the message
  * @param clientId
  *         the client id
  * @return the i push result
  */
 public IPushResult pushMessageToSingleClient(SingleMessage message, String clientId) {
  return this.pushMessageToSingle(message, targetClient(clientId));
 }

 /**
  * 向单个clientid或别名用户推送消息。 场景1：某用户发生了一笔交易，银行及时下发一条推送消息给该用户。
  * 场景2：用户定制了某本书的预订更新，当本书有更新时，需要向该用户及时下发一条更新提醒信息。
  * 这些需要向指定某个用户推送消息的场景，即需要使用对单个用户推送消息的接口。.
  *
  * @param message
  *         the message
  * @param alias
  *         the alias
  * @return the i push result
  */
 public IPushResult pushMessageToSingleAlias(SingleMessage message, String alias) {
  return this.pushMessageToSingle(message, this.targetAlias(alias));
 }

 /**
  * Gets the id.
  *
  * @return the id
  */
 public String getId() {
  return id;
 }

 /**
  * Sets the id.
  *
  * @param id
  *         the new id
  */
 public void setId(String id) {
  this.id = id;
 }

 /**
  * Gets the key.
  *
  * @return the key
  */
 public String getKey() {
  return key;
 }

 /**
  * Sets the key.
  *
  * @param key
  *         the new key
  */
 public void setKey(String key) {
  this.key = key;
 }

 /**
  * Gets the secret.
  *
  * @return the secret
  */
 public String getSecret() {
  return secret;
 }

 /**
  * Sets the secret.
  *
  * @param secret
  *         the new secret
  */
 public void setSecret(String secret) {
  this.secret = secret;
 }

 /**
  * Gets the master secret.
  *
  * @return the master secret
  */
 public String getMasterSecret() {
  return masterSecret;
 }

 /**
  * Sets the master secret.
  *
  * @param masterSecret
  *         the new master secret
  */
 public void setMasterSecret(String masterSecret) {
  this.masterSecret = masterSecret;
 }

 /**
  * Gets the url.
  *
  * @return the url
  */
 public String getUrl() {
  return url;
 }

 /**
  * Sets the url.
  *
  * @param url
  *         the new url
  */
 public void setUrl(String url) {
  this.url = url;
 }
}
