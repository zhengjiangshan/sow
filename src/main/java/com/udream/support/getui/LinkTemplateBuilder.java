package com.udream.support.getui;

import com.gexin.rp.sdk.template.LinkTemplate;
import com.gexin.rp.sdk.template.style.AbstractNotifyStyle;

/**
 * LinkTemplateBuilder.
 * 
 * @author zhoupan
 */
public class LinkTemplateBuilder extends LinkTemplate {

 /**
  * App id.
  *
  * @param appId
  *         the app id
  * @return the link template builder
  */
 public LinkTemplateBuilder appId(String appId) {
  this.setAppId(appId);
  return this;
 }

 /**
  * App key.
  *
  * @param appKey
  *         the app key
  * @return the link template builder
  */
 public LinkTemplateBuilder appKey(String appKey) {
  this.setAppkey(appKey);
  return this;
 }

 /**
  * Style.
  *
  * @param style
  *         the style
  * @return the link template builder
  */
 public LinkTemplateBuilder style(AbstractNotifyStyle style) {
  this.setStyle(style);
  return this;
 }
}
