package com.udream.support.getui;

import java.util.concurrent.TimeUnit;

import com.gexin.rp.sdk.base.ITemplate;
import com.gexin.rp.sdk.base.impl.SingleMessage;

/**
 * SingleMessageBuilder.
 * 
 * @author zhoupan
 */
public class SingleMessageBuilder extends SingleMessage {

 /**
  * Template.
  *
  * @param template
  *         the template
  * @return the single message builder
  */
 public SingleMessageBuilder template(ITemplate template) {
  this.setData(template);
  return this;
 }

 /**
  * Offline.
  *
  * @param offline
  *         the offline
  * @return the single message builder
  */
 public SingleMessageBuilder offline(boolean offline) {
  this.setOffline(offline);
  return this;
 }

 /**
  * Offline expire time.
  *
  * @param duration
  *         the duration
  * @param timeUnit
  *         the time unit
  * @return the single message builder
  */
 public SingleMessageBuilder offlineExpireTime(long duration, TimeUnit timeUnit) {
  this.setOfflineExpireTime(timeUnit.toMillis(duration));
  return this;
 }

}
