package com.udream.support.getui;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.gexin.rp.sdk.base.ITemplate;
import com.gexin.rp.sdk.base.impl.AppMessage;

/**
 * AppMessageBuilder.
 * 
 * @author zhoupan
 */
public class AppMessageBuilder extends AppMessage {

 /**
  * App id.
  *
  * @param appId
  *         the app id
  * @return the app message builder
  */
 public AppMessageBuilder appId(String appId) {
  if (this.getAppIdList() == null) {
   this.setAppIdList(new ArrayList<String>());
  }
  this.getAppIdList().add(appId);
  return this;
 }

 /**
  * Template.
  *
  * @param template
  *         the template
  * @return the app message builder
  */
 public AppMessageBuilder template(ITemplate template) {
  this.setData(template);
  return this;
 }

 /**
  * Offline.
  *
  * @param offline
  *         the offline
  * @return the app message builder
  */
 public AppMessageBuilder offline(boolean offline) {
  this.setOffline(offline);
  return this;
 }

 /**
  * Offline expire time.
  *
  * @param duration
  *         the duration
  * @param timeUnit
  *         the time unit
  * @return the app message builder
  */
 public AppMessageBuilder offlineExpireTime(long duration, TimeUnit timeUnit) {
  this.setOfflineExpireTime(timeUnit.toMillis(duration));
  return this;
 }

}
