package com.udream.support.getui;

import com.gexin.rp.sdk.template.style.AbstractNotifyStyle;
import com.udream.support.CommonSupport;

/**
 * NotifyStyleBuilder.
 * 
 * @author zhoupan
 */
public class NotifyStyleBuilder extends AbstractNotifyStyle {
 /**
  * 标题.
  */
 private String title;

 /**
  * Title.
  *
  * @param title
  *         the title
  * @return the notify style builder
  */
 public NotifyStyleBuilder title(String title) {
  this.title = title;
  return this;
 }

 /**
  * 文本.
  */
 private String text;

 /**
  * Text.
  *
  * @param text
  *         the text
  * @return the notify style builder
  */
 public NotifyStyleBuilder text(String text) {
  this.text = text;
  return this;
 }

 /**
  * logo.
  */
 private String logo;

 /**
  * Logo.
  *
  * @param logo
  *         the logo
  * @return the notify style builder
  */
 public NotifyStyleBuilder logo(String logo) {
  this.logo = logo;
  return this;
 }

 /**
  * logoUrl.
  */
 private String logoUrl;

 /**
  * Logo url.
  *
  * @param logoUrl
  *         the logo url
  * @return the notify style builder
  */
 public NotifyStyleBuilder logoUrl(String logoUrl) {
  this.logoUrl = logoUrl;
  return this;
 }

 /**
  * 是否响铃.
  */
 private Boolean isRing;

 /**
  * Ring.
  *
  * @param isRing
  *         the is ring
  * @return the notify style builder
  */
 public NotifyStyleBuilder ring(Boolean isRing) {
  this.isRing = isRing;
  return this;
 }

 /**
  * 是否震动.
  */
 private Boolean isVibrate;

 /**
  * Vibrate.
  *
  * @param isVibrate
  *         the is vibrate
  * @return the notify style builder
  */
 public NotifyStyleBuilder vibrate(Boolean isVibrate) {
  this.isVibrate = isVibrate;
  return this;
 }

 /**
  * 是否可清除.
  */
 private Boolean isClearable;

 /**
  * Clearable.
  *
  * @param isClearable
  *         the is clearable
  * @return the notify style builder
  */
 public NotifyStyleBuilder clearable(Boolean isClearable) {
  this.isClearable = isClearable;
  return this;
 }

 /** The banner url. */
 private String bannerUrl;

 /**
  * Clearable.
  *
  * @param bannerUrl
  *         the banner url
  * @return the notify style builder
  */
 public NotifyStyleBuilder clearable(String bannerUrl) {
  this.bannerUrl = bannerUrl;
  return this;
 }

 /** The big style. */
 private String bigStyle;

 /**
  * Big style.
  *
  * @param bigStyle
  *         the big style
  * @return the notify style builder
  */
 public NotifyStyleBuilder bigStyle(String bigStyle) {
  this.bigStyle = bigStyle;
  return this;
 }

 /** The big image url. */
 private String bigImageUrl;

 /**
  * Big image url.
  *
  * @param bigImageUrl
  *         the big image url
  * @return the notify style builder
  */
 public NotifyStyleBuilder bigImageUrl(String bigImageUrl) {
  this.bigStyle = "1";
  this.bigImageUrl = bigImageUrl;
  return this;
 }

 /** The big text. */
 private String bigText;

 /**
  * Big text.
  *
  * @param bigText
  *         the big text
  * @return the notify style builder
  */
 public NotifyStyleBuilder bigText(String bigText) {
  this.bigStyle = "2";
  this.bigText = bigText;
  return this;
 }

 /**
  * Big image url and text.
  *
  * @param bigImageUrl
  *         the big image url
  * @param bigText
  *         the big text
  * @return the notify style builder
  */
 public NotifyStyleBuilder bigImageUrlAndText(String bigImageUrl, String bigText) {
  this.bigStyle = "3";
  this.bigImageUrl = bigImageUrl;
  this.bigText = bigText;
  return this;
 }

 /** The notify style. */
 private String notifyStyle;

 /**
  * Notify style.
  *
  * @param notifyStyle
  *         the notify style
  * @return the notify style builder
  */
 public NotifyStyleBuilder notifyStyle(String notifyStyle) {
  this.notifyStyle = notifyStyle;
  return this;
 }

 /**
  * Resolve notify style.
  *
  * @return the string
  */
 public String resolveNotifyStyle() {
  if (CommonSupport.isNotBlank(this.notifyStyle)) {
   return this.notifyStyle;
  }
  if (bigStyle != null || bigImageUrl != null || this.bigText != null) {
   return "6";
  }
  if (this.bannerUrl != null) {
   return "4";
  }
  if (this.isRing != null || this.isVibrate != null || this.isClearable != null) {
   return "0";
  }
  return "1";
 }

 /**
  * Is_noring.
  *
  * @return the string
  */
 protected String is_noring() {
  boolean noRing = this.isRing == null || !this.isRing;
  return String.valueOf(noRing);
 }

 /**
  * Is_noclear.
  *
  * @return the string
  */
 protected String is_noclear() {
  boolean noClear = this.isClearable == null || !this.isClearable;
  return String.valueOf(noClear);
 }

 /**
  * Is_novibrate.
  *
  * @return the string
  */
 protected String is_novibrate() {
  boolean noVibrate = this.isVibrate == null || !this.isVibrate;
  return String.valueOf(noVibrate);
 }

 /*
  * (non-Javadoc)
  * 
  * @see com.gexin.rp.sdk.template.style.INotifyStyle#getActionChain()
  */
 public com.gexin.rp.sdk.dto.GtReq.ActionChain getActionChain() {
  if (title != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("title").setVal(title)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  if (text != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("text").setVal(text)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  if (logo != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("logo").setVal(logo)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  if (logoUrl != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("logo_url").setVal(logoUrl)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  if (this.bannerUrl != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("banner_url").setVal(this.bannerUrl)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  if (this.bigStyle != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("bigStyle").setVal(bigStyle)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  if (this.bigImageUrl != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("big_image_url").setVal(bigImageUrl)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  if (this.bigText != null) {
   this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("big_text").setVal(bigText)
    .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  }
  this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("is_noring").setVal(this.is_noring())
   .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.bool).build());
  this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("is_noclear").setVal(this.is_noclear())
   .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.bool).build());
  this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("is_novibrate").setVal(this.is_novibrate())
   .setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.bool).build());
  // Resolve NotifyStyle.
  this.actionChainBuilder.addField(com.gexin.rp.sdk.dto.GtReq.InnerFiled.newBuilder().setKey("notifyStyle")
   .setVal(this.resolveNotifyStyle()).setType(com.gexin.rp.sdk.dto.GtReq.InnerFiled.Type.str).build());
  return actionChainBuilder.build();
 }
}
