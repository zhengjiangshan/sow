package com.udream.support.getui;

import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.style.AbstractNotifyStyle;

/**
 * NotificationTemplateBuilder.
 * 
 * @author zhoupan
 */
public class NotificationTemplateBuilder extends NotificationTemplate {

 /**
  * App id.
  *
  * @param appId
  *         the app id
  * @return the notification template builder
  */
 public NotificationTemplateBuilder appId(String appId) {
  this.setAppId(appId);
  return this;
 }

 /**
  * App key.
  *
  * @param appKey
  *         the app key
  * @return the notification template builder
  */
 public NotificationTemplateBuilder appKey(String appKey) {
  this.setAppkey(appKey);
  return this;
 }

 /**
  * Style.
  *
  * @param style
  *         the style
  * @return the notification template builder
  */
 public NotificationTemplateBuilder style(AbstractNotifyStyle style) {
  this.setStyle(style);
  return this;
 }

 /**
  * 透传消息设置，是否强制启动应用.
  *
  * @param forceStart
  *         the force start
  * @return the notification template builder
  */
 public NotificationTemplateBuilder forceStart(boolean forceStart) {
  // 透传消息设置，1为强制启动应用，客户端接收到消息后就会立即启动应用；2为等待应用启动
  this.setTransmissionType(forceStart ? 1 : 2);
  return this;
 }

 /**
  * 透传的内容.
  *
  * @param content
  *         the content
  * @return the notification template builder
  */
 public NotificationTemplateBuilder content(String content) {
  this.setTransmissionContent(content);
  return this;
 }
}
