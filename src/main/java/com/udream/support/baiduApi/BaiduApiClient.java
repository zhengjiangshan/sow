package com.udream.support.baiduApi;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.udream.common.DataItemResponse;
import com.udream.support.RestTemplateSupport;

@Service
public class BaiduApiClient extends RestTemplateSupport {
 /** The Constant DEFAULT_BASE_URL. */
 public static final String DEFAULT_BASE_URL = "http://api.map.baidu.com/geocoder/v2/";

 /** The Constant URL_TEMPLATE_QUERY. */
 public static final String URL_TEMPLATE_QUERY = "?ak=dlfgBEvgoRbA86VOdwxP08XQHzjMZCoq&location={lat},{lng}&output=json&pois=0";

 public BaiduApiClient() {
  super();
  this.setBaseUrl(DEFAULT_BASE_URL);
 }

 public DataItemResponse<BDResultBase> selectAreaByLat(Double lat, Double lng) {
  DataItemResponse<BDResultBase> dataResponse = new DataItemResponse<BDResultBase>();
  try {
   Map<String, String> params = new HashMap<String, String>();
   params.put("lng", lng.toString());
   params.put("lat", lat.toString());
   return this.getItem(URL_TEMPLATE_QUERY, params, BDResultBase.class);
  } catch (Throwable e) {
   dataResponse.onException(e);
  }
  return dataResponse;
 }

 public static void main(String[] args) {
  BaiduApiClient baiduApiClient = new BaiduApiClient();
  DataItemResponse<BDResultBase> result = baiduApiClient.selectAreaByLat(22.3595967d, 114.0007837d);
  System.out.println(result.getItem().getResult().getAddressComponent().getAdcode());
  System.out.println(JSON.toJSONString(result));
 }
}
