package com.udream.support.baiduApi;

/**
 * Created by PC03 on 2017/7/25.
 */

public class Point {
    private double x;

    private double y;

    public void setX(double x){
        this.x = x;
    }
    public double getX(){
        return this.x;
    }
    public void setY(double y){
        this.y = y;
    }
    public double getY(){
        return this.y;
    }
}
