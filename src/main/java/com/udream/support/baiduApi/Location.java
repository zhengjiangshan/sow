package com.udream.support.baiduApi;

/**
 * Created by PC03 on 2017/7/25.
 */

public class Location {
    private double lng;

    private double lat;

    public void setLng(double lng){
        this.lng = lng;
    }
    public double getLng(){
        return this.lng;
    }
    public void setLat(double lat){
        this.lat = lat;
    }
    public double getLat(){
        return this.lat;
    }
}
