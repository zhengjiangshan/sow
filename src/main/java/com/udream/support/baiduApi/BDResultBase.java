package com.udream.support.baiduApi;

/**
 * Created by PC03 on 2017/7/25.
 */

public class BDResultBase {
    private int status;

    private Result result;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setResult(Result result){
        this.result = result;
    }
    public Result getResult(){
        return this.result;
    }
}
