package com.udream.support.baiduApi;

/**
 * Created by PC03 on 2017/7/25.
 */

public class ParentPoi {
    private String name;

    private String tag;

    private String addr;

    private Point point;

    private String direction;

    private String distance;

    private String uid;

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setTag(String tag){
        this.tag = tag;
    }
    public String getTag(){
        return this.tag;
    }
    public void setAddr(String addr){
        this.addr = addr;
    }
    public String getAddr(){
        return this.addr;
    }
    public void setPoint(Point point){
        this.point = point;
    }
    public Point getPoint(){
        return this.point;
    }
    public void setDirection(String direction){
        this.direction = direction;
    }
    public String getDirection(){
        return this.direction;
    }
    public void setDistance(String distance){
        this.distance = distance;
    }
    public String getDistance(){
        return this.distance;
    }
    public void setUid(String uid){
        this.uid = uid;
    }
    public String getUid(){
        return this.uid;
    }
}
