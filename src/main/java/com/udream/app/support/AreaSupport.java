package com.udream.app.support;

/**
 * AreaSupport.
 * 
 * @author zhoupan.
 */
public interface AreaSupport {

 /**
  * Sets the province id.
  *
  * @param provinceId
  *         the new province id
  */
 void setProvinceId(String provinceId);

 /**
  * Gets the province id.
  *
  * @return the province id
  */
 String getProvinceId();

 /**
  * Gets the city id.
  *
  * @return the city id
  */
 String getCityId();

 /**
  * Sets the city id.
  *
  * @param cityId
  *         the new city id
  */
 void setCityId(String cityId);

 /**
  * Gets the area id.
  *
  * @return the area id
  */
 String getAreaId();

 /**
  * Sets the area id.
  *
  * @param areaId
  *         the new area id
  */
 void setAreaId(String areaId);

}