package com.udream.app.support;

public class CheckVerifyCodeRequest {

 private int codeType;
 private String mobile;
 private String code;

 public int getCodeType() {
  return codeType;
 }

 public void setCodeType(int codeType) {
  this.codeType = codeType;
 }

 public String getMobile() {
  return mobile;
 }

 public void setMobile(String mobile) {
  this.mobile = mobile;
 }

 public String getCode() {
  return code;
 }

 public void setCode(String code) {
  this.code = code;
 }

}
