package com.udream.app.support;

public class MobileRegisterRequest {

 private String mobile;
 private String code;
 private String password;
 private String passwordConfirmed;

 public String getMobile() {
  return mobile;
 }

 public void setMobile(String mobile) {
  this.mobile = mobile;
 }

 public String getCode() {
  return code;
 }

 public void setCode(String code) {
  this.code = code;
 }

 public String getPassword() {
  return password;
 }

 public void setPassword(String password) {
  this.password = password;
 }

 public String getPasswordConfirmed() {
  return passwordConfirmed;
 }

 public void setPasswordConfirmed(String passwordConfirmed) {
  this.passwordConfirmed = passwordConfirmed;
 }

}
