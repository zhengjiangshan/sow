package com.udream.app.support;

public class SendVerifyCodeRequest {

 private int codeType;
 private String mobile;

 public int getCodeType() {
  return codeType;
 }

 public void setCodeType(int codeType) {
  this.codeType = codeType;
 }

 public String getMobile() {
  return mobile;
 }

 public void setMobile(String mobile) {
  this.mobile = mobile;
 }

}
