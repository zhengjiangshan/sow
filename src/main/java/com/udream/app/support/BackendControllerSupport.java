package com.udream.app.support;

import com.udream.support.GenericModelSupportAction;
import com.udream.support.ModelSupport;

/**
 * BackendControllerSupport.
 *
 * @param <T>
 *         the generic type
 */
public class BackendControllerSupport<T extends ModelSupport> extends GenericModelSupportAction<T> {

}
