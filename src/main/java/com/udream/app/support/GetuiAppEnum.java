package com.udream.app.support;

import com.udream.support.CommonSupport;

/**
 * 个推配置. <code>
  运营端：
  app.id=xmTqcEzRAx6YDl7eDCuep4
  app.key=ffYaPJELQe6A6zule3QoE
  app.secret=ZzSFdT2RxiA7eTAJYg1mO
  app.master.secret=wg5J3QTXYn5CdvgRtiQZC6
  app.code=com.cnilike
  </code> <code>
商家端：
app.id=I0VWdfnMnz7CMINu78Lan6
app.key=CZeWqCgIzs8vyOOOIaKz29
app.secret=ckJ1ZATyiH88PEeexl3jI
app.master.secret=Sy8xl2GeGkAVW0AEcDZXK1
app.code=com.cnilike.shop
</code> <code>
公益端：
 app.id=bEX8vUVqbV8G5P9ZuuVDR6
 app.key=CHrY79tpCs8jekN53LCye4
 app.secret=A17TR5XRnNA3ehis8Udus3
 app.master.secret=2P73HjmEg08VzgKbxD0YM3
 app.code=com.cnilike.shoppingmall
 </code>
 * 
 * @author zhoupan.
 */
public enum GetuiAppEnum {

 /** 运营端app. */
 OPERATOR("com.cnilike", "xmTqcEzRAx6YDl7eDCuep4", "ffYaPJELQe6A6zule3QoE", "ZzSFdT2RxiA7eTAJYg1mO", "wg5J3QTXYn5CdvgRtiQZC6"),

 /** 商家端app. */
 MERCHANT("com.cnilike.shop", "I0VWdfnMnz7CMINu78Lan6", "CZeWqCgIzs8vyOOOIaKz29", "ckJ1ZATyiH88PEeexl3jI", "Sy8xl2GeGkAVW0AEcDZXK1"),

 /** 消费端app. */
 CONSUMER("com.cnilike.shoppingmall", "bEX8vUVqbV8G5P9ZuuVDR6", "CHrY79tpCs8jekN53LCye4", "A17TR5XRnNA3ehis8Udus3", "2P73HjmEg08VzgKbxD0YM3");

 /**
  * 个推应用编码.
  */
 private String code;

 /**
  * 个推应用ID.
  */
 private String id;

 /**
  * 个推应用KEY.
  */
 private String key;

 /**
  * 个推应用SECRET.
  */
 private String secret;

 /**
  * 个推应用MASTERSECRET.
  */
 private String masterSecret;

 /**
  * Instantiates a new getui app enum.
  *
  * @param code
  *         the code
  * @param id
  *         the id
  * @param key
  *         the key
  * @param secret
  *         the secret
  * @param masterSecret
  *         the master secret
  */
 private GetuiAppEnum(String code, String id, String key, String secret, String masterSecret) {
  this.code = code;
  this.id = id;
  this.key = key;
  this.secret = secret;
  this.masterSecret = masterSecret;
 }

 /**
  * Gets the id.
  *
  * @return the id
  */
 public String getId() {
  return id;
 }

 /**
  * Gets the key.
  *
  * @return the key
  */
 public String getKey() {
  return key;
 }

 /**
  * Gets the secret.
  *
  * @return the secret
  */
 public String getSecret() {
  return secret;
 }

 /**
  * Gets the master secret.
  *
  * @return the master secret
  */
 public String getMasterSecret() {
  return masterSecret;
 }

 /**
  * Gets the code.
  *
  * @return the code
  */
 public String getCode() {
  return code;
 }

 /**
  * In.
  *
  * @param value
  *         the value
  * @param values
  *         the values
  * @return true, if in
  */
 public static boolean in(String value, GetuiAppEnum... values) {
  if (values == null) {
   return false;
  }
  for (GetuiAppEnum ealueEnum : values) {
   if (CommonSupport.equalsIgnoreCase(ealueEnum.getCode(), value)) {
    return true;
   }
  }
  return false;
 }

 /**
  * From.
  *
  * @param value
  *         the value
  * @return the app system enum
  */
 public static GetuiAppEnum from(String value) {
  GetuiAppEnum[] values = GetuiAppEnum.values();
  for (GetuiAppEnum valueEnum : values) {
   if (CommonSupport.equalsIgnoreCase(valueEnum.getCode(), value)) {
    return valueEnum;
   }
   if (CommonSupport.equalsIgnoreCase(valueEnum.getId(), value)) {
    return valueEnum;
   }
  }
  return null;
 }
}
