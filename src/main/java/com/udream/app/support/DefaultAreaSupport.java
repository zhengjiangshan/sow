package com.udream.app.support;

public class DefaultAreaSupport implements AreaSupport {

 /**
  * 省ID.
  */
 private String provinceId;
 private String provinceName;

 /**
  * 市ID.
  */
 private String cityId;
 private String cityName;

 /**
  * 区ID.
  */
 private String areaId;
 private String areaName;

 public String getProvinceId() {
  return provinceId;
 }

 public void setProvinceId(String provinceId) {
  this.provinceId = provinceId;
 }

 public String getProvinceName() {
  return provinceName;
 }

 public void setProvinceName(String provinceName) {
  this.provinceName = provinceName;
 }

 public String getCityId() {
  return cityId;
 }

 public void setCityId(String cityId) {
  this.cityId = cityId;
 }

 public String getCityName() {
  return cityName;
 }

 public void setCityName(String cityName) {
  this.cityName = cityName;
 }

 public String getAreaId() {
  return areaId;
 }

 public void setAreaId(String areaId) {
  this.areaId = areaId;
 }

 public String getAreaName() {
  return areaName;
 }

 public void setAreaName(String areaName) {
  this.areaName = areaName;
 }

}
