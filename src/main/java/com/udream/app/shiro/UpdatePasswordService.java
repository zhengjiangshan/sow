package com.udream.app.shiro;

/**
 * 修改密码服务.
 * 
 * @author zhoupan
 *
 */
public interface UpdatePasswordService {

 public boolean accept(Object loginUser);

 public UpdatePasswordResponse updatePassword(Object loginUser, UpdatePasswordRequest request);
}
