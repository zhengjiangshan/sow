package com.udream.app.shiro;

public interface AuthorizationInfoCacheCleanSupport {

 public void clearAllCachedAuthorizationInfo();
}
