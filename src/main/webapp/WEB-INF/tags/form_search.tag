<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!-- 查询表单{ -->
<div class="box box-default">
 <div class="box-header with-border">
  <h3 class="box-title">查询</h3>
  <div class="box-tools">
   <button type="button" class="btn btn-box-tool" data-widget="collapse">
    <i class="fa fa-minus"></i>
   </button>
  </div>
 </div>
 <form id="searchForm" method="post">
  <div class="box-body">
  	<div class="row">
   		<jsp:doBody />
   	</div>
  </div>
  <div class="box-footer">
   <input type="button" class="btn btn-default btn-sm label-primary  distance" onclick="CrudApp.on('search');" value="查询" /> <input type="button" class="btn btn-default btn-sm label-primary  distance"
    onclick="CrudApp.on('clearSearch');" value="重置" />
  </div>
 </form>
</div>
<!-- 查询表单} -->