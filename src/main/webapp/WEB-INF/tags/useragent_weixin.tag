<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%
 com.cnilike.support.UserAgentSupport userAgentSupport = com.cnilike.support.UserAgentUtils.from(request);
 if (userAgentSupport.isWeixin()) {
%>
<jsp:doBody />
<%
 }
%>
