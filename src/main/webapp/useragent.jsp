<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/taglib.jsp"%>
<%
 com.cnilike.support.UserAgentSupport userAgentSupport = com.cnilike.support.UserAgentUtils.from(request);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
</head>
<body>
 <h3>UserAgent</h3>
 <p><%=userAgentSupport%></p>
 <tags:useragent_computer>This is computer</tags:useragent_computer>
 <tags:useragent_mobile>This is mobile</tags:useragent_mobile>
 <tags:useragent_android>This is android</tags:useragent_android>
 <tags:useragent_weixin>This is weixin</tags:useragent_weixin>
 <tags:useragent_not_weixin>This is NOT weixin</tags:useragent_not_weixin>
</body>
</html>
